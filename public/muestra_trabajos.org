# -*- org-html-postamble: mi-postamble -*-
#+TITLE: Muestra (no exhaustiva) de trabajos en composición tipográfica
#+SUBTITLE: Juan Manuel Macías
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:t" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:t" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (filtro-html-misc)"))
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t

#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#+HTML_HEAD: <style>body{padding:0;margin:0;background:#C6C6C6;color:#3c495a;font-weight:normal;font-size:15px;font-family:'Noto Sans','Arial',sans-serif;}</style>
#+HTML_HEAD: <style>#content{width:96%;max-width:1000px;margin:auto auto 6% auto;background:#C6C6C6;}</style>
#+HTML_HEAD: <style> .figure-number {display: none;} </style>
#+HTML_HEAD: <style> .figure { padding: 1em; width:95%;display: block; margin-left: auto; margin-right: auto;align:center} </style>

{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none
  ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

#+begin_center
/La tradición de la imprenta en el ámbito digital/

Contacto: [[mailto:maciaschain@posteo.net][maciaschain@posteo.net]]

(/click/ en cada imagen para ampliarla)
#+end_center


#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Diccionario Hispánico de la Tradición y Recepción Clásica/
[[./images/muestras-dhtc1.png][file:./images/muestras-dhtc1.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Diccionario Hispánico de la Tradición y Recepción Clásica/
[[./images/muestras-dhtc2.png][file:./images/muestras-dhtc2.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Diccionario Hispánico de la Tradición y Recepción Clásica/
[[./images/muestras-dhtc3.png][file:./images/muestras-dhtc3.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Diccionario Hispánico de la Tradición y Recepción Clásica/
[[./images/muestras-dhtc4.png][file:./images/muestras-dhtc4.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Diccionario Hispánico de la Tradición y Recepción Clásica/
[[./images/muestras-dhtc5.png][file:./images/muestras-dhtc5.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Diccionario Hispánico de la Tradición y Recepción Clásica/
[[./images/muestras-dhtc6.png][file:./images/muestras-dhtc6.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Chariton of Aphrodisias, Callirhoe/. Edición crítica a cargo de Manuel Sanz Morales (Universitätsverlag Winter, Heildelberg)
[[./images/muestras_cariton1.png][file:./images/muestras_cariton1.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Chariton of Aphrodisias, Callirhoe/. Edición crítica a cargo de Manuel Sanz Morales (Universitätsverlag Winter, Heildelberg)
[[./images/muestras_cariton2.png][file:./images/muestras_cariton2.png]]

#+ATTR_HTML: :style width:100%
#+CAPTION: /Chariton of Aphrodisias, Callirhoe/. Edición crítica a cargo de Manuel Sanz Morales (Universitätsverlag Winter, Heildelberg)
[[./images/muestras_cariton3.png][file:./images/muestras_cariton3.png]]

#+ATTR_HTML: :style width:100%
#+CAPTION: /Chariton of Aphrodisias, Callirhoe/. Edición crítica a cargo de Manuel Sanz Morales (Universitätsverlag Winter, Heildelberg)
[[./images/muestras_cariton4.png][file:./images/muestras_cariton4.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /From the ancient manuscript to the critical edition of Greek texts. A Festschrift to prof. Elsa García Novo/. Teresa Martínez Manzano y Felipe G. Hernández Muñoz (eds.)
[[./images/muestras_GNovo1.png][file:./images/muestras_GNovo1.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /From the ancient manuscript to the critical edition of Greek texts. A Festschrift to prof. Elsa García Novo/. Teresa Martínez Manzano y Felipe G. Hernández Muñoz (eds.)
[[./images/muestras_GNovo2.png][file:./images/muestras_GNovo2.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /From the ancient manuscript to the critical edition of Greek texts. A Festschrift to prof. Elsa García Novo/. Teresa Martínez Manzano y Felipe G. Hernández Muñoz (eds.)
[[./images/muestras_GNovo3.png][file:./images/muestras_GNovo3.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /From the ancient manuscript to the critical edition of Greek texts. A Festschrift to prof. Elsa García Novo/. Teresa Martínez Manzano y Felipe G. Hernández Muñoz (eds.)
[[./images/muestras_GNovo4.png][file:./images/muestras_GNovo4.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Diccionario de personajes de la comedia antigua/
[[./images/muestras-dpca1.jpg][file:./images/muestras-dpca1.jpg]]

#+ATTR_HTML: :style width:50%
#+ATTR_HTML: :target _blank
#+CAPTION: /Diccionario de personajes de la comedia antigua/
[[./images/muestras-dpca3.jpg][file:./images/muestras-dpca3.jpg]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Las Filípicas de Demóstenes/. Edición crítica (a cargo de Felipe Hernández Muñoz) y traducción (de Fernando García Romero), en formato bilingüe
[[./images/muestras_filipicas.jpg][file:./images/muestras_filipicas.jpg]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Omero, i cardinali e gli esuli/, de David Speranzi
[[./images/muestras-speranzi.jpg][file:./images/muestras-speranzi.jpg]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Poemas a la noche y otra poesía póstuma y dispersa/ (Tilke. Edición bilingüe con trad. de Juan Andrés García Román)
[[./images/muestras-rilke.jpg][file:./images/muestras-rilke.jpg]]

#+ATTR_HTML: :style width:50%
#+ATTR_HTML: :target _blank
#+CAPTION: Edición crítica de Alejandro Rétor
[[./images/muestras-aretor.jpg][file:./images/muestras-aretor.jpg]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: Edición crítica de Alejandro Rétor (detalle del aparato crítico)
[[./images/aretor.png][file:./images/aretor.png]]

#+ATTR_HTML: :style width:50%
#+ATTR_HTML: :target _blank
#+CAPTION: /El fósforo astillado/ (poemario de Juan Andrés García Román)
[[./images/muestras-fosforo.jpg][file:./images/muestras-fosforo.jpg]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Un nuevo manuscrito en griego de/ El Quijote /de Cervantes/. Edición y lectura de Olga Omatos
[[./images/muestras-quijote1.jpg][file:./images/muestras-quijote1.jpg]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Un nuevo manuscrito en griego de/ El Quijote /de Cervantes/. Edición y lectura de Olga Omatos (detalle de las notas al pie en formato párrafo)
[[./images/muestras-quijote2.jpg][file:./images/muestras-quijote2.jpg]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: /Monta y cabe. Juegos de nuestra infancia y otros recuerdos de la Posguerra española/
[[./images/muestras-mc1.jpg][file:./images/muestras-mc1.jpg]]

#+ATTR_HTML: :style width:50%
#+ATTR_HTML: :target _blank
#+CAPTION: /Monta y cabe. Juegos de nuestra infancia y otros recuerdos de la Posguerra española/
[[./images/muestras-mc2.jpg][file:./images/muestras-mc2.jpg]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: Revista /Cuaderno Ático/. Poema /Sintra/ de Luisa Sigea (trad. de Aurora Luque)
[[./images/muestras-sintra.png][file:./images/muestras-sintra.png]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: Revista /Cuaderno Ático/. Poema /Manso/ de Sarah Howe (trad. de Carlos Alcorta)
[[./images/muestras-manso.jpg][file:./images/muestras-manso.jpg]]

#+ATTR_HTML: :style width:100%
#+ATTR_HTML: :target _blank
#+CAPTION: Revista /Cuaderno Ático/. Poema de Zanasis Jatsópulos (trad. de Vicente Fernández González)
[[./images/muestras-jatsopulos.png][file:./images/muestras-jatsopulos.png]]

{{{pie}}}

#+begin_export html
<div>
<p>Publicado: 29/11/19</p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
