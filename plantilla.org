# -*- org-confirm-babel-evaluate: nil; -*-
#+TITLE:
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:t" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:t" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (filtro-html-misc)"))
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
# +STARTUP: inlineimages
#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Exportación a LaTeX
#+LaTeX_CLASS: normal-lua-org

#+NAME: preambulo
#+begin_src latex :exports none :eval never-export
 % \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
  \let\maketitleold\maketitle
  \def\maketitle{{\fontspec{Arial}\maketitleold}}
  \let\tableold\tableofcontents
  \def\tableofcontents{\tableold\bigskip}
  \def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
  \newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
  \newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{titulocab}}]{{\cabecera\MakeUppercase{titulocab}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
  \pagestyle{base}
  \usepackage{minted}
  \usepackage[spanish,nospace]{varioref}
  \renewcommand{\listingscaption}{Cuadro de código}
#+end_src

#+begin_src latex :noweb yes :results raw
,#+LaTeX_HEADER: <<preambulo>>
#+end_src

#+NAME: lunotipia-social
#+begin_src emacs-lisp :exports none
  "social.jpg"
#+end_src

{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none :noweb yes :noweb-prefix no
    ;; Para LaTeX
  (defun filtro-latex-misc (texto backend info)
      "reemplaza el coamndo \ref por \vref y forma nums. romanos con versalitas"
      (when (org-export-derived-backend-p backend 'latex)
	(replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "\\\\miscrom{\\1}"
	  (replace-regexp-in-string "\\\\begin{enumerate}"  "\\\\begin{enumerate}[mienu]"
	    (replace-regexp-in-string "\\\\ref{"  "\\\\vref{" texto)))))

    ;; Para html
    (defun filtro-html-misc (texto backend info)
      "modifica epígrafe de las notas y añade un escalado menor a los
	números romanos, para emular algo parecido a versalitas"
      (when (org-export-derived-backend-p backend 'html)
	(replace-regexp-in-string "social\\.jpg" <<lunotipia-social>>
	(replace-regexp-in-string ">Notas al pie de p&aacute;gina:"  ">Notas:"
	(replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
	(replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto))))))
#+end_src

#+begin_export html
<form>
<p style="text-align: center;"><button formaction="https://maciaschain.gitlab.io/lunotipia/pdf/entradaxxpdf.pdf"><font size="2">Descargar esta entrada en formato PDF</font></button></p>
</form>
#+end_export








