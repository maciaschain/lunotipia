% Created 2019-09-05 jue 14:54
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{«Tonos» y «oxia», o el duplicado acento agudo en griego}}]{{\cabecera\MakeUppercase{«Tonos» y «oxia», o el duplicado acento agudo en griego}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{«Tonos» y «oxia», o el duplicado acento agudo en griego}
\begin{document}

\maketitle
\tableofcontents

Nadie duda a estas alturas de la gran bendición que supuso un estándar como Unicode para el tratamiento digital de los
textos. Y la justicia y reparación que vino con ello, pues las computadoras se veían por fin capaces de entender y
representar cualquier sistema de escritura humano más allá del latino, tanto actual como antiguo, ya fuese usado por
muchos, ya minoritario y casi desconocido. O, al menos, aspirar a semejante ideal, una meta que cada vez se antoja más
cercana con cada edición oficial del estándar. Desde los toscos grafos oghámicos a las intrincadas escrituras de Asia o
de África, pasando por silabarios, alfabetos, notaciones musicales y símbolos de toda clase, por supuesto que no podría
faltar en esta ordenada fiesta y encasillado bullicio la venerable escritura griega. De la cual volvemos a hablar aquí a
cuento de una pequeña y curiosa inconsistencia que viene acarreando el estándar (Unicode no es perfecto) desde los
propios orígenes de la inclusión del griego. La cuestión afecta al acento agudo y, si no andamos con el debido cuidado,
puede complicarnos innecesariamente la vida a la hora de trabajar digitalmente con un texto en caracteres helenos.

\section{Dos acentos agudos ¿innecesarios?}
\label{sec:org96844ce}
Como bien sabe (o debería saber) todo aquel que trabaja en un ordenador con textos griegos, Unicode tiene asignadas dos
«parcelas» bien delimitadas para la correcta representación de esta lengua. A una la llama «griego» y a la otra «griego
extendido». La primera incluye el alfabeto completo, los signos de puntuación y todas las vocales acentuadas como
caracteres precompuestos. O sea, la vocal de turno y, sobre ella, el único acento que desde la reforma monotónica del 82
admite la escritura griega actual, y que no es sino un acento agudo como el que tenemos en español y al cual
impropiamente llamamos «tilde». Por otro lado, la parcela de griego extendido comprende todos los caracteres
precompuestos (vocales, mayormente) con el juego y combinatoria de diacríticos necesarios para la escritura del griego
politónico, que es la manera de representar el griego «antiguo» y el griego anterior a la mencionada reforma, pues a
pesar de que la lengua sólo cuenta desde hace mucho tiempo con un único acento intensivo, se seguía escribiendo (por
cuestiones más culturales que pragmáticas) con los signos de los espíritus y la ancestral tríada de acentos agudo, grave
y circunflejo.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/ebgaramond1.png}
\caption{\label{fig:org08e506b}
Área de griego básico en la fuente EB Garamond (visualizada en FontForge)}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/ebgaramond2.png}
\caption{\label{fig:orga2bfabb}
Área de griego extendido en la misma fuente}
\end{figure}

Naturalmente, las fuentes pueden incluir ambas parcelas o sólo la primera, que es la básica. Si se da este último caso,
únicamente podremos escribir o representar griego monotónico. Hasta aquí, todo perfecto y, más o menos, dentro de la
habitual coherencia de Unicode. Pero trazar una línea divisoria tan drástica entre los sistemas mono y politónicos nos
llevó también a una inesperada duplicidad de acentos agudos. De tal suerte que Unicode presenta un acento para el área
monotónica, al cual llama con el término griego \emph{tonos}, y otro para el griego extendido politónico, que denomina,
también en griego, \emph{oxia}. Esto ya de por sí es una inconsistencia, pues la distinción que hace aquí el estándar no es
gráfica sino histórica. Y nos da a entender algo que no se corresponde con la realidad, como si la reforma monotónica
estableciese un nuevo signo diacrítico cuando lo que hizo fue simplemente eliminar de la escritura normativa aquellos
signos innecesarios por representar acentos y hechos fonológicos ya dejados muy atrás por la lengua griega en su
constante discurrir. En dos palabras, se descartaron los acentos grave y circunflejo (además de los dos espíritus que
marcaban la aspiración inicial o la ausencia de ella), y se dejó sólo el acento agudo. En lo que atañe al plano de la
letra escrita, y al margen de la fonología y de la historia, este acento, por tanto, es y debe ser siempre el mismo.

A pesar de la inconsistencia, no tendríamos que temer ningún inconveniente, si al fin y al cabo ambos acentos son
idénticos. Sin embargo, el problema de verdad (al menos a efectos prácticos) viene ahora. Y es que no pocas fuentes,
manteniéndose fieles a esta distinción espuria que hace Unicode, incluyen dos diseños distintos para los dos
acentos. Mientras que el acento agudo (el \emph{oxia} del área politónica) aparece como un acento agudo normal y corriente,
el \emph{tonos}, por contra, queda casi (cuando no totalmente) perpendicular a la línea base de la letra. Este diseño, por
cierto, no es arbitrario sino que proviene de una moda, por no decir plaga, relativamente reciente en la tipografía
hecha en Grecia, como una especie de autoafirmación en el nuevo estilo monotónico frente al preterido politonismo. Al
margen de la cuestión meramente estética, y de que tal signo es difícilmente combinable con los espíritus y el
circunflejo, ni se le puede anteponer claramente un acento grave debido a esa verticalidad, el problema que salta a la
vista es que contamos con dos acentos agudos que pueden muy fácil acabar mezclándose en un texto griego, ya sea
monotónico o politónico. Y si vamos a componer en una fuente que distinga gráficamente el \emph{tonos} del \emph{oxia}, el aspecto
de nuestro texto también se volverá inconsistente.

¿Es habitual esta confusión de acentos? Sin ninguna duda: digamos que está a la orden del día. Por diversas causas, y
entre ellas no es la menos importante el hecho de que los teclados para escribir en griego politónico tienen casi
siempre mapeado el signo del \emph{tonos} a costa del \emph{oxia}. Y podemos hacer también una prueba muy sencilla. Copiar, como
he hecho yo, cualquier palabra tomada al azar de los textos del \href{http://www.perseus.tufts.edu/hopper/}{Perseus-Hopper} (por ejemplo, de Heródoto), que contenga
alguna letra con acento agudo, y la pegamos en un editor de texto que pueda interpretar el código hexadecimal de los
caracteres Unicode. Para ello yo uso Yudit. Y la palabra que he escogido, ἀπόδεξις. Vemos que en la pantalla de Yudit
(fig. \vref{fig:org6d2b017}), si situamos el cursor a la altura del signo «ό», se nos informa que se trata del carácter Unicode que
lleva el código \texttt{U+03CC}. Éste no es otro que el denominado en la jerga del estándar \emph{GREEK SMALL LETTER OMICRON WITH
TONOS}. Así pues, el \emph{Proyecto Perseo} está usando el \emph{tonos} y no el esperable \emph{oxia} en sus textos de griego antiguo.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/yudit.png}
\caption{\label{fig:org6d2b017}
Una prueba en el editor de texto plano Unicode Yudit}
\end{figure}

Si un texto así lo procesamos con una fuente que distinga gráficamente ambos acentos, se notará a simple vista la
diferencia. Por fortuna, son más las tipografías que usan un único signo para el \emph{tonos} y el \emph{oxia}.  Pero las de la
otra cuerda haberlas haylas, y no están por cierto entre las menos conocidas. De unas cuantas de éstas, muy populares,
podemos citar las Noto de Google o la Minion Pro de Adobe. En la fig. \vref{fig:org91868bd} se muestra cómo luciría la palabra
ἀπόδεξις en la Minion, tal y como está copiada de los textos del \emph{Proyecto Perseo}, con la ómicron y el \emph{tonos}. Justo
debajo, la misma palabra en la misma fuente, pero esta vez con el carácter de la ómicron y el oxia (\texttt{U+1F79}).

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/minion1.png}
\caption{\label{fig:org91868bd}
Contraste entre \emph{oxia} y \emph{tonos} en la fuente Minion Pro}
\end{figure}

Y la diferencia de inclinación se hace, si cabe, más evidente cuando comparamos (fig. \vref{fig:org5d4b55e}) la \emph{ómicron} con el
\emph{tonos} frente a la \emph{ómicron} con el espíritu suave y el \emph{oxia}.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/minion2.png}
\caption{\label{fig:org5d4b55e}
Contraste entre \emph{oxia} y \emph{tonos} en la fuente Minion Pro (2)}
\end{figure}

Otro ejemplo notable lo tenemos en la tipografía FiraGo, una \emph{sans} humanística auspiciada por la fundación Mozilla
(fig. \vref{fig:orgb95c2f6}).

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/tonos-oxia-firago.png}
\caption{\label{fig:orgb95c2f6}
Contraste entre \emph{tonos} y \emph{oxia} en la fuente FiraGo}
\end{figure}

\section{Una solución en LuaTeX}
\label{sec:orge27a4e0}
\begin{sloppypar}

Como se ve, si queremos trabajar en griego con alguna de estas fuentes no nos queda otra que normalizar el texto. Lo más
sensato sería procurar que todos los acentos agudos pasen a ser \emph{oxia}. Una forma de hacerlo es editar la fuente
mediante un \emph{software} como Fontforge. Pero si no queremos alterar nuestra fuente, se me ocurre una manera muchísimo más
simple si estamos componiendo en Lua(La)\TeX{}, que consiste en echar mano ---una vez más--- de la siempre utilísima
función Lua \texttt{fonts.handlers.otf.addfeature}, que nos permite (gracias a la inclusión de código Lua en nuestro
documento (La)\TeX{}) modificar o aplicar ciertas características OpenType al vuelo, como las de substitución de
caracteres. Así pues, podemos definir los reemplazos necesarios para las vocales con \emph{tonos}, bajo la nueva
característica OpenType que hemos bautizado «tonosoxia»:

\end{sloppypar}

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\directlua{
fonts.handlers.otf.addfeature{
name = "tonosoxia",
type = "substitution",
data = {
alphatonos = "ά",
epsilontonos = "έ",
etatonos = "ή",
iotatonos = "ί",
omicrontonos = "ό",
omegatonos = "ώ",
upsilontonos = "ύ",
                },
        }
}
\end{minted}

Y a continuación hacemos la prueba con la fuente FiraGo para comparar el texto con y sin conversión. A tal efecto,
definimos (con \texttt{fontspec}) dos familias para griego con FiraGo. Una, de base, que se comporte «normal»:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage{fontspec}
\newfontfamily\sinoxia{FiraGo}
\end{minted}

Y otra a la que le aplicaremos la nueva característica de substitución. Por tanto, convertirá automáticamente en la
compilación cualquier letra con \emph{tonos} en la misma letra con \emph{oxia}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\newfontfamily\oxia[RawFeature={+tonosoxia}]{FiraGo}
\end{minted}

Y, por último, en nuestro documento de prueba escribimos las dos series de vocales con \emph{tonos}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begin{document}

\sinoxia

ά έ ή ί ό ώ ύ

\oxia

ά έ ή ί ό ώ ύ

\end{document}
\end{minted}

Con lo que obtendremos el esperado resultado (fig. \vref{fig:org08ad392}) tras la compilación.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/tonos-oxia-luatex.png}
\caption{\label{fig:org08ad392}
Resultado de la compilación con la doble serie de acentos}
\end{figure}
\end{document}