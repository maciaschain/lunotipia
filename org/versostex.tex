% Created 2021-11-26 vie 16:36
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
	  \usepackage{normal-lua}
	 	 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Apunte sobre el tratamiento (estado de la cuestión) de los versos en TeX}}]{{\cabecera\MakeUppercase{Apunte sobre el tratamiento (estado de la cuestión) de los versos en TeX}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{Apunte sobre el tratamiento (y estado de la cuestión) de los versos en \TeX{}}
\begin{document}

\maketitle
\tableofcontents

\index{LaTeX!paquete verse}
\index{LaTeX!paquete gmverse}
\index{LaTeX!paquete microtype}
\index{LaTeX!paquete reledmac}
\index{tipografía del verso!centrado óptico del poema en la página}

Aunque creemos que es tema éste merecedor de un tratamiento algo más extenso, ensayaremos
de momento una suerte de \emph{estado de cosas} urgente en torno a la poesía versal en la
tipografía digital real, es decir, en \TeX{}\footnote{Nos ceñiremos, no obstante, sólamente al formato \LaTeX{}.}. Conviene aclarar una vez más, y antes de
empezar, que nos referimos a la composición tipográfica de los versos llamados \guillemotleft{}clásicos\guillemotright{},
donde por norma la tipografía suele aplicar de oficio una serie de procedimientos. El
poeta tiene siempre el derecho a desautomatizar tales protocolos. Quizás sea el único tipo
de autor que pueda jactarse de semejante bula. Y el tipógrafo, en el otro extremo, el
deber de escucharlo y dialogar con él. Pero al poeta también le cabe el deber de adquirir
una cierta noción del medio en que va a ser pubicado ---el objeto que llamamos \guillemotleft{}libro\guillemotright{}---,
y de no pedir caprichos que no sólo van más allá de las normas básicas y más razonables de
la tipografía, sino también de la naturaleza y de la física e incluso del sentido común.
En la otra orilla el tipógrafo, por supuesto, goza del derecho de advertirle de todo
aquello al poeta o, en última instancia, de ignorar sus demandas\footnote{Como por desgracia ha venido sucediendo en tantas otras cosas, el Microsoft Word ha
hecho estragos entre algunos poetas, lastrándolos de no pocos desvaríos tipográficos, de
los cuales probablemente estarían a salvo si empleasen un editor de texto en condiciones
o, al menos, una máquina de escribir. O incluso bolígrafo y papel. Les serviría también de
saludable penitencia.}.

\section{El macro paquete verse.sty}
\label{sec:org98c92df}
Esta \href{https://www.ctan.org/pkg/verse}{biblioteca de código}, firmada en origen por Peter Wilson para Herries Press, es sin
lugar a dudas la opción más completa que tenemos en el vasto ecosistema \LaTeX{} para el
tratamiento tipográfico serio de los versos: ya sea para componer libros enteros de poesía
o para incluir poemas en cualquier otro contexto, por ejemplo las citas con fragmentos de
poemas. Posee la gran virtud que siempre aplaudo en ciertos paquetes de \LaTeX{}, y que es su
carácter abstracto. Esto es, partiendo de una serie de macros básicas que hacen cada cual
una cosa y la hacen bien (como Unix manda), podemos extenderlo y adaptarlo a nuestras
necesidades particulares.

La pulpa del paquete no es otra que la redefinición del entorno \texttt{verse} de \LaTeX{},
reemplazando al que viene \guillemotleft{}de fábrica\guillemotright{}, que es bastante limitado e ineficiente para
componer versos. Éste, en efecto, y entre otras precariedades, nos sitúa el poema en un
margen fijo hacia la izquierda de la página. La nueva versión nos proporcionará un
argumento opcional donde poder declarar la anchura máxima entre la cual podrán expandirse
los versos. Algo muy útil y que nos permitirá llevar a cabo procedimientos esenciales como
el centrado óptico del poema en la página, como veremos a continuación.

Además de eso, \texttt{verse} cuenta con más funcionalidades útiles, como la numeración de versos
en el margen junto a la posibilidad de controlar su formato secuencia, el sangrado
automático de versos pares (entorno \texttt{altverse}), el encabalgamiento de versos o el
tratamiento automatizado de los versos que abarcan más de una línea.

El entorno \texttt{verse} viene a ser el mismo, en lo que hace a su estructura, que el que
proporciona \LaTeX{} de manera estándar. Al igual que en éste, los versos han de terminar con
el signo \texttt{\textbackslash{}\textbackslash{}}, salvo el último de cada estrofa. La separación estrófica, asímismo, se
marca con una línea en blanco. A dicha separación podemos asignarle desde nuestro
preámbulo un valor fijo (lo razonable sería lo equivalente a una interlínea:
\texttt{\textbackslash{}baselineskip} o 1.2em) o una longitud con propiedades elásticas (un \emph{glue}, en la jerga
de \TeX{}) dentro de unos márgenes de \emph{plus} y \emph{minus}.

\section{El centrado óptico del poema}
\label{sec:orga27882e}
El centrado óptico de los poemas en la página es de las cosas que más brillan por su ausencia en los libros de poesía
publicados hoy día (véase fig. \vref{fig:org609faf7}). En parte, porque los \guillemotleft{}maquetadores\guillemotright{} o editores lo desconocen; en parte
porque aquellos que sí saben de su existencia escurren grácilmente el bulto, pues estas técnicas hay que realizarlas a
mano y a ojímetro (tabuladores, cajas de texto y demás triquiñuelas) en InDesign, el programa que se usa
mayoritariamente para hacer libros y que no sabe hacer libros. Hay honrosas excepciones, ojo. Pero es una pena que
prefieran anclarse en un \emph{software} ineficiente e incompetente a todas luces y pagar desproporcionadas licencias
propietarias de uso antes que estudiar la tipografía digital real y los medios y el flujo de trabajo coherentes con
ella. Por supuesto, hay que estudiar y mucho, como para casi todo en esta vida, y tener curiosidad y no plegarse al
conformismo. Nadie espera pilotar un avión de buenas a primeras y por inspiración.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/versos_mal_bien.png}
\caption{\label{fig:org609faf7}Incorrecta vs. correcta disposición óptica del poema en el marco de la página}
\end{figure}

El paquete \texttt{verse} nos proporciona los recursos necesarios para poder realizar un centrado óptico \guillemotleft{}guiado\guillemotright{} del poema,
tomando como referencia la anchura del verso más largo. Este verso se añade, literalmente, como valor de la longitud
\texttt{\textbackslash{}versewidth} mediante la orden \texttt{\textbackslash{}settowidth}, justo antes de iniciar el entorno \texttt{verse}, al que le habremos pasado el
valor de \texttt{\textbackslash{}versewidth} como argumento opcional, para declarar que ésa es la anchura máxima del poema. Veamos un ejemplo
con las dos primeras estrofas de la \emph{Sonatina} de Darío:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
%declaramos el verso más largo como valor de |\versewidth|
\settowidth{\versewidth}{La princesa está triste... ¿Qué tendrá la princesa?}

\begin{verse}[\versewidth] % <== Aplicamos esta longitud como argumento opcional del
			   % entorno, indicando que se trata de la anchura del poema

La princesa está triste... ¿Qué tendrá la princesa?\\
Los suspiros se escapan de su boca de fresa,\\
que ha perdido la risa, que ha perdido el color.\\
La princesa está pálida en su silla de oro,\\
está mudo el teclado de su clave sonoro,\\
y en un vaso, olvidada, se desmaya una flor.

El jardín puebla el triunfo de los pavos reales.\\
Parlanchina, la dueña dice cosas banales,\\
y vestido de rojo piruetea el bufón.\\
La princesa no ríe, la princesa no siente;\\
la princesa persigue por el cielo de Oriente\\
la libélula vaga de una vaga ilusión.

\end{verse}
\end{minted}

Tras la compilación, el poema quedará centrado ópticamente entre sendos márgenes verticales de la página. Por
supuesto, la longitud \texttt{\textbackslash{}versewidth} es variable, y su valor se reiniciará cada vez que establezcamos un nuevo
\texttt{\textbackslash{}settowidth}.

\subsection{Una función para Gnu Emacs}
\label{sec:orge0f13e3}
Para facilitar la inclusión de estos comandos en el editor GNU Emacs escribí \href{https://maciaschain.gitlab.io/gnutas/versolargo.html}{esta función} en elisp que realiza lo
siguiente:

\begin{enumerate}[mienu]
\item En un poema señalado (marcado, en la jerga emacsiana) nos busque y copie el verso más largo;
\item Nos encierre ese poema dentro del entorno antes descrito, con el valor de ese verso añadido a \texttt{\textbackslash{}versewidth}.
\end{enumerate}

Este \href{https://vimeo.com/328937083}{breve vídeo} muestra todo el procedimiento en acción.

\section{El tratamiento de los versos que abarcan más de una línea}
\label{sec:org9f3b531}
Este es un escenario que puede estar abierto a muchas y variadas estrategias. Optar por una u otra dependerá en gran
medida del contexto, del tipo de poema, del libro que estamos componiendo, etc. El paquete \texttt{verse} ofrece una solución
que podríamos tildar de \guillemotleft{}clasica\guillemotright{}, y que consiste en sangrar la línea o líneas del verso desbordado desde el margen
izquierdo por un valor que se controla con la longitud \texttt{\textbackslash{}vindent}. Ésta es la opción que yo suelo preferir y, de hecho,
resulta casi la única posibilidad a escoger cuando el verso se desborda a lo largo de varias líneas. Pero, al margen de
qué camino sigamos, parece haber una premisa que se impone: si el verso se desborda es porque el poema se ha expandido
en algún lugar más allá de los márgenes de la caja de texto. La primera línea, por tanto, ha de ser siempre una línea
justificada a ambos márgenes. Y (aunque asumo que en esto puede haber controversia), no debemos huir en este contexto
del corte de palabras por guiones. Yo no lo hago. Ni \TeX{} ni el paquete \texttt{verse} tampoco. Será el mayor aliado que
tengamos aquí para facilitar la lectura. Y es que el habitual guionado sólo puede producir extrañeza cuando el lector no
está ejerciendo el rol que debe ejercer ante una página, es decir, el de leer el poema y no mirar el poema. Y el rol que
le toca a la tipografía (no debemos olvidarlo, señores \guillemotleft{}diseñadores gráficos\guillemotright{}) es el de guiar sutilmente, como un
Virgilio cualquiera, al lector hacia su cometido.

Por tanto, si entendemos que la primera línea del verso se desborda, ya no tendría sentido declarar un valor para el
verso más largo. En este caso, la longitud que debemos pasar como argumento opcional del entorno \texttt{verse} no será la
variable \texttt{\textbackslash{}versewidth} sino, directamente, \texttt{\textbackslash{}textwidth}. En el momento en que el verso desborde ese margen, \texttt{verse} ya
entiende el tipo de situación y se encargará de hacer los cálculos oportunos. Imaginemos un caso con un verso
palmariamente largo que pueda abarcar varias líneas. Así, partiendo de este código fuente, donde la cadena de texto
completa constituye un único verso (marcado al final con \texttt{\textbackslash{}\textbackslash{}}):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
  \begin{verse}[\textwidth]

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec
hendrerit tempor tellus. Donec pretium posuere tellus. Proin quam
nisl, tincidunt et, mattis eget, convallis nec, purus. Cum sociis
natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus. Nulla posuere. Donec vitae dolor. Nullam tristique
diam non turpis. Cras placerat accumsan nulla. Nullam rutrum. Nam
vestibulum accumsan nisl.\\

  \end{verse}
\end{minted}

Obtendremos tras compilar el resultado que se aprecia en la fig. \vref{fig:org63c1ca4}. La macro de \texttt{verse} habrá sangrado las líneas restantes
con el valor de \texttt{\textbackslash{}vindent}, que en este caso tengo fijado a 1em.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/versos-desbordados1.png}
\caption{\label{fig:org63c1ca4}Tratamiento de los versos que abarcan más de una línea con el paquete \texttt{verse.sty}}
\end{figure}

\subsection{Un tratamiento alternativo}
\label{sec:org67516a4}
Sin embargo, habrá ciertas situaciones en que la solución de sangrar las líneas
desbordadas desde el margen izquierdo no sea la más satisfactoria. Un caso muy típico se
da cuando es el propio poema el que ya nos viene con uno o varios versos sangrados, según
ha dispuesto el poeta. Corremos el riesgo de confundir muy fácilmente al lector, que
tendrá entonces que discernir más de una vez qué línea sangrada es continuación del
anterior verso y cuál constituye un verso autónomo. En tales pasajes creo que la opción
más aconsejable consiste en alinear el fragmento de verso desbordado a la derecha, tras un
corchete o claudátur de apertura. El paquete \texttt{verse.sty} no trae este procedimiento
automatizado \guillemotleft{}de fábrica\guillemotright{}, pero no es muy difícil ensayar un poco de código de alto nivel.
Podemos definir, por ejemplo, un comando como éste:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\newlength{\vdesbordado}
% la anchura del makebox es el ancho de texto menos el valor de vindent (1em)
\def\vresto#1{\makebox[\textwidth-1em][r]{[#1}}
\end{minted}

Y encerrar en nuestro comando \texttt{\textbackslash{}vresto\{...\}} el tramo de verso que en una primera compilación nos resulta desbordado. El
resultado de la compilación puede verse en la fig. \vref{fig:org09499ac}.

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begin{verse}[12cm]
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit \vresto{tempor tellus.}\\
Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis eget,
convallis nec, purus. vestibulum accumsan nisl.\\
\end{verse}
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/versos-desbordados2.png}
\caption{\label{fig:org09499ac}Tratamiento de los versos desbordados con el procedimiento del claudátur}
\end{figure}

\section{Conflictos de verse.sty con otros paquetes}
\label{sec:orge1c22fc}
Naturalmente, uno habla aquí de los paquetes que más suele utilizar, y a partir de la experiencia puedo afirmar que
\texttt{verse.sty} es un código bastante estable y que no suele acusar problemas de convivencia con otros macro paquetes.
Sin embargo, hay dos en concreto donde se produce un choque frontal: \texttt{microtype} y el paquete para componer ediciones
críticas filológicas \texttt{reledmac}.

\subsection{El caso de microtype}
\label{sec:org8e206d6}
No es especialmente traumático, dado que podemos cargar ambos paquetes en el preámbulo sin que suceda ninguna
catástrofe. El único problema menor es que el entorno \texttt{verse} está basado en el entorno \texttt{list}, donde las esotéricas y
microtipográficas propiedades de \texttt{microtype} no tienen efecto alguno. ¿Debe quitarnos esto el sueño? Sí ---o, al menos,
moderadamente sí--- en caso de que juzguemos al margen izquierdo del poema con la misma legitimidad que tiene el margen
real de la caja de texto en la página para admitir el alineamiento óptico o \emph{protrusion}. Pero hay un contexto que nos
puede desvelar aún más, y es aquél donde queramos aplicar la técnica del \guillemotleft{}colgado de diacríticos\guillemotright{} para la poesía en griego
politónico, que supone un caso extremo del alineamiento óptico de márgenes y del cual hablamos en la anterior entrada sobre
\href{https://maciaschain.gitlab.io/lunotipia/cavafis\_sarpedon.html}{\emph{El funeral de Sarpedón} de Cavafis}. Por suerte, existe una solución, no todo lo elegante y limpia que nos gustaría,
pero que funciona: el \emph{hacking} que sugiere el autor de \texttt{microtype} para el entorno \texttt{list} y que, por consiguiente,
también resulta satisfactorio para el entorno \texttt{verse}. Si el lector desea más información, le remito a la mencionada
entrada.

\subsection{El caso de reledmac}
\label{sec:org3313627}
Aquí ya entramos de lleno en el drama, pues el entorno \texttt{verse} sencillamente no funcionará dentro de una sección numerada
de \texttt{reledmac} (o sea, el texto editado propiamente dicho de una edición crítica) y entregará un error grave de
compilación. Cabe decir que \texttt{reledmac} cuenta con sus propios recursos para la composición de poesía versal, pero no es
la clase de composición que aquí nos interesa, ya que consiste en un tratamiento del verso destinado a la edición
crítica de poesía, especialmente de largas tiradas de versos, donde se siguen unos protocolos algo distintos de los que
aquí estamos comentando, que es la edición de la poesía como un objeto artístico y un \emph{corpus} literario para ser leído,
más que un texto \guillemotleft{}crítico\guillemotright{} anotado filológicamente, tal y como lo presentaría \texttt{reledmac}.

Aun así, dentro de estos textos anotados puede haber pasajes donde se citen unos versos o un poema entero, y en tal caso
es preciso componer esa parte atendiendo a la norma del centrado óptico, si bien es común en las ediciones críticas que
no se añada epacio antes ni después de los versos. Éstos, al formar parte del texto anotado filológicamente, entrarían
en el cómputo de la numeración de lineas. Un ejemplo de lo dicho lo encontramos en esta página de Teubner, donde el
autor editado (Dionisio de Halicarnaso) cita un fragmento de Safo:

\begin{figure}[!h]
\centering
\includegraphics[width=0.7\textwidth]{./images/Ejemplo_versocitado_Teubner.jpg}
\caption{\label{fig:orgbfa6d1b}Disposición con centrado óptico de unos versos de Safo en una edición de Teubner}
\end{figure}

Como dije, no podemos usar dentro de una región textual donde actúa \texttt{reledmac} el entorno \texttt{verse}. Se me ocurrió, sin
embargo, una solución definiendo el siguiente comando, que he puesto en práctica con óptimo resultado en una edición
crítica del \emph{Calírroe} de Caritón, trabajo que terminé recientemente (fig. \vref{fig:orgdea7892}). Para que este comando funcione correctamente,
necesitamos cargar antes el paquete \texttt{calc}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage{calc} %%%%%%%%%%%%%%%%%% se necesita para lo de los versos
\newlength{\verso} % anchura variable: el v. más largo del poema
\newlength{\versob} % anch. variable: es siempre el ancho de texto menos el verso más largo
\def\versolargo#1{\settowidth{\verso}{#1}}
\newenvironment{edpoema}%
{\setlength{\versob}{\textwidth-\verso}%
  \leftskip=0.5\versob %divide en dos para ambos márgenes
  \parindent=0pt % elimina la sangría de primera línea
}
{\par}
\end{minted}

La anchura variable \texttt{\textbackslash{}verso}, que va a definir siempre el verso más largo del pasaje citado, nos puede resultar útil
también para este otro comando destinado a los versos que comienzan \emph{in media re}, y que por tanto deben alinearse a la
derecha (según convenciones de las ediciones críticas), tomando como margen el de dicho verso más largo:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\def\finverso#1{\makebox[\verso][r]{#1}}
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=0.8\textwidth]{./images/resultado-cariton.png}
\caption{\label{fig:orgdea7892}Detalle del centrado óptico de los versos en una edición crítica compuesta por el autor de este artículo}
\end{figure}

\section{El macro paquete gmverse.sty, ¿una alternativa a verse.sty?}
\label{sec:orge151c5b}
La \href{https://www.ctan.org/pkg/gmverse}{biblioteca de código} \texttt{gmverse}, escrita por Grzegorz Murzynowski y cuya última versión
se remonta al 2008, pretende ser una redefinición completa y desde cero del entorno
\texttt{verse}, lo cual implica que ya no estamos ante un entorno, en este caso, dependiente del
entorno \texttt{list}. Aparte de eso, el paquete aporta una serie de características novedosas,
pero destacaremos aquí dos que son especialmente interesantes: el centrado óptico
\guillemotleft{}automático\guillemotright{} y el tratamiento, igualmente \guillemotleft{}automático\guillemotright{}, de los versos que desbordan la
línea, realizado mediante la técnica del alineamiento a la derecha y el corchete.

\subsection{Centrado óptico automático}
\label{sec:orgfae0206}
Junto a un centrado óptico guiado al estilo de \texttt{verse} (en este caso, mediante el comando \texttt{\textbackslash{}versecenterdue\{...\}}, en
que se encerraría el verso más largo del poema a centrar), la característica de un centrado óptico que se
ejecute de manera automática es ciertamente innovadora. No contento con eso, además, el autor aporta cuatro variantes,
cuyos entresijos técnicos y matemáticos explica en la \href{https://osl.ugr.es/CTAN/macros/latex/contrib/gmverse/gmverse.pdf}{documentación del paquete}\footnote{Documentación que, dicho sea de paso, no es precisamente un ejemplo de claridad de cara a un público más general,
y en la que a menudo se hace necesario navegar por el código para llegar a entender las diversas funcionalidades o
enterarnos de su existencia.}: \texttt{\textbackslash{}vocweigh1}, \texttt{\textbackslash{}vocweigh2},
\texttt{\textbackslash{}vocweigh3} y \texttt{\textbackslash{}vocweigh8}.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{./images/gmverse.png}
\caption{\label{fig:org456c794}Un detalle de la documentación de \texttt{gmverse.sty} donde se explica el centrado óptico automático}
\end{figure}

Sin embargo, he de decir que ninguna de las cuatro opciones logra centrar el poema de manera perfecta. Todas fallan
(unas más que otras) por un pequeño margen positivo o negativo, si comparamos (fig. \vref{fig:org006b7ad}) sus resultados con los del centrado
guiado, que son los correctos.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{./images/comparacion-centrados.png}
\caption{\label{fig:org006b7ad}Comparación del centrado óptico automático con el centrado \guillemotleft{}guiado\guillemotright{}}
\end{figure}

\subsection{Tratamiento automático de los versos desbordados}
\label{sec:org24633d0}
El comando \texttt{\textbackslash{}versehangright}, es el que se encarga del alineamiento a la derecha del verso que desborda la línea. Es la
opción por defecto. De hecho, es la única opción para este tipo de contextos; es decir, que ni existe un
\texttt{\textbackslash{}versehangleft} ni debemos esperarlo. Si deseamos, además, que la línea desbordada se encierre en el típico claudátur,
tendremos que declarar antes el comando \texttt{\textbackslash{}versehangrightsquare}. Ahora bien, como ya dijimos más arriba, los casos de
verso desbordado implican que el poema se ha llegado a expandir en un punto determinado más allá de los márgenes
izquierdo y derecho de la caja de texto, así que la primera línea del verso ha de estar justificada a ambos márgenes.
Por defecto, \texttt{gmverse} aplica una pequeña sangría a todo el poema desde el margen izquierdo, cuyo valor controla el
comando \texttt{\textbackslash{}verseleftskip} y que debemos poner previamente a cero: \texttt{\textbackslash{}verseleftskip=0pt}. De todas formas, por el margen
derecho también se evita la justificación completa, como se puede apreciar en el \href{https://vimeo.com/342279901}{breve vídeo} que he hecho para ilustrar
el procedimiento.

\subsection{Uso de gmverse conjuntamente con verse}
\label{sec:orgfdd35ad}
En teoría sería posible, según la documentación del paquete, si añadimos la opción \texttt{local} al cargarlo en el preámbulo.
En este caso, las macros de \texttt{gmverse} no se aplicarían por defecto y habría que declararlas previamente. No obstante, si
bien el tratamiento de los versos desboradados en versión de \texttt{gmverse} funciona correctamente y puede convivivir sin
problemas con el de \texttt{verse}, la opción del centrado automático del primero se comporta de forma errática si la activamos
de manera local.

\section{En suma}
\label{sec:org4d631ee}
La poesía constituye un caso muy particular dentro de la composición tipográfica y la
producción de libros. Pero en ese quehacer también representa un perfecto ejemplo de lo
que podríamos denominar la capa semántica de la tipografía, que se movería en una órbita
superior a su capa física o pragmática. Allí mismo, más abajo, el tipógrafo (incluyendo a
nuestro tipógrafo binario \TeX{}) sólo ve palabras, líneas y párrafos. Pero estas líneas, en
ocasiones, han de entenderse también como \emph{versos}. \LaTeX{}, que es la cara semántica de
\TeX{}, lo comprende estupendamente, y por eso se convierte (como en cualquier otro aspecto
de la composición libresca) en la perfecta herramienta y en el mejor aliado del tipógrafo
digital. Por eso mismo, también, un entorno de \LaTeX{} tenía que acabar llamándose \texttt{verse}.

Por otra parte, paquetes de factura más reciente como \texttt{gmverse.sty} han aportado novedades
muy interesantes. Si bien es una lástima que su desarrollo haya quedado algo estancado. En
todo caso, el código está ahí, abierto y al alcance de todos. No hace falta insistir en
que el ecosistema \TeX{} es conocimiento libre. Y probablemente no se haya dicho la última
palabra en este vasto ámbito respecto al centrado óptico automático de los poemas.

\section{Actualización de 26/11/21: una función en Lua para los versos con claudátur}
\label{sec:org51d9ccf}
Una de las características esenciales de LuaTeX, y la que precisamente da nombre a esta
versión del motor tipográfico, es la de poder acceder a los entresijos de \TeX{} mediante
\emph{scripts} en Lua, de una forma limpia, simple y quirúrgica, y asociarlos a distintos
\emph{callback} o grupos especializados de funciones de forma que se ejecuten en puntos
determinados del proceso de compilación. Dado que contamos con un \emph{callback} llamado
\texttt{post\_linebreak\_filter}, o sea, específico para todo código que ha de entrar en acción
cada vez que \TeX{} corte una línea, se me ocurrió escribir y asociarle esta función, para
que aquellos versos desbordados se ajusten automáticamente a la derecha, tras un
claudátur, según el procedimiento que vimos en \vref{sec:org67516a4}. La ventaja es que
funciona bien con el paquete \texttt{verse} y supone una solución alternativa y automatizada para
los versos desbordados, que ya hemos visto que el paquete \texttt{verse} los trata mediante
sangrías.

Primeramente, debemos definir una nueva caja para nuestro claudátur:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\newbox\claudatur

\setbox\claudatur=\hbox{[}
\end{minted}

Nuestra función en Lua, encerrada en el entorno \texttt{luacode}, del paquete del mismo nombre, pintaría tal que así:

\begin{minted}[frame=lines,linenos=true,breaklines]{lua}
versos_desbordados = function(head)
   local last_line = nil
   for line in node.traverse_id(node.id("hlist"), head) do
      if last_line then
	 local has_userpenalty = false
	 for pen in node.traverse_id(node.id("penalty"), last_line.list) do
	    if not has_userpenalty then
	       has_userpenalty = (pen.subtype == 0 and pen.penalty == -10000)
	    else
	       break
	    end
	 end
	 if not has_userpenalty then
	    line.head = node.insert_before(line.head,line.head,node.copy(tex.box.claudatur))
	    x = node.new("glue")
	    x.stretch = 2^16
	    x.stretch_order = 3
	    line.head = node.insert_before(line.head,line.head,x)
	    x = node.new("glue")
	    x.stretch = 2^16
	    x.stretch_order = 2
	    line.head = node.insert_before(line.head,line.head,x)
	 end
      end
      last_line = line
   end
   return head
end
\end{minted}

Y ya sólo nos quedaría definir dos comandos para activar y desactivar la función a
voluntad, es decir, para asociarla y desligarla del \emph{callback}, respectivamente:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\newcommand\onclaudatur{%
  \directlua{luatexbase.add_to_callback("post_linebreak_filter",
    versos_desbordados, "versos_desbordados")}

\newcommand\offclaudatur{%
  \directlua{luatexbase.remove_from_callback("post_linebreak_filter",
    "versos_desbordados")}
\end{minted}

Podemos poner el código en práctica con estos versos de Auden, y los márgenes adecuados para
garantizar el desbordamiento de algunos versos. El resultado de la compilación, en la fig.
\vref{fig:org27cc333}.

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\onclaudatur
\poemtitle{In memory W.B. Yeats (W.H. Auden, fragmento)}
\begin{verse}
But for him it was his last afternoon as himself,\\
An afternoon of nurses and rumours;\\
The provinces of his body revolted,\\
The squares of his mind were empty,\\
Silence invaded the suburbs,\\
The current of his feeling failed; he became his admirers.

Now he is scattered among a hundred cities\\
And wholly given over to unfamiliar affections,\\
To find his happiness in another kind of wood\\
And be punished under a foreign code of conscience.\\
The words of a dead man\\
Are modified in the guts of the living.

But in the importance and noise of to-morrow\\
When the brokers are roaring like beasts on the floor of the bourse,\\
And the poor have the sufferings to which they are fairly accustomed\\
And each in the cell of himself is almost convinced of his freedom\\
A few thousand will think of this day\\
As one thinks of a day when one did something slightly unusual.

What instruments we have agree\\
The day of his death was a dark cold day.\\
\end{verse}
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=0.6\textwidth]{./images/yeats.png}
\caption{\label{fig:org27cc333}Versos desbordados con claudátur (ejemplo con un poema de Auden)}
\end{figure}
\end{document}