% Created 2020-09-17 jue 02:31
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Didot, /aplá/ y el bálsamo de la tradición}}]{{\cabecera\MakeUppercase{Didot, \emph{aplá} y el bálsamo de la tradición}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{Didot, \emph{aplá} y el bálsamo de la tradición}
\begin{document}

\maketitle
\tableofcontents

Ciertas corrientes en la producción actual de libros, que podríamos tildar de vandálicas o
iletradas, parecen disfrutar de un placer malsano en abolir las tradiciones tipográficas a
golpe de ego. Y cuando el ego del productor del libro, inflado, estorba entre el autor y
el lector se rompe ese antiguo contrato social mediador de ambas orillas, que es la
esencia de la tipografía y que garantiza su benefactora invisibilidad. La \guillemotleft{}originalidad\guillemotright{},
lo hemos dicho muchas veces pero no importa insistir una más, es un veneno en la
tipografía del libro. El otro veneno, casi concatenado, suele ser la salmodia del diseño
gráfico, que alimenta hasta límites insospechados la arrogancia del, ya no tipógrafo, sino
\guillemotleft{}diseñador\guillemotright{}. Éste, habitual analfabeto digital y analógico, o bien desconoce las
elementales normas de composición que le ha legado la tradición, o bien se siente tan
venido arriba que se cree en potestad de inventarse toda la tipografía de golpe él solo, a
base de una infantil, casi automática transgresión. Sin ir más lejos, hace poco me topé
con un libro cuya tabla de contenido estaba dispuesta de tal forma que generaba en el
lector (es decir, en mí) la desagradable sensación de que todo ese índice estaba del
revés. No sé si era eso lo que pretendía nuestro \guillemotleft{}diseñador\guillemotright{} de turno. Probablemente no,
pero seguro que le pareció divertido jugar con unos márgenes lisérgicos e incluso le
extrañaría por qué nadie lo había hecho antes. Cuánto mejor si tan sólo hubiese tenido en
cuenta que el lector (es decir, yo) que acude a una tabla de contenido lo que quiere es
eso: hacerse una idea del contenido y de las páginas en que éste se despliega. No
sorpresas. No mareos. No fiestas.

En todo caso, entiéndase: no estamos afirmando que sea ilícito buscar nuevas vetas en la
tipografía del libro; ni que en ella esté ya todo definitivamente dicho. Pero para minar
tales vetas no es necesario ---ni aconsejable--- dinamitar lo colindante o precedente. Se
impone siempre un equilibrio. Entre lo nuevo y lo que ha funcionado siempre. Y, sobre
todo, se hace necesario comprender cómo y de qué manera ha funcionado. La tradición
tipográfica, que es un conjunto de normas y de rutinas, no hay que verla como una rémora
sino como un inagotable tesoro.

Dentro de estas alhajas heredadas daremos aquí unos breves trazos de una en especial ---y
de un caso concreto en la tradición vernácula de la tipografía griega--- que podríamos
denominar los \guillemotleft{}tipos estándar\guillemotright{}. Es decir, aquellos tipos o familia de letras que se
convierten en los habituales para componer un texto y que el lector de libros, de alguna
forma inconsciente, ya los espera, pues los tiene tan asimilados como el rumor del mar en
la costa o el runrún del tráfico en su ciudad.

\begin{figure}[htp]
\centering
\includegraphics[width=.9\linewidth]{./images/himno_aristoteles.png}
\caption{\label{fig:orgf301ff8}Un pasaje de \emph{Hymne D'Aristote a la vertu. Traduit en vers français par Firmin Didot}, editado en París por Firmin Didot en 1832 (obsérvese las típicas variantes contextuales de la \emph{beta} inicial y media)}
\end{figure}

Los tipos estándar son un concepto variable. Así, en la tradición tipográfica anglosajona
puede recaer no en una sino en varia familias, dependiendo de la época, el lugar e incluso
el vaivén de las modas. Tipos como Bembo o Caslon bien pueden asumir tal rango, por citar
un par de nombres célebres. Y, en tiempos más recientes, la Times Roman. Pero si hablamos
de la tipografía griega hecha en Grecia, al menos hasta el siglo \miscrom{XX} hay un nombre que
brilla en justicia por sobre todos: Didot.

Como ya referimos en esta \href{https://lunotipia.juanmanuelmacias.com/griegos\_del\_rey.html}{historia de la tipografía griega para impacientes}, la
representación libresca de la lengua griega se desarrolló y maduró durante un largo y
fructífero período de exilio, y no llegaría a su Ítaca particular, a Grecia, hasta entrado
el siglo \miscrom{XIX}. Hasta entonces, y promovida por escribas y tipógrafos griegos,
igualmente exiliados, sus centros de influencia estaban en Venecia, París y Viena,
principalmente.

\begin{figure}[htp]
\centering
\includegraphics[width=.9\linewidth]{./images/manucio.png}
\caption{\label{fig:org8231df5}Un ejemplo de convivencia de tipos Didot latinos y griegos en \emph{Alde Manuce et L'Hellénisme a Venisse}, publicado por Firmin Didot en 1875}
\end{figure}

Cuando llegó la imprenta a Grecia, que se hallaba aún bajo el yugo turco, las páginas
impresas de la escritura griega habían alcanzado ya un notable estado de madurez; de
mayoría de edad tipográfica, podríamos decir, en que, gracias a los primeros hallazgos de
Bodoni y, sobre todo, a la posterior revolución a cargo de Didot, ya se decía adiós
definitivamente al estilo caligráfico. Ambroise-Firmin Didot había conseguido, en efecto,
dotar a las letras griegas de un temperamento propio, decididamente tipográfico, sin
servidumbre alguna de los trazos y formas latinas, sin resto de las florituras del
escriba. No es de extrañar que desde esa atalaya privilegiada y vanguardista, fuera
también Didot el mentor del regreso (encuentro primerizo, más bien) de la tipografía
griega a su patria real y legítima.

\begin{figure}[htp]
\centering
\includegraphics[angle=90,width=.9\linewidth]{./images/vamvas.png}
\caption{\label{fig:orgcc4e22c}Una página de la \emph{Gramática griega} en demótico de Neofitos Vamvas, compuesta en Didot}
\end{figure}

Con el abundante material y tipos, en efecto, que hizo mandar Didot hacia Quíos (el primer
deseo del impresor francés era Atenas, pero se desestimó a causa de la situación
política), pudo imprimirse allí la \emph{Gramática griega} en griego demótico del erudito
Neófitos Vamvas, en 1821 (fig. \vref{fig:orgcc4e22c}). La nueva imprenta no tardó en ser destruida
por los turcos, y a su vez no tardó Didot en donar nuevo material, cuyo destino fue ahora
la isla de Hidra. Allí se emprendió (1824) la impresión del periódico \href{https://el.wikipedia.org/wiki/\%CE\%9F\_\%CE\%A6\%CE\%AF\%CE\%BB\%CE\%BF\%CF\%82\_\%CF\%84\%CE\%BF\%CF\%85\_\%CE\%9D\%CF\%8C\%CE\%BC\%CE\%BF\%CF\%85}{\emph{Ο Φίλος του Νόμου}}
(\emph{El amigo de la ley}), de una efímera existencia de tres años, pero de un poso decisivo.

\begin{figure}[htp]
\centering
\includegraphics[width=0.7\textwidth]{./images/o-filos.png}
\caption{\label{fig:org7bb21e3}Primera página del periódico \emph{Ο Φίλος του Νόμου}, compuesta en Didot}
\end{figure}

\section{Los tipos \emph{aplá}}
\label{sec:orge777f80}
Gracias al trascendente impulso didotiano la tipografía griega, ya por fin en Grecia, fue
escalando y adquiriendo su personalidad propia. La influencia francesa fue siempre
innegable (¿quién en su sano juicio renunciaría a ella?) pero los impresores griegos
también supieron construir poco a poco una tradición que ya era, y con toda justicia,
vernácula. Y aún nos atreveríamos a añadir esto que sigue: desde que Grecia se puso por
fin al frente de su propia tipografía para representar la lengua que le es propia, la
representación del griego fuera de la Hélade debe seguir ya las normas de la tipografía
hecha allí, que conjunta lo mejor del acervo acumulado durante su larga historia
\guillemotleft{}apátrida\guillemotright{} con los nuevos establecimientos y prescripciones que dictaron los peritos
impresores griegos\footnote{Por ejemplo, véase \href{https://lunotipia.juanmanuelmacias.com/comillasgriegas.html}{esta entrada} que aquí dedicamos al caso de las comillas en la
tipografía griega.}.

\begin{figure}[htp]
\centering
\includegraphics[width=0.7\textwidth]{./images/galatia.png}
\caption{\label{fig:org8f7d5c6}Detalle de una página de \emph{Η Μεγάλη Ελλάς}, de Galatia Kazantzaki, publicado en Atenas en 1927 y compuesto en Didot}
\end{figure}

Y es lícito llamar la atención sobre la letra que se convirtió en feliz constante a lo
largo de esta joven pero fructífera tradición: los tipos griegos que definiera Firmin
Didot. No tardaron en erigirse en un estilo propio y netamente reconocible, un estándar
\emph{de facto} que los impresores del país acabaron denominando \emph{aplá}, esto es, \guillemotleft{}sencillo\guillemotright{},
\guillemotleft{}plano\guillemotright{}, \guillemotleft{}simple\guillemotright{}. Con la implantación del uso de la monotipia a finales del \miscrom{XIX} y
principios del \miscrom{XX} llegó la culminación de este estilo, por obra de los habilidosos
monotipistas helenos y la asunción de la versión didotiana que distribuía la casa
Monotype, la célebre Greek 90\footnote{Véase \href{http://web.archive.org/web/20120229131933/http://omega.enstb.org/yannis/pdf/boston99.pdf}{From Unicode to Typography, a Case Study the Greek Script}, de Yannis Haralambous.}, con la que se compusieron páginas griegas de una
claridad diáfana y limpia, hermosas y paradigmáticas.

\begin{figure}[htp]
\centering
\includegraphics[width=.9\linewidth]{./images/periodico.png}
\caption{\label{fig:orgfdbbda4}Detalle del periódico \emph{Ανθολογία} (1837), compuesto en Didot. Resalto dos ejemplos de caracteres contextuales alternativos de \emph{beta} media e incial}
\end{figure}

Ahora bien, el estilo \emph{aplá} no sólo se encontró con el reto de ir definiendo un espíritu
autóctono para la representación tipográfica de la lengua griega. Había por delante otro
desafío acaso más profundo, y era el de representar con caracteres griegos por primera vez
una lengua viva, algo prácticamente exótico para los impresores en griego fuera de Grecia,
más atentos a la representación del griego antiguo y, por regla general, en un marco
académico y filológico. Un hito particular de este trance fue el caso de la cursiva, cuyo
uso enfático no es pertinente a la hora de representar el griego antiguo, pero se hace
necesario en una lengua viva de Europa. Por supuesto, ya había cursivas de reconocida
solvencia que en la impresión del griego antiguo se empleaban, no como énfasis
(insistimos), sino como variante de estilo del texto principal. Por un lado estaba la
británica \href{https://lunotipia.juanmanuelmacias.com/porson.html}{Porson} de las ediciones clásicas oxonienses; por otro, la familia de tipos
\emph{lipsiakó}, inspirados en la Didot recta, nacidos en torno a Leipzig y las ediciones de
Teubner, principalmente. Este último estilo se convirtió en Grecia en la cursiva habitual
del \emph{aplá}, especialmente en la versión de Monotype denominada Greek 91. Pero también hubo
casos en que se aplicó la Porson con gran maestría y equilibrio.

\section{El estilo \emph{aplá} en la era digital}
\label{sec:org227d46b}
Por supuesto, dejada atrás la composición mecánica y la fotocomposición, y ya en el
nebuloso e incierto ámbito de la llamada \guillemotleft{}autoedición\guillemotright{}, tanto dentro y fuera de Grecia se
desarrollaron incontables fuentes TrueType o PostScript de inspiración didotiana, pero
generalmente de baja calidad y sin el refinamiento de sus ancestros mecánicos; en parte,
por las limitaciones del formato; en parte también por el poco generoso rigor histórico de
los desarrolladores. Una excepción, y un oasis, lo constituía el ecosistema \TeX{}, y
especialmente \texttt{metafont}, cuya tecnología de fuentes escalables se anticipó en muchos
lustros a la de las fuentes TrueType, e incluso a la de las OpenType. Así, cabe señalar el
espíritu decididamente Didot de la Computer Modern para representar griego.

Con la llegada de las fuentes OpenType y de diseñadores más peritos, el panorama para los
amantes de la Didot (entre los que se cuenta quien escribe este textito) ha mejorado
considerablemente, si bien estamos algo lejos aún de la excelsa perfección y gracia de
la Greek 90 de Monotype. Entre las aproximaciones actuales (a fecha de estas líneas,
entiéndase) me atrevería a citar dos muy conseguidas, todas ellas de licencia libre: La
\href{https://greekfontsociety-gfs.gr/typefaces/19th\_century}{GFS Didot Classic}, desarrollada bajo el paraguas de la \emph{Sociedad de fuentes griega}, y los
tipos \href{https://www.ctan.org/pkg/oldstandard}{Old Standard}, diseñados por el profesor Alexej Kryukov y mantenidos y enriquecidos
en la actualidad por Robert Alessi, Nikola Lečić y Bob Tennent.

Ambas tienen, naturalmente, sus particularidades. La de la Greek Font Society acusa una
inspiración más cercana a la Didot de Monotype, con la caja de sus caracteres algo más
ancha. Pero, por otra parte, la elección de un estilo Palatino para su juego de caracteres
latinos y sus signos de puntuación nos parece completamente desafortunada y sin ningún
correlato en la tradición dentro y fuera de Grecia que lo justifique. Si tuviese que
quedarme con una de ambas, me decantaría claramente por la segunda, la Old Standard, de
diseño muchísimo más cuidado y sustentado por un gran conocimiento histórico. Diríamos que
se trata de una Didot de tradición más filológica, aunque su empleo es perfectamente
factible y legítimo en una gran variedad de escenarios. Digna de mención aparte es su
delicadísima y bella cursiva, claramente de inspiración lipsiaka\footnote{Véase \href{https://lunotipia.juanmanuelmacias.com/cavafis\_sarpedon.html}{nuestro \guillemotleft{}experimento\guillemotright{}} con un poema de Cavafis y la cursiva \emph{lipsiakó} de la Old Standard.}.

\begin{figure}[htp]
\centering
\includegraphics[width=.9\linewidth]{./images/angelis.png}
\caption{\label{fig:orgd202925}Página de un artículo de Dimitris Angelís, compuesto por mí en Old Standard, perteneciente al Volumen de homenaje a la profesora Penélope Stavrianopúlu (Berlín 2013)}
\end{figure}


\section*{Fuentes de las imágenes}
\label{sec:org117f9f2}
\begin{description}
\item[{Fig. \vref{fig:orgf301ff8}}] \url{https://archive.org/}
\item[{Fig. \vref{fig:org8231df5}}] \url{https://archive.org/}
\item[{Fig. \vref{fig:orgcc4e22c}}] \url{https://fdocument.org}
\item[{Fig. \vref{fig:org7bb21e3}}] \emph{Wikimedia commons}
\item[{Fig. \vref{fig:org8f7d5c6}}] \url{https://dimartblog.com}
\item[{Fig. \vref{fig:org8f7d5c6}}] \url{http://spaniasyllektika.blogspot.com}
\end{description}
\end{document}