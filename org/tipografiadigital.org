#+TITLE: Tipografía digital: la (eterna) asignatura pendiente
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:t" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:nil" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (filtro-html-misc)"))
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
# +STARTUP: inlineimages
#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Exportación a LaTeX
#+LaTeX_CLASS: normal-lua-org
#+LaTeX_Header: \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
#+LaTeX_HEADER: \let\maketitleold\maketitle
#+LaTeX_HEADER: \def\maketitle{{\fontspec{Arial}\maketitleold}}
#+LaTeX_HEADER: \let\tableold\tableofcontents
#+LaTeX_HEADER: \def\tableofcontents{\tableold\bigskip}
#+LaTeX_HEADER: \def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
#+LaTeX_HEADER: \newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
#+LaTeX_HEADER: \newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Tipografía digital: la (eterna) asignatura pendiente}}]{{\cabecera\MakeUppercase{Tipografía digital: la (eterna) asignatura pendiente}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
#+LaTeX_HEADER: \pagestyle{base}
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage[spanish]{varioref}
#+LaTeX_HEADER: \renewcommand{\listingscaption}{Cuadro de código}

#+INDEX: tipógrafos, desarrolladores y teóricos!Hermann Zapf
#+INDEX: tipógrafos, desarrolladores y teóricos!Donald Knuth

{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none
  ;; Para LaTeX

  (defun filtro-latex-misc (texto backend info)
    "reemplaza el coamndo \ref por \vref y forma nums. romanos con versalitas"
    (when (org-export-derived-backend-p backend 'latex)
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "\\\\miscrom{\\1}"
				(replace-regexp-in-string "\\\\ref{"  "\\\\vref{" texto))))

  ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

#+begin_export html
<form>
<p style="text-align: center;"><button formaction="https://maciaschain.gitlab.io/lunotipia/pdf/tipografiadigital.pdf"><font size="2">Descargar esta entrada en formato PDF</font></button></p>
</form>
#+end_export

#+CAPTION: El prof. Donald Knuth (izquierda) junto a Hermann Zapf (derecha)
#+NAME: zapfknuth
[[./images/ZapfKnuth.jpg]]

  La tipografía digital no es sino la continuación de la tipografía mecánica o "tradicional" mediante medios digitales.
  Aspira a sintetizar, reproducir y automatizar aquellos procesos y rutinas de los viejos impresores mediante la
  programación y la potencia de cálculo de una computadora. Se trata, por tanto, de una evolución natural que suma y no
  resta, y que edifica sobre las bases de un conocimiento heredado. Algoritmos tan sofisticados como el Plass-Knuth de TeX
  para el corte de líneas y la composición de párrafo sistematizan la pericia de los cajistas. El propio TeX podría
  definirse (lo hemos hecho así a menudo) como un cajista binario. Y Hermann Zapf acudió a la /Biblia/ de Gutenberg para
  sus teorías de escalado horizontal y márgenes ópticos, que aplicaría el /hz/-Program de Peter Karow y recogerían luego,
  renovadas, pdfTeX y LuaTeX.

  Dicho lo cual, podríamos pensar en justicia que la tipografía que se hace en estos tiempos que corren, con dos
  decenios casi cumplidos del siglo v-XXI-v, es tipografía digital. Todo parecería indicarlo, habida cuenta de que la
  práctica totalidad del grueso del material de impresión que reciben las imprentas consiste en ese producto etéreo que
  llamamos PDF y que se genera en un ordenador. Lo malo es que esta época de avances tecnológicos, presuntos muchas
  veces, también lo es de una inusitada frivolidad, de la cual no se escapa ni el término "digital", empleado con pareja
  frivolidad por doquier. Y es que lo digital es el lema de nuestro tiempo, de hecho. Lo define todo y todo acaba
  siendo, al cabo, digital. Una cosa tan presente y tan ubicua que termina brillando por su ausencia. Acaso ganaríamos
  en precisión si hablásemos, más bien, de servidumbre digital. Incluso de borreguismo digital, donde el usuario no es
  administrador de los sistemas que utiliza y se contenta con pulsar botones aquí y allá, siempre donde /otros/ (las
  benefactoras corporaciones, las dueñas reales de lo digital) se lo indican. El usuario de medios digitales del llamado
  Primer Mundo no sería muy distinto de cualquier miembro de alguna tribu perdida de la Amazonia, instruido en el manejo
  de un /smartphone/ por unos imaginarios y perversos exploradores. Ambos, al final, tardarían lo mismo ---muy poco---
  en abrirse un perfil en /Instagram/.

  Insistamos, entonces, en la pregunta: ¿la tipografía que se hace ahora ---al menos, la mayor parte de ella--- ha de ser
  tildada como "digital"? Ciertamente, no. Sería una tipografía producida /en/ una computadora, pero no /por medio de/ una
  computadora. De la misma forma que una carta de amor redactada con un teclado (y no con pluma de ave y tintero) y
  enviada por correo electrónico (y no por paloma mensajera) no convierte a ese amor, expresado con mayor o menor fortuna,
  en "digital". Esta tipografía sería, más bien, /paradigital/. Está realizada en una computadora, sí, pero no aprovecha
  la computación. No la comprende y, por tanto, la desprecia. Los cajistas y los linotipistas de antaño conocían a fondo
  su herramienta de trabajo. Ahora tendrás suerte si encuentras a algún "maquetador" o "diseñador gráfico" de hoy día que
  sepa o tenga una vaga noción de por qué ha guardado un texto en UTF-8, y qué consecuencias prácticas supone tal acción.
  Este tipo de operarios, iletrados digitales /stricto sensu/, ha terminado alcanzando el raro estatus de artífices sin
  técnica que ejecutan un arte sin herramientas. Algunos, los menos, tendrán algún tipo de cultura tipográfica en
  abstracto, y cierta idea de lo que quieren conseguir. Pero les falta el medio y, lo más importante, la materia, junto a
  la comprensión de dicha materia. De vacíos así e inanidades tales se nutre el contenido de todos esos másteres de
  edición que pululan por ahí (que considero una verdadera estafa y, en caso de estar auspiciados por universidades
  públicas, una estafa vergozante) o de páginas web, tan populares como sobrevaloradas, del estilo de /Unos tipos duros/,
  que por cierto resultan sonrojantemente blandos a la hora de plegarse al /software/ propietario y privativo. Se verá
  bastante decepcionado quien se asome por esos ámbitos buscando algo de tipografía digital real.

  El paisaje en torno, por tanto, no parece precisamente acogedor, y lo que tendría que haber sido una transición
  natural entre el arte tipográfico mecánico y el producido mediante un ordenador, se ha visto truncado por un penoso
  hiato, no surgido de casualidad por cierto, sino promovido interesadamente por las corporaciones de /software/. Apple
  y Adobe tienen no poca culpa de ello cuando en los 80 y 90 del pasado siglo se estableció la tiranía de los programas
  de autoedición, que unidos casi genéticamente al término nebuloso (o vacuo) de "diseño gráfico" han acabado deviniendo
  plaga y cáncer para la tipografía. Es el emblema de "todo para el usuario pero sin el usuario", que va de la mano con
  la imposición de esa cantinela tan malsana y perniciosa de la "informática amigable", que es la informática sin la
  informática, y donde los procesos complejos se embozan bajo una capa de impostada banalidad y de un simplismo que no
  tiene nada que ver con el mero concepto de lo simple. Con herramientas tan débiles, el usuario no tiene un control
  real sobre los procesos que ejecuta, si es que los llega a ejecutar, y carece de una noción precisa de ellos. Es más,
  la aparente "amigabilidad" del medio no se traduce ---paradójicamente--- en menos trabajo, sino en un mayor coste
  humano y material. Casi todo ha de hacerse a mano, cuando no a ojo. Pero en programas como Adobe InDesign lo manual no
  tiene sentido si hablamos de la composición y producción seria de libros (como sí lo tendría en un /software/ de
  dibujo). Lo manual en este caso es llana imposibilidad y, en última instancia, masoquismo.

{{{pie}}}

#+begin_export html
<div>
<p>Publicado: 12/10/19</p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
