% Created 2019-09-01 dom 18:46
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{LuaTeX y Cavafis}}]{{\cabecera\MakeUppercase{LuaTeX y Cavafis}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{LuaTeX y Cavafis}
\begin{document}

\maketitle
\tableofcontents


\section{\emph{El funeral de Sarpedón} en \emph{Nea Zoí}}
\label{sec:org001e13c}

Hace unos meses Javier Sánchez Quirós, profesor de griego, inquieto divulgador de la cultura clásica y mantenedor de un
muy \href{https://paulatinygriego.wordpress.com/}{recomendable blog}, me hizo llegar un PDF con un par de páginas reproducidas de la revista griega \emph{Nea Zoí} donde
aparecía por primera vez publicado (1908) el poema de Cavafis \emph{El funeral de Sarpedón}. Las muestro aquí (véase figs.
\vref{fig:orgf1646df} y \vref{fig:orge9b00d6}), pero también se pueden ver en \href{https://cavafy.onassis.org/object/dgfw-2hx4-cr4b/}{el \emph{Archivo Cavafis}} de la Fundación Onassis. Le comenté
en su momento a Javier que aquel envío cavafiano no sólo era muy goloso (cómo no) por lo que hace a la poesía del
alejandrino, sino que también me suscitaba un enorme interés tipográfico; así que planeé un pequeño apunte por estos
lares, postergado hasta hoy por unas cuantas circunstancias y trajines de la vida.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.8\textwidth]{./images/Funeral_original1.jpg}
\caption{\label{fig:orgf1646df}
\emph{El funeral de Sarpedón} de Cavafis, publicado en \emph{Nea Zoí} (primera página)}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=0.8\textwidth]{./images/Funeral_original2.jpg}
\caption{\label{fig:orge9b00d6}
\emph{El funeral de Sarpedón} de Cavafis, publicado en \emph{Nea Zoí} (segunda página)}
\end{figure}

Como decía (y nos metemos ya en harina), estas páginas son un perfecto ejemplo de dos rasgos muy distintivos de lo que
ha venido siendo la tradición tipográfica de Grecia hasta la llegada del \guillemotleft{}diseño gráfico\guillemotright{}, la \guillemotleft{}maquetación\guillemotright{} y la
decadencia que padecemos ahora, no sólo en Grecia sino por doquier. Por un lado, tenemos el empleo de unos tipos de estilo
\emph{lipsiakó}, una cursiva muy reconocible que en Grecia se convirtió en la acompañante prácticamente habitual de
los tipos Didot (el estilo \emph{aplá}: simple, sencillo, es decir: estándar). La versión más refinada de esta familia
de itálicas la fabricaba la empresa Monotype para sus monotipias, con el nombre de \emph{Greek 91}, ya que el \emph{90}
correspondía al estilo recto de su Didot.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/funeral_original_detalle_rho.png}
\caption{\label{fig:org91ab780}
Detalle de la \emph{rho} con descendente curvo en la cursiva \emph{lipsiakó}}
\end{figure}

Pero, más allá de Grecia, la fama de este estilo de cursiva (al menos dentro de los gabinetes filológicos) se la
debemos, como su propio y griego nombre indica, a la ciudad de Leipzig y especialmente a las ediciones críticas de
Teubner. El círculo germano-heleno, pues, se va cerrando y ya tenemos a nuestra letra perfectamente ubicada.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/detalle_Teubner.png}
\caption{\label{fig:org6020b0d}
Un detalle de la edición del Dionisio de Halicarnaso de Teubner}
\end{figure}

El segundo rasgo distintivo es mucho más curioso y excéntrico, pues se circunscribe únicamente a la composición
tipográfica de la poesía en verso. Me temo que no tenemos en español un nombre técnico para el procedimiento que vamos a
comentar, pero podríamos ensayar algo como \guillemotleft{}colgado de diacríticos\guillemotright{}, si seguimos el término inglés de \guillemotleft{}hanging
diacritics\guillemotright{} que emplea Yannis Haralambous en su opúsculo sobre ortotipografía griega y Unicode\footnote{Véase \href{http://web.archive.org/web/20120229131933/http://omega.enstb.org/yannis/pdf/boston99.pdf}{From Unicode to Typography, a Case Study the Greek Script}, de Yannis Haralambous}. Vendría a ser como
una variante extrema del \href{https://revistacuadernoatico.com/apuntestipograficos/2019/03/04/mas-margenes-opticos-un-aperitivo/}{alineamiento óptico de márgenes} y, a diferencia de éste, que consiste en un artificio sutil y
no debe ser percibido por el lector, el \guillemotleft{}colgado de diacríticos\guillemotright{} es bastante notorio a ojos vistas. Consiste en
desplazar fuera del margen izquierdo los diacríticos de las mayúsculas, de tal forma que la letra quede alineada con el
resto de letras del margen. Es una técnica, insistimos, que ha de emplearse únicamente en la poesía versal y sólo
tendría sentido hacerlo con el sistema politónico, dada la variedad y combinatoria de los acentos y espíritus que
producen un importante desplazamiento de la vocal afectada(fig. \vref{fig:orgd6777ac}).

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/funeral_original_detalle_protrusion.png}
\caption{\label{fig:orgd6777ac}
Detalle del colgado de diacríticos en el texto original}
\end{figure}

\section{Una recreación en LuaTeX}
\label{sec:org3bc86d6}
¿Podemos recrear estas páginas históricas con la potencia del más avanzado sistema de composición tipográfica digital?
Yo me pasé un rato muy agradable trasteando en ello, y damos a continuación cuenta del proceso, mientras desgranamos
paso a paso nuestro preámbulo.

\subsection{La fuente escogida}
\label{sec:org015ed62}
Sin lugar a dudas, optamos por la excelsa Old Standard, diseño de Alexej Krykov y probablemente la mejor recreación
digital del estilo Didot-\emph{aplá} y de su itálica \emph{Lipsiakó}. Así pues, definimos un documento a 11 puntos y cargamos el
paquete \texttt{fontspec}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\documentclass[11pt]{article}
\usepackage{fontspec}
\end{minted}

Cargo también un paquete de factura propia, \texttt{caluacode.sty}, donde incluyo el código Lua necesario para ejecutar una
serie de sustituciones contextuales, especialmente las de la \emph{beta} curvada sin descendente y la \emph{theta} cerrada en mitad de
palabra. Estas filigranas no están presentes en las páginas originales de \emph{Nea Zoí}, pues son más típicas de la
tradición impresora francesa. Pero nos gustan mucho y nos permitimos la licencia. Cabe señalar que la propia fuente Old
Standard ya incluye las propiedades OpenType de sustitución contextual para esos dos glifos, pero su cadena condicional
de caracteres no está completa, así que definí hace tiempo unas cuantas funciones en Lua del tipo
\texttt{fonts.handlers.otf.addfeature} que incluyen todos los contextos de sustitución y pueden aplicarse a cualquier fuente
que disponga de los glifos a sustituir, aunque no lleven la correspondiente propiedad OpenType\footnote{Véase \href{https://maciaschain.gitlab.io/lunotipia/sustitucion\_contextual.html}{esta entrada} en \emph{La lunotipia}.}. Así pues, cargo
el paquete con las funciones Lua y añado cuatro propiedades OpenType que definí para estos menesteres:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage{caluacode}
\defaultfontfeatures{%
RawFeature={+contextual,
+contextual2,
+contextual3,
+contextual4}}
\end{minted}


\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/old_standard_contextual.png}
\caption{\label{fig:orga23df3f}
variantes estilísticas-contextuales de la \emph{beta} y la \emph{theta} en la fuente Old Standard}
\end{figure}

La interlínea del original de \emph{Nea Zoí} tal vez sea algo excesiva. Pero nos mantenemos fieles y añadimos un valor algo
mayor del normal (que suele ser de 1.2em, es decir, un 120\% del cuerpo de letra), redefiniendo el valor del texto normal
de la clase \texttt{article} de \LaTeX{}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\renewcommand{\normalsize}{\fontsize{11pt}{14pt}\selectfont}
\end{minted}

Por último, cargamos \texttt{Babel} con griego politónico y la fuente:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage[greek.polytonic]{babel}
\babelfont{rm}{Old Standard}
\end{minted}

\subsection{El formato de los versos}
\label{sec:orgf523265}
Cargamos el macro paquete \texttt{verse.sty} y hacemos las definiciones pertinentes de formato. La separación estrófica del
original es un tanto excesiva, pero seguimos manteniendo una cierta fidelidad. Así que:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage{verse}
\setlength{\stanzaskip}{24pt}
\end{minted}

La fuente del título es una \emph{paloseco} geométrica. Valdría una Helvetica o una Arial, pero aquí yo prefiero desviarme
algo y emplear una de estilo \emph{attiká}, como la excelente GFS Neohellenic de la Sociedad de Fuentes Griega:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\renewcommand{\poemtitlefont}{%
\bfseries\fontsize{15pt}{14pt}\selectfont
\centering
\fontspec{GFS Neohellenic}}
\setlength{\afterpoemtitleskip}{28pt}
\setlength{\beforepoemtitleskip}{0pt}
\end{minted}

\subsection{El colgado de los diacríticos}
\label{sec:org451527b}
Aquí llegamos a la parte más divertida. Nos basta con cargar el paquete \texttt{microtype.sty}, que es el encargado de
gestionar todas las propiedades microtipográficas de LuaTeX, especialmente la conocida como \guillemotleft{}protrusion\guillemotright{} para los
márgenes ópticos. Yo tengo definido un largo archivo de configuración con valores de márgenes ópticos y colgado de
diacríticos específicos para la fuente Old Standard, de modo que si hemos declarado previamente esa fuente, \texttt{microtype}
extraerá los valores de dicho archivo. Concretamente para el colgado de diacríticos definí también un contexto, llamado
\guillemotleft{}versogriego\guillemotright{}, y \texttt{microtype} sólo aplicará esos valores dentro del entorno \texttt{versogriego}. Conviene insistir que todas
esas cifras están optimizadas para la fuente Old Standard, y si se aplican a otras tipografías podríamos encontrarnos con resultados
inesperados. Así comienza mi definición para los valores de colgado de diacríticos (aún en proceso de pruebas y, por
tanto, experimentales):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
%% Para los diacríticos al margen

\SetProtrusion
[ name = versogriego,
  load = oldstandard-default,
  context = versogriego,
  unit = 1em ]
{ encoding = {EU1,EU2,TU},
  family   = {OldStandard} }
{ Ἐ  = {212, },
  Ἑ  = {212, },
  Ἒ  = {258, },
  Ἓ  = {258, },
  Ἔ  = {286, },
  Ἕ  = {286, },
  Ἠ  = {212, }, %
  Ἡ  = {212, },
  Ἢ  = {258, },
  Ἣ  = {258, },

% (etc.)

}
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/funeral_TeX_detalle_protrusion.png}
\caption{\label{fig:org2cc2c57}
Detalle de colgado de diacríticos en nuestra recreación con LuaTeX}
\end{figure}

\subsection{Conflictos de \texttt{microtype} y \texttt{verse}}
\label{sec:org0bac206}
Los valores de \texttt{microtype} funcionarían sólo en los márgenes \guillemotleft{}reales\guillemotright{} y no en los márgenes de los versos que componemos
a través del paquete \texttt{verse} y que no dejan de ser una variante muy modificada de un entorno \texttt{list}. Pero podemos
aplicar antes un \emph{hacking} que sugiere el autor de \texttt{microtype}. Por tanto, defino un comando \texttt{protverso} (de
\guillemotleft{}protrusion en el verso\guillemotright{}):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\makeatletter
 \newcommand*\protverso[1]{%
         {\everypar{}%
                 \setbox\z@\vbox{\noindent#1}%
                 \vbadness=\@M
                 \splittopskip=\z@
                 \global\setbox\z@=\vsplit\z@ to \baselineskip
                 \unvbox\z@ \global\setbox\z@=\lastbox
         }%
         \ifhbox\z@
         \leavevmode
         \kern\leftmarginkern\z@
         \fi
         #1}
 \makeatother
\end{minted}

\subsection{Montando los versos y compilando todo}
\label{sec:orgef921c4}
Así comenzaría nuestro documento con \emph{El funeral de Sarpedón}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begin{document}

\poemtitle{Η ΚΗΔΕΙΑ ΤΟΥ ΣΑΡΠΗΔΟΝΟΣ}

\begin{microtypecontext}{protrusion=versogriego}

\settowidth{\versewidth}{και γύρισαν στες άλλες τους φροντίδες και δουλειές.}
 \itshape
\begin{verse}[\versewidth]\protverso
Βαρυὰν ὀδύνην ἔχει ὁ Ζεύς. Τὸν Σαρπηδόνα\\\protverso
ἐσκότωσεν ὁ Πάτροκλος· καὶ τώρα ὁρμοῦν\\\protverso
ὁ Μενοιτιάδης κ’ οἱ Ἀχαιοὶ τὸ σῶμα\\\protverso
ν’ ἁρπάξουνε καὶ νὰ τὸ ἐξευτελίσουν.

\protverso
Ἀλλὰ ὁ Ζεὺς διόλου δὲν στέργει αὐτά.\\\protverso
Τὸ ἀγαπημένο του παιδὶ ― ποὺ τὸ ἄφισε\\\protverso
καὶ χάθηκεν· ὁ Νόμος ἦταν ἔτσι ―\\\protverso
τουλάχιστον θὰ τὸ τιμήσει πεθαμένο.\\\protverso
Καὶ στέλνει, ἰδού, τὸν Φοῖβο κάτω στὴν πεδιάδα\\\protverso
ἑρμηνευμένο πῶς τὸ σῶμα νὰ νοιασθεῖ.

% (... ... ... ... ...)

% (etc.)

\end{verse}
\end{microtypecontext}
\end{document}
\end{minted}



Y \href{https://maciaschain.gitlab.io/lunotipia/pdf/funeral.pdf}{aquí} se puede descargar el PDF resultante.
\end{document}