% Created 2019-12-27 vie 03:02
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Combinar tipos latinos y griegos (un breve apunte)}}]{{\cabecera\MakeUppercase{Combinar tipos latinos y griegos (un breve apunte)}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\usepackage{rotating}
\author{Juan Manuel Macías}
\date{\today}
\title{Combinar tipos latinos y griegos (sólo un apunte)}
\begin{document}

\maketitle
\index{tipografía griega!combinación de tipos griegos con latinos}
\index{LaTeX!=fontspec=!propiedad =scale=}

Demasiado breve, ciertamente, para lo que este tema es capaz de dar de sí. Porque son muchas y variadas las
estrategias que podemos seguir a la hora de intentar casar en un libro tipos latinos y griegos de distintas familias
tipográficas, ya sea cuando el texto «rector» sea de caracteres latinos o viceversa. Partamos, ante todo, de una base
general, algo en que no haría falta redundar, pero que nos lleva a ello, una vez más, el desconocimiento coral que en
la producción gráfica de estos tiempos se tiene de la tipografía griega. Aunque la escritura griega es dueña de una
personalidad propia, de un temperamento y, cómo no, de una historia (larga historia no carente de más de un deseacomodo
con los caracteres latinos en su no menos larga \emph{Odisea} desde \href{https://maciaschain.gitlab.io/lunotipia/griegos\_del\_rey.html}{la mano del escriba} hacia su propio estatus
tipográfico), conviene buscar un cierto espíritu que enlace el texto griego y el latino. Pero sin temor al natural
contraste entre ambos. No queremos letras griegas que parezcan latinas, ni letras latinas helenizadas en exceso.
Deseamos, más bien, esa noción tan imprecisa que se llama «estilo», y que puede concretarse muchas veces con el
espíritu de una época o los modos de alguna que otra tradición; pero que también estará esperando detrás de nuevas
vetas para minarlas con el debido recato. Porque en tipografía, insistimos de nuevo, la originalidad es el más
pecaminoso de los suicidios: caeremos nosotros primero, pero nos llevaremos detrás a quien más debemos cuidar, que es
el lector. La versión abreviada de todo lo dicho hasta aquí: combinar una Garamond o una Bembo con una Didot griega en
la misma página no es precisamente la más feliz de las ideas.

Por contra, si hemos escogido bien nuestras dos fuentes, entonces ya tendremos la mayor parte del camino recorrido.
Sólo queda probar, calibrar y volver a probar. Habrá ocasiones, por cierto, en que la altura x de la fuente griega
difiera perceptiblemente de la de la latina, bien por arriba o por debajo. ¿Es esto un problema? Pues depende de la
circunstancia y caso concretos, pero en general no debería serlo si entendemos que estamos tratando con dos escrituras
que tienen su propia idiosincrasia. Por fortuna, tampoco nos faltan en el ecosistema \TeX{} recursos para lidiar
con esos y muchos más avatares. Sin ir más lejos, hay una propiedad muy interesante que proporciona el paquete
\texttt{fontspec} a la hora de definir nuestras familias de tipos, llamada \texttt{scale}. Esta propiedad admite tres valores: uno
numérico, el que queramos, y otros dos prefijados llamados \texttt{MatchLowercase} y \texttt{MatchUppercase}, que se encargarán de
escalar los tipos en que han sido declarados para que éstos se ajusten de la forma más aproximada posible a la
altura de las minúsculas o mayúsculas, respectivamente, de la fuente principal. Conviene añadir que se trata siempre
de un escalado no destructivo ni deformador, llevado a cabo mediante unos cálculos muy precisos en el proceso de
compilación y cargado de las fuentes. De dichos cálculos nos da buena cuenta el archivo \texttt{*.log}, y también podemos
tomar como referencia la cifra que ha obtenido \TeX{} (seguida siempre por una hilera abrumadora de decimales) para
hacer alguna corrección, si es que nos interesa seguir esta vía.

Para ver claramente el proceso, nada mejor que una exageración: muestro este primer ejemplo extremo con la fuente
Crimson (los tipos griegos de esta fuente, quiero decir) y una fuente de estilo \guillemotleft{}grafitero\guillemotright{} llamada Abuse. Podemos usar
\texttt{fontspec} mediante Babel, y así declaramos a un tiempo las familias con sus propiedades y la lengua en que deben ser
cargadas. El resultado de la compilación, en la fig. \vref{fig:org57c9297}).

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage{fontspec}
% Cargamos Babel con español como lengua principal y como lenguas invitadas inglés, latín,
% alemán, italiano y griego (atributo: griego antiguo)
\usepackage[greek.ancient,english,latin,german,italian,spanish]{babel}
% Definimos la famila rm principal
\babelfont{rm}{Abuse}
% Y la familia sólo para ser usada en griego (Crimson)
\babelfont[greek]{rm}[Scale=MatchLowercase]{Crimson}
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=\textwidth]{./images/abuse.png}
\caption{\label{fig:org57c9297}Abuse con Crimson griega escalada a la altura de las minúsculas de la primera}
\end{figure}

Y, visto ya como trabajan los cálculos de escalado, pasemos a un ejemplo más sensato, donde combinaremos Crimson con
Sabon (resultado en fig. \vref{fig:org14b1cf1}).

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage{fontspec}
% Cargamos Babel con español como lengua principal y como lenguas invitadas inglés, latín, alemán, italiano y griego (atributo: griego antiguo)
\usepackage[greek.ancient,english,latin,german,italian,spanish]{babel}
% Definimos la famila rm principal
\babelfont{rm}[Numbers=Lowercase]{Sabon LT Std}
% Y la familia sólo para ser usada en griego (Crimson)
\babelfont[greek]{rm}[Scale=MatchLowercase]{Crimson}
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=\textwidth]{./images/sabon.png}
\caption{\label{fig:org14b1cf1}Sabon con Crimson griega escalada a la altura de las minúsculas de la primera}
\end{figure}

\section*{En cualquier caso\ldots{}}
\label{sec:orge98fc72}
Insisto en lo que dije más arriba. En otras ocasiones es mejor dejar las cosas como están y no poner cadenas al
contraste, siempre y cuando éste pueda jugar a nuestro favor. Hoy las fuentes Unicode que cubren múltiples sistemas de
escritura nos han acostumbrado (tal vez demasiado) al aspecto homogéneo de los glifos a través de cada rango. Y salvo
honrosos casos donde esa homogeneidad viene dada por un minucioso y esmerado proceso de diseño, muy a menudo no
hallaremos más que letras saludando con su uniforme y en formación a los caracteres latinos. Pero cuando el griego
supo encontrar su propio lugar en la tipografía, el contraste no deja de ser bello, como bellas eran (ya no, por
desgracia) las páginas de la Colección Budé (fig. \vref{fig:org4426aee}). Qué mayor color que el aportado por su característica
paloseco griega. Y a la vez cuánta coherencia y limpieza en la composición.

\begin{sidewaysfigure}
\centering
\includegraphics[width=.9\linewidth]{./images/herodoto.jpg}
\caption{\label{fig:org4426aee}El Heródoto de Legrand (1944). Texto griego principal en la paloseco de Budé (que la colección usó hasta 1950).\\ El griego del ap.crít. está en Didot.}
\end{sidewaysfigure}
\end{document}