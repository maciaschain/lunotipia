#+TITLE: La sustitución contextual OpenType en LuaTeX
#+SUBTITLE: (y un ejemplo con la escritura griega)
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:t" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:nil" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (filtro-html-misc)"))
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
# +STARTUP: inlineimages
#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Exportación a LaTeX
#+LaTeX_CLASS: normal-lua-org
#+LaTeX_Header: \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
#+LaTeX_HEADER: \let\maketitleold\maketitle
#+LaTeX_HEADER: \def\maketitle{{\fontspec{Arial}\maketitleold}}
#+LaTeX_HEADER: \let\tableold\tableofcontents
#+LaTeX_HEADER: \def\tableofcontents{\tableold\bigskip}
#+LaTeX_HEADER: \def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
#+LaTeX_HEADER: \newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
#+LaTeX_HEADER: \newpagestyle{base}{\sethead[\thepage][][{\cabecera LA SUSTITUCIÓN CONTEXTUAL OPENTYPE EN LUATeX}]{{\cabecera LA SUSTITUCIÓN CONTEXTUAL OPENTYPE EN LUATeX}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
#+LaTeX_HEADER: \pagestyle{base}
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage[spanish]{varioref}
#+LaTeX_HEADER: \renewcommand{\listingscaption}{Cuadro de código}

#+INDEX: Open Type!sustitución contextual
#+INDEX: LuaTeX!=fonts.handlers.otf.addfeature=!sustitución contextual
#+INDEX: tipografía griega!caracteres estilísticos alternativos

{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none
    ;; Para LaTeX
(defun filtro-latex-misc (texto backend info)
    "reemplaza el coamndo \ref por \vref y forma nums. romanos con versalitas"
    (when (org-export-derived-backend-p backend 'latex)
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "\\\\miscrom{\\1}"
	(replace-regexp-in-string "\\\\begin{enumerate}"  "\\\\begin{enumerate}[mienu]"
	  (replace-regexp-in-string "\\\\ref{"  "\\\\vref{" texto)))))

  ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

#+begin_export html
<form>
<p style="text-align: center;"><button formaction="https://maciaschain.gitlab.io/lunotipia/pdf/sustitucion_contextual.pdf "><font size="2">Descargar esta entrada en formato PDF</font></button></p>
</form>
#+end_export

Una de las características más atractivas que ofrece la tecnología OpenType es aquella que consiste en sustituir unas
letras por otras dependiendo de un determinado contexto. Puede usarse para resolver ciertas necesidades de tipografía
pragmática, pero también habrá escenarios para satisfacer otras de índole más bien estilística o histórica, casi siempre
sepultadas en el olvido tras la llegada de los ordenadores al mundo de la producción editorial. A este segundo grupo
pertenecería la sustitución en mitad de palabra de la letra griega beta minúscula β por su variante estilística curvada
(o beta sin descendente) ϐ, procedimiento muy habitual en la tradición impresora francesa del pasado siglo y que,
aplicado en los textos y en las condiciones adecuadas, puede crear un efecto muy grato al lector (o al menos al lector
que soy yo).

Tenemos (si somos consecuentes con la tesis Unicode que distingue caracteres de glifos) un único carácter para la letra
beta, idea platónica que puede concretarse textualmente en dos posibles glifos: el de la beta «normal» con descendente y
el de la beta curvada, que el estándar Unicode sitúa bajo el código ~U+03D0~ y que denomina «Greek beta symbol» (fig.
[[fig:betasymbol]])

Lo que viene a aportar OpenType es el añadir a la fuente tipográfica una propiedad ---diga@@latex:\-@@mos--- tridimensional, que
permite que el carácter opte por un glifo u otro según una serie de instrucciones que la fuente pueda incluir. Basta con
activar la etiqueta adecuada (que casi siempre suele ser ~calt~, de «contextual alternates»), por ejemplo en nuestro
{{{lualatex}}} con el paquete Fontspec. A los efectos de transmisión textual ---y esto es lo esencial, insistimos--- ambos
glifos siguen siendo el mismo carácter beta, como podemos comprobar si copiamos cualquier pasaje de un PDF donde se haya
operado la sustitución contextual y lo pegamos en un editor de texto.

#+CAPTION: Descripción del carácter Unicode ~GREEK BETA SYMBOL~
#+NAME: fig:betasymbol
#+ATTR_HTML: :style width:70%
#+ATTR_LaTeX: :width 0.7\textwidth
[[./images/beta_symbol.png]]

Ahora bien, OpenType se limita a ofrecer los protocolos necesarios. Otro cantar es hasta qué punto los implementan los
diseñadores de fuentes. Por supuesto, si nuestra fuente no dispone de esa característica, como sucede en la gran mayoría
de fuentes que incluyen soporte para la escritura griega, siempre podremos añadirla nosotros mismos mediante el editor
de fuentes [[http://fontforge.github.io/en-US/][Fontforge]]. Pero hoy toca hablar aquí de otra manera mucho más simple, si cabe, de hacerlo, que es dentro de
{{{luatex}}} y echando mano de la utilísima función Lua ~fonts.handlers.otf.addfeature~, que nos permite definir en el
pre-procesado del material textual ciertas características OpenType «al vuelo», gracias a la habilidad que {{{luatex}}}
tiene de "injertar", como su propio nombre anuncia, /scripts/ y código Lua en el proceso de compilación. {{{luatex}}}
incluye en sus engranajes un intérprete de Lua, lo que supone que podemos puentear las primitivas de {{{tex}}} mediante
este versátil y ligero lenguaje. En lo que atañe a nuestra deseada sustitución, la ventaja de hacerlo así es evidente,
pues añadiremos la propiedad OpenType a cualquier fuente que se nos antoje, sin necesidad de editarla y de crear
versiones modificadas de ésta. Siempre y cuándo, claro, la fuente disponga de los glifos necesarios con que jugar, pues
~fonts.handlers.otf.addfeature~ no crea letras de la nada, sino que opera sobre la posición y el contexto de los glifos
de que ya dispone la fuente.

Veamos una prueba, por ejemplo, con la fuente Linux Libertine, que sí dispone de esos glifos. Añadimos lo siguiente a
nuestro archivo fuente de {{{latex}}}, convenientemente encerrado en la primitiva ~\directlua~, necesaria para poder
insertar código Lua.

Empezamos por definir una variable y asignarle una tabla de todos los caracteres griegos que deben ir antes del glifo a
sustituir:

#+begin_src lua :exports code
  letras_griegas = { "α", "ἀ", "ἁ", "ἂ", "ἃ", "ἄ", "ἅ", "ἆ", "ἇ",
  "Ἀ", "Ἁ", "Ἂ", "Ἃ", "Ἄ", "Ἅ", "Ἆ", "Ἇ", "ε", "ἐ", "ἑ", "ἒ", "ἓ", "ἔ", "ἕ",
  "Ἐ", "Ἑ", "Ἒ", "Ἓ", "Ἔ", "Ἕ", "η", "ἠ", "ἡ", "ἢ", "ἣ", "ἤ", "ἥ", "ἦ", "ἧ",
  "Ἠ", "Ἡ", "Ἢ", "Ἣ", "Ἤ", "Ἥ", "Ἦ", "Ἧ", "ι", "ἰ", "ἱ", "ἲ", "ἳ", "ἴ", "ἵ", "ἶ", "ἷ",
  "Ἰ", "Ἱ", "Ἲ", "Ἳ", "Ἴ", "Ἵ", "Ἶ", "Ἷ", "ο", "ὀ", "ὁ", "ὂ", "ὃ", "ὄ", "ὅ",
  "Ὀ", "Ὁ", "Ὂ", "Ὃ", "Ὄ", "Ὅ", "υ", "ὐ", "ὑ", "ὒ", "ὓ", "ὔ", "ὕ", "ὖ", "ὗ",
  "Ὑ", "Ὓ", "Ὕ", "Ὗ", "ω", "ὠ", "ὡ", "ὢ", "ὣ", "ὤ", "ὥ", "ὦ", "ὧ",
  "Ὠ", "Ὡ", "Ὢ", "Ὣ", "Ὤ", "Ὥ", "Ὦ", "Ὧ",
  "ὰ","ά","ὲ","έ","ὴ","ή","ὶ","ί","ὸ","ό","
  ὺ","ύ","ὼ","ώ","ᾀ","ᾁ","ᾂ","ᾃ","ᾄ","ᾅ","ᾆ","ᾇ",
  "ἈΙ","ᾉ", "ἊΙ","ᾋ", "ἌΙ","ᾍ", "ἎΙ","ᾏ",
  "ᾐ","ᾑ","ᾒ","ᾓ", "ᾔ","ᾕ","ᾖ","ᾗ","ἨΙ","ᾙ",
  "ἪΙ","ᾛ", "ἬΙ","ᾝ", "ἮΙ","ᾟ", "ᾠ","ᾡ","ᾢ","ᾣ","ᾤ","ᾥ","ᾦ","ᾧ",
  "ὨΙ","ᾩ", "ὪΙ", "ᾫ", "ὬΙ","ᾭ", "ὮΙ","ᾯ",
  "ᾰ","ᾱ","ᾲ","ᾳ","ᾴ","ᾶ","ᾷ","Ᾰ","Ᾱ","ῆ","ῇ",
  "ῖ","ῗ","Ῐ","Ῑ","ῠ","ῡ","ῢ","ΰ","ῤ","ῥ","ῦ","ῧ","Ῠ","Ῡ","Ῥ",
  "ῲ","ῳ","ῴ","ῶ","ῷ", "Ά","Έ","Ή","Ί","Ό","Ύ","Ώ","ΐ",
  "Α","Β","Γ","Δ","Ε","Ζ","Η","Θ","Ι","Κ",
  "Λ","Μ","Ν","Ξ","Ο","Π","Ρ","Σ","Τ","Υ","Φ","Χ","Ψ","Ω","Ϊ","Ϋ",
  "ά", "έ","ή","ί","ΰ","α","β","γ","δ","ε","ζ","η","θ",
  "ι","κ","λ","μ","ν","ξ","ο","π","ρ","ς","σ","τ","υ",
  "φ","χ","ψ","ω","ϓ","ϔ","ϕ","ϖ","ϗ", "Ϙ", "ϙ","Ϛ","ϛ","Ϝ","ϝ", "ϐ" }
#+end_src

Y a continuación, la nueva propiedad OpenType:

#+begin_src lua :exports code
  fonts.handlers.otf.addfeature{
     name = "contextualtest",
     type = "chainsustitution",
     lookups = {
	{
	   type = "sustitution",
	   data = {
	      ["β"] = "ϐ",
	   },

	},
     },
     data = {
	rules = {
	   {
	      before  = { letras_griegas  },
	      current = { { "β" } },
	      lookups = { 1 },
	   },
	},
     },
  }
#+end_src

Explicándolo un poco a trazo grueso. Hemos creado una propiedad OpenType que llamamos ~contextualtest~, y declaramos que
es de tipo ~chainsustitution~, es decir, queremos definir una cadena de sustitución contextual. Le incluimos un
/lookup/ que indica en qué consiste la sustitución ("β" a "ϐ"), y más abajo añadimos las reglas. Las cuales consisten en
que todos los caracteres encerrados bajo la variable ~before~ propiciarán la sustitución de beta simple a curvada
/sólo/ si ellos se sitúan inmediatamente antes de la letra.

Bien, ya casi lo tenemos. Esto nos soluciona el 99 % de los contextos para sustituir la beta en mitad de
palabra. Pero la queremos, precisamente, así, en mitad de palabra y no al final, lo que no evitaría el código
anterior. Naturalmente, son muy raros los casos en que aparezca una beta en esa posición, si pensamos en la escritura
griega real. Pero algún que otro contexto puede surgir, así que sumamos al anterior este otro código, que evitará que
nuestra beta se sustituya por la variante curvada al final de una palabra:

#+begin_src lua :exports code
  fonts.handlers.otf.addfeature{
     name = "contextualtest2",
     type = "chainsustitution",
     lookups = {
	{
	   type = "sustitution",
	   data = {
	      ["ϐ"] = "β",
	   },
	},
     },
     data = {
	rules = {
	   {
	      after  = { { " ", 0xFFFC, ",", ".", "´", "·"} },
	      current = { { "ϐ" } },
	      lookups = { 1 },
	   },
	},
     },
  }
#+end_src

Y pasamos a probar nuestro código, cargando las dos nuevas propiedades OpenType con =fontspec=. Definimos entonces, para
la Linux Libertine, dos familias, con y sin sustitución (resultado de la compilación en la fig. [[fig:compilacion]]):

#+begin_src latex :exports code
  \documentclass{article}
  \usepackage{fontspec}
  % Aquí iría todo el código Lua...
  \setmainfont{Linux Libertine}
  \usepackage[greek.ancient]{babel}
  \newfontfamily\nobetasub{Linux Libertine}
  \newfontfamily\sibetasub[RawFeature={+contextualtest,+contextualtest2}]{Linux Libertine}
  \begin{document}
  \nobetasub
  βαρβάρων - ἐβούλετο - ιβ´
  \sibetasub
  βαρβάρων - ἐβούλετο - ιβ´
  \end{document}
#+end_src


#+CAPTION: Resultado de la compilación
#+NAME: fig:compilacion
[[./images/contextual2.png]]

{{{pie}}}

#+begin_export html
<div>
<p>Publicado: 31/08/19</p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
