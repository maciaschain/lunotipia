#+TITLE: El punto alto griego, Unicode y LuaTeX
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:t" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:nil" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (exporta-romanos-versalitas-html)"))
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
# +STARTUP: inlineimages
#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Exportación a LaTeX
#+LaTeX_CLASS: normal-lua-org
#+LaTeX_Header: \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
#+LaTeX_HEADER: \let\maketitleold\maketitle
#+LaTeX_HEADER: \def\maketitle{{\fontspec{Arial}\maketitleold}}
#+LaTeX_HEADER: \let\tableold\tableofcontents
#+LaTeX_HEADER: \def\tableofcontents{\tableold\bigskip}
#+LaTeX_HEADER: \def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
#+LaTeX_HEADER: \newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
#+LaTeX_HEADER: \newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{El punto alto griego, Unicode y LuaTeX}}]{{\cabecera\MakeUppercase{El punto alto griego, Unicode y LuaTeX}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
#+LaTeX_HEADER: \pagestyle{base}
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage[spanish]{varioref}
#+LaTeX_HEADER: \renewcommand{\listingscaption}{Cuadro de código}

#+INDEX: tipografía griega!punto alto!ubicación correcta
#+INDEX: TeX!catcodes
#+INDEX: LuaTeX!=fonts.handlers.otf.addfeature=!corrección del punto alto griego


{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none
  ;; Para LaTeX

  (defun filtro-latex-misc (texto backend info)
    "reemplaza el coamndo \ref por \vref y forma nums. romanos con versalitas"
  (when (org-export-derived-backend-p backend 'latex)
  (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "\\\\miscrom{\\1}"
			    (replace-regexp-in-string "\\\\ref{"  "\\\\vref{" texto))))

  ;; Para html
  (defun exporta-romanos-versalitas-html (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
    números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))
#+end_src

#+begin_export html
<form>
<p style="text-align: center;"><button formaction="https://maciaschain.gitlab.io/lunotipia/pdf/punto_alto.pdf"><font size="2">Descargar esta entrada en formato PDF</font></button></p>
</form>
#+end_export


El punto alto es un signo prosódico específico de la escritura griega, cuyo uso vendría a ser equivalente al del punto y
coma en la puntuación latina. El estándar Unicode lo tiene codificado en la posición =U+0387=, y lo nombra (como en el
caso del resto de caracteres de todo el rango Unicode que afecta al griego) con el término empleado en griego actual:
/ano teleía/. El signo, pues, estaría bien definido de cara al diseño de fuentes y la correcta representación
tipográfica de la lengua griega. Pero el problema viene, como suele ser demasiado frecuente en Unicode, por la
duplicidad de caracteres. Tal es así, que el carácter que nos ocupa no pocas veces se confunde con el del punto medio
(/middle dot/: =U+00B7=). La contaminación ya se remonta a antes de Unicode, aunque parece que en estos últimos tiempos
digitales el punto medio va ganando la partida. Sin ir más lejos, la distribución de teclado que uso en Linux para
escribir en griego tiene mapeado por defecto este punto a costa del correcto /ano teleía/. Vemos, por tanto, que el
problema del punto alto resulta ser al cabo un problema de altura. Se viene a unir al juego, además, otra anomalía
habitual, y es que algunas fuentes, aun disponiendo de los dos caracteres, igualan la altura del /ano teleía/ con la del
punto medio, con lo cual ambos signos son indistinguibles. Probablemente los diseñadores de tipos se dejan llevar por
esta tendencia a la baja de nuestro punto griego. O, en el fondo, no hay un consenso total y la cuestión ha de quedar
abierta /sine die/. En mi opinión, el punto alto debe ser eso: un punto alto. ¿A qué altura? Lo más razonable sería no
muy lejos de la altura /x/ de la fuente, como se ve en estos dos ejemplos tomados de las /Platonis Opera/ de Oxford
(fig. [[fig:platonis]]) y el Dionisio de Halicarnaso de Teubner (fig. [[fig:teubner]]).

#+CAPTION: El punto /correctamente/ alto en las /Platonis Opera/ de Oxford
#+NAME: fig:platonis
#+ATTR_LaTeX: :width 0.7\textwidth
[[./images/platonis-opera-punto-alto.png]]

#+CAPTION: Y en Teubner
#+NAME: fig:teubner
#+ATTR_LaTeX: :width 0.7\textwidth
[[./images/dionisio-de-halicarnaso-punto-alto-teubner.png]]

La excelente y siempre recomendable fuente Old Standard de Alexey Kryukov distingue claramente el punto medio del punto
alto (aunque este último, curiosamente, es de un peso algo mayor, como se aprecia en la fig. [[fig:oldstandard]]). Es más,
incluye la etiqueta Open Type =locl= (sustitución de formas locales para un sistema de escritura determinado), gracias a
la cual nos aseguraremos de que todo punto medio en nuestro texto griego a procesar sea sustituido por el /ano teleía/:
siempre y cuando, claro, tengamos activada dicha marca y el =script= para griego.

#+CAPTION: Contraste de punto alto y punto medio en la fuente Old Standard
#+NAME: fig:oldstandard
#+ATTR_LaTeX: :width 0.7\textwidth
[[./images/punto_alto_old_standard.png]]

@@latex:\begin{sloppypar}@@

En otras fuentes que dispongan de ambos caracteres bien diferenciados, pero que carezcan de esa característica Open
Type, podremos añadirla con un software editor de fuentes (Fontforge es aquí siempre el recomendado). La otra
posibilidad, si usamos Lua(La)TeX, es aplicarla al vuelo, sin necesidad de editar nuestra fuente, gracias a la función
Lua =fonts.handlers.otf.addfeature= dentro de una orden =directlua=. Añadiríamos esto en el preámbulo:

@@latex:\end{sloppypar}@@

#+begin_src latex :exports code
  \directlua{
  fonts.handlers.otf.addfeature{
  name = "substest",
  type = "substitution",
  data = {
  periodcentered = "anoteleia",
	 },
      }
  }
#+end_src

La etiqueta, que hemos llamado (por ejemplo) =substest=, podemos activarla o desactivarla a discreción mediante un
=RawFeature={+substest}= o =RawFeature={-substest}=, respectivamente.

Podemos también realizar esta sustitución de forma nativa a TeX, mediante la modificación de los catcodes. Tocar los
catcodes siempre es delicado y puede traer consecuencias inesperadas en nuestra compilación. La forma correcta de
hacerlo sería algo así como esto:

#+begin_src latex :exports code
\begingroup
\catcode`\^^^^00b7=\active
\protected\gdef^^^^00b7{\string ^^^^0387}%
\endgroup
\AtBeginDocument{%
	\catcode`\^^^^00b7=\active
}
#+end_src

En otros contextos, tal vez queramos componer en una fuente griega que, si bien tiene definido el carácter del /ano
teleia/, éste se ubica (como es habitual, ya dijimos) o bien más bajo de lo deseable o exactamente a la misma altura que
el punto medio. Vamos, que el diseñador de fuentes ha hecho un simple «copia y pega» de glifos y no se ha complicado la
vida. ¿Cómo lo arreglamos? Podemos, en principio, editar la fuente con Fontforge, y así modificar la altura de nuestro
signo. Pero en LuaTeX nos vuelve a ayudar un poco de código Lua para el preprocesado, de tal modo que nos ahorramos el
andar editando y renombrando fuentes. De nuevo, un método simple, limpio y no destructivo (para con la fuente original).

Supongamos, entonces, que queremos componer con Minion Pro, cuyo punto alto luce así de mal colocado cuando nos viene
«de fábrica» (fig. [[fig:minion][fig:minion]]).

#+CAPTION: El remiso punto alto de la Minion Pro
#+NAME: fig:minion
[[./images/puntos-altos1.png]]

definimos una simple función en Lua que nos reemplace tanto el punto medio como el punto alto (así nos aseguramos de que
la corrección será global) por el propio punto alto, que habremos (eso sí) encerrado en una ~\raisebox~ para elevarlo.
Por ejemplo, declarando este comando:

#+begin_src latex :exports code
\def\puntoalto{\raisebox{0.3ex}{\char"0387}}
#+end_src

Conviene usar, por cierto, una magnitud relativa como ~ex~ y no absoluta (puntos, milímetros, etc). Así el punto se
ubicará siempre igual independientemente del cuerpo de letra empleado.

Nuestra función en Lua la podemos encerrar en un entorno ~luacode~, proporcionado por el paquete del mismo nombre:

#+begin_src latex :exports code
\begin{luacode}
   function cambiar_puntoalto ( texto )
   texto = string.gsub ( texto, "·",   "\\puntoalto" ) -- sustituye punto medio
   texto = string.gsub ( texto, "·",   "\\puntoalto" ) -- sustituye punto alto original
    return texto
   end
\end{luacode}
#+end_src

Y en lo que hace a la parte LaTeX, un par de comandos que active o desactive la función:

#+begin_src latex :exports code
\newcommand\anoteleia{\directlua{luatexbase.add_to_callback
   ( "process_input_buffer" , cambiar_puntoalto , "cambiar_puntoalto" )}}
\newcommand\middledot{\directlua{luatexbase.remove_from_callback
    ( "process_input_buffer" , "cambiar_puntoalto" )}}
#+end_src

Y, por último, nuestro mínimo ejemplo funcional (resultado de la compilación en la fig. [[fig:compilacion]]):

#+begin_src latex :exports code
\begin{document}

   πέπονθα· (con punto alto original)

     \anoteleia % Sustituye los 2 puntos a nuestro punto alto, pero alto de verdad ;-)

  πέπονθα· / πέπονθα· (con punto medio / punto alto original)

     \middledot % Deja las cosas como estaban

  πέπονθα· (con punto medio)

\end{document}
#+end_src

#+CAPTION: Compilación del ejemplo
#+NAME: fig:compilacion
[[./images/puntos-altos2.png]]

*Y estrambote*: En @@html:este vídeo@@ @@latex:\href{https://vimeo.com/341114471 }{este vídeo}@@ muestro un ejemplo real (esta vez con la fuente Linux Libertine), dentro de un libro que
compuse recientemente.

#+begin_export html
<iframe src="https://player.vimeo.com/video/341114471" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/341114471">Correcci&oacute;n de la altura del punto alto griego en LuaTeX</a> from <a href="https://vimeo.com/user70979426">Juan Manuel Mac&iacute;as Cha&iacute;n</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
#+end_export

{{{pie}}}

#+begin_export html
<div>
<p>Publicado: 03/09/19 - 20:37</p>

<p>Última actualización: 03/09/19 - 20:38</p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
