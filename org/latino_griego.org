#+TITLE: Combinar tipos latinos y griegos (sólo un apunte)
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:nil" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:nil" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (filtro-html-misc)"))
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
# +STARTUP: inlineimages
#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Exportación a LaTeX
#+LaTeX_CLASS: normal-lua-org
#+LaTeX_Header: \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
#+LaTeX_HEADER: \let\maketitleold\maketitle
#+LaTeX_HEADER: \def\maketitle{{\fontspec{Arial}\maketitleold}}
#+LaTeX_HEADER: \let\tableold\tableofcontents
#+LaTeX_HEADER: \def\tableofcontents{\tableold\bigskip}
#+LaTeX_HEADER: \def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
#+LaTeX_HEADER: \newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
#+LaTeX_HEADER: \newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Combinar tipos latinos y griegos (un breve apunte)}}]{{\cabecera\MakeUppercase{Combinar tipos latinos y griegos (un breve apunte)}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
#+LaTeX_HEADER: \pagestyle{base}
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage[spanish]{varioref}
#+LaTeX_HEADER: \renewcommand{\listingscaption}{Cuadro de código}
#+LaTeX_HEADER: \usepackage{rotating}

#+INDEX: tipografía griega!combinación de tipos griegos con latinos
#+INDEX: LaTeX!fontspec!propiedad scale

{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none
  ;; Para LaTeX
(defun filtro-latex-misc (texto backend info)
    "reemplaza el coamndo \ref por \vref y forma nums. romanos con versalitas"
    (when (org-export-derived-backend-p backend 'latex)
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "\\\\miscrom{\\1}"
	(replace-regexp-in-string "\\\\begin{enumerate}"  "\\\\begin{enumerate}[mienu]"
	  (replace-regexp-in-string "\\\\ref{"  "\\\\vref{" texto)))))

  ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

#+begin_export html
<form>
<p style="text-align: center;"><button formaction="https://maciaschain.gitlab.io/lunotipia/pdf/latino_griego.pdf"><font size="2">Descargar esta entrada en formato PDF</font></button></p>
</form>
#+end_export


  Demasiado breve, ciertamente, para lo que este tema es capaz de dar de sí. Porque son muchas y variadas las
  estrategias que podemos seguir a la hora de intentar casar en un libro tipos latinos y griegos de distintas familias
  tipográficas, ya sea cuando el texto «rector» sea de caracteres latinos o viceversa. Partamos, ante todo, de una base
  general, algo en que no haría falta redundar, pero que nos lleva a ello, una vez más, el desconocimiento coral que en
  la producción gráfica de estos tiempos se tiene de la tipografía griega. Aunque la escritura griega es dueña de una
  personalidad propia, de un temperamento y, cómo no, de una historia (larga historia no carente de más de un deseacomodo
  con los caracteres latinos en su no menos larga /Odisea/ desde [[https://maciaschain.gitlab.io/lunotipia/griegos_del_rey.html][la mano del escriba]] hacia su propio estatus
  tipográfico), conviene buscar un cierto espíritu que enlace el texto griego y el latino. Pero sin temor al natural
  contraste entre ambos. No queremos letras griegas que parezcan latinas, ni letras latinas helenizadas en exceso.
  Deseamos, más bien, esa noción tan imprecisa que se llama «estilo», y que puede concretarse muchas veces con el
  espíritu de una época o los modos de alguna que otra tradición; pero que también estará esperando detrás de nuevas
  vetas para minarlas con el debido recato. Porque en tipografía, insistimos de nuevo, la originalidad es el más
  pecaminoso de los suicidios: caeremos nosotros primero, pero nos llevaremos detrás a quien más debemos cuidar, que es
  el lector. La versión abreviada de todo lo dicho hasta aquí: combinar una Garamond o una Bembo con una Didot griega en
  la misma página no es precisamente la más feliz de las ideas.

  Por contra, si hemos escogido bien nuestras dos fuentes, entonces ya tendremos la mayor parte del camino recorrido.
  Sólo queda probar, calibrar y volver a probar. Habrá ocasiones, por cierto, en que la altura x de la fuente griega
  difiera perceptiblemente de la de la latina, bien por arriba o por debajo. ¿Es esto un problema? Pues depende de la
  circunstancia y caso concretos, pero en general no debería serlo si entendemos que estamos tratando con dos escrituras
  que tienen su propia idiosincrasia. Por fortuna, tampoco nos faltan en el ecosistema {{{tex}}} recursos para lidiar
  con esos y muchos más avatares. Sin ir más lejos, hay una propiedad muy interesante que proporciona el paquete
  ~fontspec~ a la hora de definir nuestras familias de tipos, llamada ~scale~. Esta propiedad admite tres valores: uno
  numérico, el que queramos, y otros dos prefijados llamados ~MatchLowercase~ y ~MatchUppercase~, que se encargarán de
  escalar los tipos en que han sido declarados para que éstos se ajusten de la forma más aproximada posible a la
  altura de las minúsculas o mayúsculas, respectivamente, de la fuente principal. Conviene añadir que se trata siempre
  de un escalado no destructivo ni deformador, llevado a cabo mediante unos cálculos muy precisos en el proceso de
  compilación y cargado de las fuentes. De dichos cálculos nos da buena cuenta el archivo ~*.log~, y también podemos
  tomar como referencia la cifra que ha obtenido {{{tex}}} (seguida siempre por una hilera abrumadora de decimales) para
  hacer alguna corrección, si es que nos interesa seguir esta vía.

  Para ver claramente el proceso, nada mejor que una exageración: muestro este primer ejemplo extremo con la fuente
  Crimson (los tipos griegos de esta fuente, quiero decir) y una fuente de estilo "grafitero" llamada Abuse. Podemos usar
  ~fontspec~ mediante Babel, y así declaramos a un tiempo las familias con sus propiedades y la lengua en que deben ser
  cargadas. El resultado de la compilación, en la fig. [[abuse]]).

  #+begin_src latex :exports code
    \usepackage{fontspec}
    % Cargamos Babel con español como lengua principal y como lenguas invitadas inglés, latín,
    % alemán, italiano y griego (atributo: griego antiguo)
    \usepackage[greek.ancient,english,latin,german,italian,spanish]{babel}
    % Definimos la famila rm principal
    \babelfont{rm}{Abuse}
    % Y la familia sólo para ser usada en griego (Crimson)
    \babelfont[greek]{rm}[Scale=MatchLowercase]{Crimson}
  #+end_src

#+ATTR_LaTeX: :width \textwidth
#+CAPTION: Abuse con Crimson griega escalada a la altura de las minúsculas de la primera
#+NAME: abuse
[[./images/abuse.png]]

  Y, visto ya como trabajan los cálculos de escalado, pasemos a un ejemplo más sensato, donde combinaremos Crimson con
  Sabon (resultado en fig. [[sabon]]).

  #+begin_src latex :exports code
  \usepackage{fontspec}
  % Cargamos Babel con español como lengua principal y como lenguas invitadas inglés, latín, alemán, italiano y griego (atributo: griego antiguo)
  \usepackage[greek.ancient,english,latin,german,italian,spanish]{babel}
  % Definimos la famila rm principal
  \babelfont{rm}[Numbers=Lowercase]{Sabon LT Std}
  % Y la familia sólo para ser usada en griego (Crimson)
  \babelfont[greek]{rm}[Scale=MatchLowercase]{Crimson}
  #+end_src

#+ATTR_LaTeX: :width \textwidth
#+CAPTION: Sabon con Crimson griega escalada a la altura de las minúsculas de la primera
#+NAME: sabon
[[./images/sabon.png]]

* En cualquier caso...
   :PROPERTIES:
   :CUSTOM_ID: En cualquier caso...
   :END:

  Insisto en lo que dije más arriba. En otras ocasiones es mejor dejar las cosas como están y no poner cadenas al
  contraste, siempre y cuando éste pueda jugar a nuestro favor. Hoy las fuentes Unicode que cubren múltiples sistemas de
  escritura nos han acostumbrado (tal vez demasiado) al aspecto homogéneo de los glifos a través de cada rango. Y salvo
  honrosos casos donde esa homogeneidad viene dada por un minucioso y esmerado proceso de diseño, muy a menudo no
  hallaremos más que letras saludando con su uniforme y en formación a los caracteres latinos. Pero cuando el griego
  supo encontrar su propio lugar en la tipografía, el contraste no deja de ser bello, como bellas eran (ya no, por
  desgracia) las páginas de la Colección Budé (fig. [[herodoto]]). Qué mayor color que el aportado por su característica
  paloseco griega. Y a la vez cuánta coherencia y limpieza en la composición.

# +ATTR_LaTeX: :options angle=90
#+ATTR_LaTeX: :float sideways
#+CAPTION: El Heródoto de Legrand (1944). Texto griego principal en la paloseco de Budé (que la colección usó hasta 1950).@@latex:\\@@ El griego del ap.crít. está en Didot.
#+NAME: herodoto
[[./images/herodoto.jpg]]


{{{pie}}}

#+begin_export html
<div>
<p>Publicado: 27/12/19</p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
