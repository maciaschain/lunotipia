% Created 2019-10-07 lun 15:22
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Ejecutar Common-Lisp en LaTeX: el paquete lisp-on-tex}}]{{\cabecera\MakeUppercase{Ejecutar Common-Lisp en LaTeX: el paquete lisp-on-tex}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{Ejecutar Common-Lisp en \LaTeX{}: el paquete lisp-on-tex}
\begin{document}

\maketitle
\tableofcontents

\index{LaTeX!lisp-on-tex}
\index{LaTeX!Expresiones regulares}
\index{LaTeX3}

He estado probando estos primeros días soleados de octubre el paquete experimental \href{https://www.ctan.org/pkg/lisp-on-tex}{\texttt{lisp-on-tex}}, escrito por Hakuta
Shizuya. Como su nombre anuncia (y los característicos guiones también), se trata de una biblioteca destinada a evaluar
código Lisp a través del proceso de compilación de \LaTeX{}. Algo que, a primera vista, me puso los dientes largos, tan
devoto y apasionado como soy por este bello y potente lenguaje de programación, en gran parte en su dialecto Emacs-Lisp.
Con Lisp siempre es bueno recordar estas palabras de Stallman:

\begin{quote}
The most powerful programming language is Lisp. If you don't know Lisp (or its variant, Scheme), you don't know what it
 means for a programming language to be powerful and elegant. Once you learn Lisp, you will see what is lacking in most
 other languages.

Unlike most languages today, which are focused on defining specialized data types, Lisp provides a few data types which
are general. Instead of defining specific types, you build structures from these types. Thus, rather than offering a
way to define a list-of-this type and a list-of-that type, Lisp has one type of lists which can hold any sort of data.

Where other languages allow you to define a function to search a list-of-this, and sometimes a way to define a generic
list-search function that you can instantiate for list-of-this, Lisp makes it easy to write a function that will search
any list — and provides a range of such functions.

In addition, functions and expressions in Lisp are represented as data in a way that makes it easy to operate on them.

When you start a Lisp system, it enters a read-eval-print loop. Most other languages have nothing comparable to `read',
nothing comparable to `eval', and nothing comparable to `print'. What gaping deficiencies!

--- Richard Stallman, \href{https://stallman.org/stallman-computing.html}{\emph{How I do my computing}}, en su \href{https://stallman.org}{página personal}.
\end{quote}

Con todas esas premisas, cualquier caballo de Troya lispiano, aunque sea un potrillo, no puede sino ser bienvenido entre
el engranaje de nuestro cajista binario \TeX{} y su director editorial binario \LaTeX{}. Porque (y es lo que aquí nos ocupa)
las posibilidades tipográficas que se desprenderían de ello son más que interesantes. Echemos un vistazo a las
primeras\linebreak impresiones.

\section{Primeros juegos}
\label{sec:org16c0e3b}
Antes de empezar, conviene insistir que este paquete se encuentra aún en desarrollo (más bien lento, al parecer, pues su
última actualización es de 2015), y esto trae como consecuencia cosas como que la documentación es inexistente. Hay que
conformarse con un escueto y espartano \emph{Readme}, aunque suficiente para sacar algunas conclusiones de entrada. A saber:

\begin{enumerate}
\item El código Lisp ha de encerrarse en el comando \texttt{\textbackslash{}lispinterp\{...\}},
\item Los símbolos de Lisp han de llevar siempre una barra invertida, como los comandos de \TeX{}. Por ejemplo, un \texttt{concat} se
convierte aquí en \texttt{\textbackslash{}concat},
\item Las cadenas de texto se ponen con comillas simples, no dobles.
\end{enumerate}

Añadido a eso, la plena funcionalidad del paquete depende de la infraestructura de LaTeX3, la futura versión de \LaTeX{},
también en desarrollo. Por suerte, podemos usar gran parte del kernel de LaTeX3 sobre LaTeX2ε si cargamos en nuestro
preámbulo el paquete \texttt{expl3}. Y es aquí donde vino el primer tropezón leve. Un módulo de \texttt{lisp-on-tex} que nos hacía
mucha gracia probar, \texttt{lisp-mod-l3regex}, para trabajar las expresiones regulares sobre Lisp, requería del paquete de
LaTeX3 \texttt{l3regex.sty}. Pero no encontraba el paquete y devolvía error. La causa estaba en que \texttt{l3regex.sty} se encontraba
plegado dentro del propio \texttt{expl3}. Se solucionó fácilmente añadiendo la línea \texttt{\textbackslash{}expandafter\textbackslash{}def\textbackslash{}csname
ver@l3regex.sty\textbackslash{}endcsname\{\}}, y con eso ya teníamos nuestro preámbulo listo para el recreo:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\documentclass{article}
\usepackage{fontspec}
\setmainfont{Linux Libertine O}
\usepackage{expl3}
\expandafter\def\csname ver@l3regex.sty\endcsname{}
\usepackage{lisp-on-tex}
\usepackage{lisp-mod-l3regex}
\end{minted}

Las primeras pruebas que hice, como comenté antes, estaban relacionadas con expresiones regulares. El paquete viene ya con algunas funciones definidas. Por ejemplo, \texttt{\textbackslash{}regReplaceAll} es lo más parecido a lo que en Elisp
sería un \texttt{replace-regex-in-string}, con los mismos tres argumentos (el regex a reemplazar, el regex que reemplaza y la
cadena donde reemplazar). Con esta función, ya he probado a definir algunos comandos sencillos para \LaTeX{}. Como éste, que convertiría todo carácter \guillemotleft{}a\guillemotright{} en \guillemotleft{}xxx\guillemotright{}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\def\prueba#1{%
\lispinterp{
  (\texprint
  (\regReplaceAll 'a' 'xxx' '#1'))
}}
\end{minted}

Podemos probarlo si escribimos algo así como:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\prueba{la casa de arriba}
\end{minted}

Con esa misma estructura, este otro comando nos pondría en negrita todos los números romanos. Ojo, para que no salga el
literal \texttt{\textbackslash{}textbf\{...\}} sino su resultado, obsérvese la sintaxis. Por otra parte, \texttt{\textbackslash{}1} para el grupo, es lo esperable:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\def\pruebabis#1{%
\lispinterp{
  (\texprint
  (\regReplaceAll '(\b[IVXLCDM]+\b)' '\c{textbf}\cB\{\1\cE\}' '#1'))
}}
\end{minted}

Lo probamos con:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\pruebabis{El siglo XXI y el volumen VI}
\end{minted}

Y este tercer comando nos pondría en negrita cualquier texto en griego, ya sea poli- o monotónico:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\def\pruebatris#1{%
\lispinterp{
  (\texprint
  (\regReplaceAll
'([\x{1F00}-\x{1FFE}\x{0370}-\x{03FF}]+)'
'\c{textbf}\cB\{\1\cE\}'
'#1'))
}}
\end{minted}

Y podemos probarlo con:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\pruebatris{Lorem ipsum dolor sit amet, consectetuer adipiscing
elit. Donec hendrerit tempor tellus. Δαρείου καὶ Παρυσάτιδος
γίγνονται παῖδες δύο. Donec pretium posuere tellus. Proin quam
nisl, tincidunt et, mattis eget, convallis nec, purus.}
\end{minted}

\section{Un comando menos trivial. ¡Y hasta un entorno!}
\label{sec:org17153e3}
Hace no mucho, escribí para Emacs una sencilla función para evitar (como casi siempre) un trabajo latoso. El caso es que
andaba trabajando en la composición de un extenso libro, cuyo autor tuvo a bien diseminar a través del texto infinidad
de pasajes en griego: palabras unas veces; otras, frases de una o dos líneas, más o menos. Naturalmente, lo ideal sería
incluir todos esos segmentos en un comando \texttt{\textbackslash{}foreignlanguage\{greek\}\{...\}} de Babel, para asegurar el correcto guionado
del griego antiguo, pero se puede morir cualquiera si tiene que ir haciéndolo a mano y uno por uno. Primero, se me
ocurrió intentar escribir algún script en Perl, hasta que caí en la cuenta de que con Elisp sería más sencillo, así que,
tras algunas pruebas ensayo/error di con una regex que funciona y que no devuelve ningún falso positivo. La tuve que
segmentar en una expresión \texttt{concat} porque es bastante larga y entorpece la depuración del código. Pero traducida viene a
decir: «localiza cadenas de texto dentro de unos parámetros determinados, y que incluyen los caracteres de los rangos
Unicode 'griego básico' (\texttt{\textbackslash{}u0370-\textbackslash{}u03FF}) y 'griego extendido' (\texttt{\textbackslash{}u1F00-\textbackslash{}u1FFE}), signos de puntuación y espacios». Y la
función quedó, finalmente, así:

\begin{minted}[frame=lines,linenos=true,breaklines]{common-lisp}
(defun babel-griego-en-region ()
 (interactive)
  (save-restriction
   (narrow-to-region (region-beginning) (region-end))
  (save-excursion
   (goto-char (point-min))
      (replace-regexp
      (concat
  "\\(^\\|\\w+[[:blank:]]+\\|[,.;?¿:«»]+[[:blank:]]+\\|{\\)"
  "\\([,.;?¿:«»··\u1F00-\u1FFE\u0370-\u03FF ]+\\)"
  "\\([[:blank:]]+\\|\n\\|\\'\\|}\\)")
  "\\1\\\\foreignlanguage{greek}{\\2}\\3"
    nil)))
     (deactivate-mark))
\end{minted}

Y ahora que andaba trasteando con este \texttt{lisp-on-tex}, lo siguiente que me vino a la cabeza fue intentar adaptar esa
regex para que la sustitución se ejecutase durante la compilación. Que conste que ya se me había ocurrido hacerlo dentro
de Lua (con LuaLaTeX, se entiende), pero las expresiones regulares complejas no son el fuerte de Lua, reconozcámoslo.
Con \texttt{lisp-on-tex}, por contra, no fue muy difícil adaptar mi regex, \emph{mutatis mutandis}. Y a lo siguiente que llegué es a
una macro con la misma estructura de las de los ejemplos anteriores:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\def\babelgriego#1{%
\lispinterp{
  (\texprint
  (\regReplaceAll (\concat
'(^|\w+\s+|[,.;?¿:«»]+\s+)'
'([,.;?¿:«»··\n\s\x{1F00}-\x{1FFE}\x{0370}-\x{03FF}]+)'
'(\s+|\n)')
'\1\c{foreignlanguage}\cB\{greek\cE\}\cB\{\2\cE\}\3'
'#1'))
}}
\end{minted}

Pero, claro, cuánto mejor sería que esto se convirtiese en un entorno. El problema es que el comando de este intérprete
de Lisp, \texttt{\textbackslash{}lispinterp}, es una macro \guillemotleft{}corta\guillemotright{} y no puede actuar a través de los párrafos: \TeX{} nos devolvería el típico
error de \guillemotleft{}runaway argument\guillemotright{}. Podemos resolverlo echando mano de un \texttt{\textbackslash{}everypar}, pero hay muchos paquetes que lo
redefinen. Por suerte, viene en nuestra ayuda el paquete \texttt{everyhook} para hacernos el apaño, ya que (en palabras de su
autor) \guillemotleft{}takes control of the six TEX token parameters \texttt{\textbackslash{}everypar}, \texttt{\textbackslash{}everymath}, \texttt{\textbackslash{}everydisplay}, \texttt{\textbackslash{}everyhbox}, \texttt{\textbackslash{}everyvbox}
and \texttt{\textbackslash{}everycr}.\guillemotright{}

Cargamos, entonces, el paquete (y de paso \texttt{showhyphens}, para comprobar que los puntos de corte de sílaba están
activados para los pasajes en griego, y son los correctos). A continuación definimos la macro que aplicará por párrafo
nuestra función de Elisp. Y, finalmente, el entorno:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage{showhyphens}
\usepackage{everyhook}
\def\bgriegopar#1\par{\babelgriego{#1}\par}
\newenvironment{fragsgriego}{%
\PushPreHook{par}{\bgriegopar\null}}
{\PopPreHook{par}}
\end{minted}

Sólo nos queda ya probar nuestro entorno (\hyperref[fig:orge950ba3]{aquí}, el resultado de la compilación):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begin{fragsgriego}
Consectetuer adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer
adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing
elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing elit
Παρυσάτιδος γίγνονται. Consectetuer adipiscing elit Παρυσάτιδος
γίγνονται. Consectetuer adipiscing elit Παρυσάτιδος γίγνονται.
Consectetuer adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer
adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing
elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing elit
Παρυσάτιδος γίγνονται.

Consectetuer adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer
adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing
elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing elit
Παρυσάτιδος γίγνονται. Consectetuer adipiscing elit Παρυσάτιδος
γίγνονται. Consectetuer adipiscing elit Παρυσάτιδος γίγνονται.
Consectetuer adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer
adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing
elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing elit
Παρυσάτιδος γίγνονται.

Consectetuer adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer
adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing
elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing elit
Παρυσάτιδος γίγνονται. Consectetuer adipiscing elit Παρυσάτιδος
γίγνονται. Consectetuer adipiscing elit Παρυσάτιδος γίγνονται.
Consectetuer adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer
adipiscing elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing
elit Παρυσάτιδος γίγνονται. Consectetuer adipiscing elit
Παρυσάτιδος γίγνονται.
\end{fragsgriego}
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/lipontex.png}
\caption{\label{fig:orge950ba3}
Nuestro ejemplo compilado con los cortes de sílaba correctos para griego antiguo y español}
\end{figure}
\end{document}