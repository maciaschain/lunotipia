% Created 2022-02-10 jue 14:36
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
	  \usepackage{normal-lua}
	 	 % \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Caligrafía, tipografía, quimeras}}]{{\cabecera\MakeUppercase{Caligrafía, tipografía, quimeras}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish,nospace]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{Caligrafía, tipografía, quimeras}
\begin{document}

\maketitle
\tableofcontents

Es sabido que la tipografía surgió para encerrar a la escritura humana en un delirio de
simetría, rutina y orden. Y con ello nació también ese asombroso objeto industrial que
llamamos \guillemotleft{}libro\guillemotright{}, y el texto que puede clonarse hasta el infinito ---siempre que contemos
con tinta infinita y un impresor de infinito dinero--- frente al extraordinario e
irrepetible códice. El arte de la letra impresa, al cabo, no es otra cosa que el intento
de capturar un simple lapso en el continuo de la mano que escribe: el molde de cada letra,
como la mosca encerrada en su gota de ámbar. Pero aun dentro de ese nuevo y limpio orbe
matemático, lo tipográfico no ha dejado de volver la mirada en ocasiones a su origen
humano y perfectible, generalmente en la forma de tipos de inspiración caligráfica,
diseñados a lo largo de la historia con mayor o menor fortuna. Con la tecnología OpenType
---y otras parecidas--- para las fuentes digitales se ha aportado, además, la variedad de
las ligaduras y otros glifos mudables por posición o contexto.

Hay fuentes caligráficas ahí fuera, en fin, muy logradas y bellas, y muchas de licencia
libre. El problema casi siempre es encontrarles un escenario adecuado para que actúen.
Pero todo tiene su momento. Precisamente estos días, para un trabajo en que ando,
necesitaba crear una suerte de entorno caligráfico para \LaTeX{}, de suerte que entre
el inicio y el cierre se imprima un bloque que intente emular en la página el aspecto de
uno de esos cuadernillos de caligrafía de toda la vida. La idea es echar mano de los
recursos que tenemos más a mano, sin necesidad de inventar nada nuevo, como en esos
programas de televisión sobre bricolaje doméstico. Un poco de código Lua también vendrá en
nuestra ayuda para pautar los renglones de nuestro cuaderno ficticio. En los párrafos que
siguen intentaremos dar cuenta en detalle de todo el código empleado.

\section{El preámbulo}
\label{sec:orge8d938b}
Para nuestro experimento necesitaremos unos cuantos paquetes: \texttt{xparse} y \texttt{xkeyval}, para
definir el entorno y las opciones, respectivamente; \texttt{xcolor} para manipular colores;
\texttt{lipsum}, para el texto falso; \texttt{fontspec} para todo lo relacionado con las fuentes y su
gestión y \texttt{babel} para las lenguas (para lo que queremos, necesitamos español como lengua
principal y griego antiguo); \texttt{tikz} para los gráficos, \texttt{calc} para definir dimensiones
cómodamente; y, por último, \texttt{mdframed} nos servirá para el bloque contenedor. Es decir,
nuestro preámbulo se puede parecer a esto:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\documentclass[11pt]{article}
\usepackage{fontspec}
\usepackage[greek.ancient,spanish]{babel}
\usepackage{tikz,calc,lipsum,xcolor,mdframed}
\usepackage{xparse,xkeyval}
\end{minted}

Definimos ahora una caja que contendrá cada renglón pautado del cuaderno (realmente, son
dos renglones, uno para la línea base y otro para la altura de las minúsculas), un color
que intente aproximarse al azul de un bolígrafo y una dimensión que nos será útil para
delimitar los márgenes a izquierda y derecha de nuestro bloque:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\newbox\rejilla
\newlength{\cuadernomarg}
\setlength{\cuadernomarg}{.062\textwidth}
\definecolor{bolibic}{HTML}{194E92}
\end{minted}

\section{La función en Lua}
\label{sec:org873c007}
Nuestra función en Lua deberá asociarse al \emph{callback} \texttt{post\_linebreak\_filter}, y manipula
el nodo principal de la línea de tal forma que justo antes de cada línea recién cortada se
inserte nuestra caja, que hemos llamado \texttt{rejilla}. Podemos encerrar la función dentro de
la primitiva \texttt{\textbackslash{}directlua\{...\}}. De momento, quedaría así:

\begin{minted}[frame=lines,linenos=true,breaklines]{lua}
function rejilla (h,c)
   for linea in node.traverse(h) do
      if linea.id==0 then
	 linea.head=node.insert_before(linea.head,linea.head,node.copy(tex.box.rejilla))
      end
   end
   return h
end
\end{minted}

\section{El entorno para \texttt{mdframed}}
\label{sec:orgdbaed4c}
Nuestro entorno para el paquete \texttt{mdframed} deberá tener una serie de propiedades. Nótese
que este entorno también dibujará la línea roja típica del margen izquierdo en este tipo
de cuadernillos:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\mdfdefinestyle{cuaderno}{%
  linewidth=.5pt,
  linecolor= red,
  topline = false,
  rightline = false,
  bottomline = false,
  skipabove=\bigskipamount,
  skipbelow=\bigskipamount,
  leftmargin=2em,
  innerleftmargin=1em,
  rightmargin=3em,
  innerrightmargin=0pt,
  innertopmargin=0pt,
  innerbottommargin=0pt,
}

\newmdenv[style=cuaderno]{mdcuaderno}
\end{minted}

\section{El entorno \guillemotleft{}cuadernillo\guillemotright{} y sus opciones}
\label{sec:org45d17cd}
Las opciones serán del estilo \guillemotleft{}propiedad/valor\guillemotright{}. En concreto estas:

\begin{itemize}
\item \texttt{color}: el color de los renglones del cuaderno. Por defecto será gris con un valor de
opacidad de 0.4.
\item \texttt{font}: la fuente del entorno. Por defecto es la familia QtMerryScript, que no es una
gran fuente, pero la elijo porque está incluida en \TeX{} Live. Por supuesto, la gracia
está en que escojamos siempre una fuente caligráfica.
\item \texttt{fontfeature}: para indicar las propiedades opentype que queramos cargar, siempre con la
sintaxis de \texttt{fontspec}. Por defecto, ninguna en especial que no incluya la fuente de
salida en el documento.
\item \texttt{fontsize}: el tamaño de la fuente. Podemos indicarlo con los típicos comandos del
estilo \texttt{\textbackslash{}large} o mediante un comando \texttt{\textbackslash{}fontsize}. Por defecto, el tamaño normal.
\end{itemize}

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\makeatletter
\define@key{cuaderno}{color}[]{
  \def\cuadernocolor{#1}}
\define@key{cuaderno}{font}[]{
  \def\cuadernofont{\fontspec{#1}}}
\define@key{cuaderno}{fontfeature}[]{
  \def\cuadernofontsp{\addfontfeatures{#1}}}
\define@key{cuaderno}{fontsize}[]{
  \def\cuadernofontsize{#1}}
\makeatother
\end{minted}

Y nuestro entorno cuadernillo activa y desactiva el \emph{callback} que llama a la función en
Lua, y de paso dota a la caja \texttt{rejilla} de los renglones del cuaderno, dibujados mediante
\texttt{tikz}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\NewDocumentEnvironment{cuadernillo}{o}{%
  \def\cuadernofontsize{}
  \def\cuadernocolor{gray}
  \def\cuadernofont{\fontspec{qtmerryscript}}
  \def\cuadernofontsp{}
  \IfNoValueF{#1}{\setkeys{cuaderno}{#1}}
  \cuadernofont\cuadernofontsp
  \begin{mdcuaderno}
    \cuadernofontsize
    \setbox\rejilla=\hbox{%
      \begin{tikzpicture}[overlay]
	\draw[\cuadernocolor,opacity=.4] ((-\cuadernomarg,1ex) -- ++(\textwidth+1.4\cuadernomarg,0);
	\draw[\cuadernocolor,opacity=.4] ((-\cuadernomarg,0) -- ++(\textwidth+1.4\cuadernomarg,0);
      \end{tikzpicture}
    }
    \directlua{luatexbase.add_to_callback("post_linebreak_filter", rejilla, "cuaderno")}
}
{%
    \par\end{mdcuaderno}%
  \directlua{luatexbase.remove_from_callback("post_linebreak_filter", "cuaderno")}
}
\end{minted}

\section{Una prueba}
\label{sec:org264e6be}
Podemos hacer una prueba, por ejemplo con la fuente Vladimir Script (el resultado, en la
fig. \vref{fig:org80f094e}):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begin{cuadernillo}[font=Vladimir Script,fontfeature={Color=bolibic}]
  \parindent=0pt

\lipsum[1-2]

\end{cuadernillo}
\end{minted}

\href{./images/cuadernillo-vladimir.png}{\begin{figure}[htbp]

\includegraphics[width=.9\linewidth]{./images/cuadernillo-vladimir.png}
\caption{\label{fig:org80f094e}Primera prueba de nuestro entorno caligráfico con la fuente Vladimir Script}
\end{figure}}

\section{Una quimera}
\label{sec:org011e44b}
¿Y qué tal una prueba con la escritura griega? Pero hablar de tipografía griega y
caligrafía nos llevará, antes de empezar, a repasar la \href{griegos_del_rey.org}{historia} de la letra impresa en
griego. En efecto, desde el Renacimiento hasta el s.~ s-XIX-s, la práctica totalidad
del griego impreso en Europa era de inspiración caligráfica. Lo gracioso es que aquellos
primeros impresores se pensaban que el griego escrito era así, pues tuvieron como
referencia a los escribas griegos migrados a occidente, y para los impresores esos
escribas eran la autoridad competente en la materia. En el fondo no dejaban de ser bellas
florituras bizantinas. No fue hasta el s-XiX-s cuando Bodoni y, sobre todo, Didot,
pusieron un poco de orden tipográfico. Pero antes, la estrella indiscutible entre todo
aquel bello desvarío de ligaduras y volutas eran los tipos Grecs du Roi que Claude
Garamond fundió en el s-XVI-s para la Imprenta Real de Francia y las ediciones de Robert
Estienne. En los años 90 del pasado siglo Minaugas Strokis hizo una recreación digital
para la antigua codificación \texttt{wingreek}, excelente si tenemos en cuenta las limitaciones
técnicas de la época. Gracias a Yannis Haralambous, contamos con una versión de esa fuente
recodificada a Unicode. Por supuesto, carece de ligaduras, pero está tan bien dibujada que
apenas las echamos en falta. En fin, no pudimos resistir la tentación de crear una quimera
histórica con nuestro entorno de estilo cuadernillo: usar como caligrafía una fuente que
se usó siempre en un escenario puramente tipográfico. Con el color del bolígrafo y los
guiones pautados, queda patente la lejana mano del escriba. Cómo quedaría en nuestro
entorno el primer párrafo de la \emph{Anábasis} de Jenofonte, lo podemos apreciar en la fig.
\vref{fig:org18e25ff}.

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begin{cuadernillo}[font=grecsduroiwg,fontfeature={Color=bolibic},fontsize=\large]
  \parindent=0pt

Δαρείου καὶ Παρυσάτιδος γίγνονται παῖδες δύο, πρεσβύτερος μὲν Ἀρταξέρξης, νεώτερος δὲ
Κῦρος· ἐπεὶ δὲ ἠσθένει Δαρεῖος καὶ ὑπώπτευε τελευτὴν τοῦ βίου, ἐβούλετο τὼ παῖδε ἀμφοτέρω
παρεῖναι. ὁ μὲν οὖν πρεσβύτερος παρὼν ἐτύγχανε· Κῦρον δὲ μεταπέμπεται ἀπὸ τῆς ἀρχῆς ἧς
αὐτὸν σατράπην ἐποίησε, καὶ στρατηγὸν δὲ αὐτὸν ἀπέδειξε πάντων ὅσοι ἐς Καστωλοῦ πεδίον
ἁθροίζονται. ἀναβαίνει οὖν ὁ Κῦρος λαβὼν Τισσαφέρνην ὡς φίλον, καὶ τῶν Ἑλλήνων ἔχων
ὁπλίτας ἀνέβη τριακοσίους, ἄρχοντα δὲ αὐτῶν Ξενίαν Παρράσιον. ἐπεὶ δὲ ἐτελεύτησε Δαρεῖος
καὶ κατέστη εἰς τὴν βασιλείαν Ἀρταξέρξης, Τισσαφέρνης διαβάλλει τὸν Κῦρον πρὸς τὸν ἀδελφὸν
ὡς ἐπιβουλεύοι αὐτῶι. ὁ δὲ πείθεται καὶ συλλαμβάνει Κῦρον ὡς ἀποκτενῶν· ἡ δὲ μήτηρ
ἐξαιτησαμένη αὐτὸν ἀποπέμπει πάλιν ἐπὶ τὴν ἀρχήν.

\end{cuadernillo}
\end{minted}

\href{./images/cuadernillo-anabasis1.png}{\begin{figure}[htbp]

\includegraphics[width=.9\linewidth]{./images/cuadernillo-anabasis1.png}
\caption{\label{fig:org18e25ff}Una prueba con la fuente Grecs du Roi}
\end{figure}}

\section{Esos márgenes no tan \guillemotleft{}perfectos\guillemotright{}}
\label{sec:orgf1ca0ca}
Hay un problema evidente en nuestro entorno cuadernillo de pega, y es que la refinada
justificación de \TeX{} no casa demasiado bien con la emulación de la letra manuscrita.
Lo primero que se le puede ocurrir a un ojo poco entrenado es optar por la justificación a
la izquierda (\texttt{raggedright} / \texttt{flushleft}, en la jerga de \TeX{} y \LaTeX{}); pero,
en contra de lo que puede pensarse, este tipo de justificación nos aleja más que nos
acerca del renglón caligráfico, pues tiende a ser demasiado mecanizado y a producir unas
líneas de ancho muy irregular, sobre todo con palabras largas: si una palabra no cabe en
la línea, simplemente \TeX{} la manda a la línea siguiente. Cuando escribimos a mano
(algunos, incluso, no hemos olvidado cómo se hacía) tendemos a mantener la uniformidad de
unos márgenes a izquierda y derecha, con mejor o peor éxito. Pero la tendencia a la
uniformidad, haberla, hayla. Basta con echar un ojo a algún manuscrito medieval, como el
que se aprecia en la fig. \vref{fig:orgdc96453}. Es decir, lo que podemos hacer es intentar añadir cierta
irregularidad a los márgenes, pero manteniendo un aspecto general de justificación a
izquierda y derecha.

Para los márgenes de la izquierda (y, en parte de la derecha) podríamos aplicar ciertos
valores exagerados de protrusión a algunas letras, mediante el paquete \texttt{microtype}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage{microtype}

\SetProtrusion
   { encoding = {EU1,EU2,TU},
     family   = {grecsduroiwg} }
   {
     - = {500,-100},
     · = { ,700},
     τ = {390, },
     ν = {-90, -650},
     π = {300, },
     ὁ = {-80, -430},
     ο = {10, 30},
     θ = {280, },
     ς = {, -430},
     ρ = {, -530},
     α = {200, -730},
     ε = {, -630},
     μ = {260, -130},
 }
\end{minted}

\href{./images/cuadernillo-codice.png}{\begin{figure}[htbp]

\includegraphics[width=.9\linewidth]{./images/cuadernillo-codice.png}
\caption{\label{fig:orgdc96453}\emph{Heidelbergensis Palatinus gr. 281} (s. s-XI-s), Biblioteca de la Universidad de Heildelberg (fuente: \url{https://www.historyofinformation.com/detail.php?id=2266})}
\end{figure}}

En cuanto a la irregularidad de los márgenes derechos, creemos que la opción más realista
sería una justificación total, pero imperfecta. Es decir, un tipo de justificación donde
algunas líneas estuviesen justificadas a ambos márgenes pero otras \guillemotleft{}casi\guillemotright{} justificadas.
Podemos mantener el guionado, pues no nos parece incompatible con el estilo manuscrito. En
la wiki de \LaTeX{} se aporta una sencilla función (igualmente para manipular el nodo
de la línea y asociada al \emph{callback} \texttt{post\_linebreak\_filter}) para lograr este efecto. Yo
he modificado un poco la función para que se muestren en azul aquellas líneas que están
casi justificadas, pero no llegan por un pequeño margen a la justificación completa (en la
fig. \vref{fig:org4b91776} se muestra a la izquierda un texto justificado a la izquierda, y a la derecha
el mismo texto con justificación \guillemotleft{}mixta\guillemotright{}. Apréciese el contraste):

\begin{minted}[frame=lines,linenos=true,breaklines]{lua}
w = node.new("whatsit","pdf_literal")
w.data = "0 0 1 rg"
z = node.new("whatsit","pdf_literal")
z.data = "0 g"
function marg (head)
   for linea in node.traverse_id(node.id("hlist"), head) do
   if linea.glue_order == 0 and linea.glue_sign == 1 and linea.glue_set > .4 then
	 %% para colorear las líneas no justificadas
	 local antes, despues = node.copy(w), node.copy(z)
	  linea.head = node.insert_before(linea.head,linea.head,antes)
	  linea.head = node.insert_after(linea.head,linea,despues)
	  linea.head = node.hpack(linea.head)
	  end
    end
   return head
end
\end{minted}

\href{./images/cuadernillo-mixta.png}{\begin{figure}[htbp]

\includegraphics[width=.9\linewidth]{./images/cuadernillo-mixta.png}
\caption{\label{fig:org4b91776}Justificación a la izquierda (izquierda) y justificación \guillemotleft{}mixta\guillemotright{} (derecha)}
\end{figure}}

Así pues, para obtener este efecto en nuestro experimento, podemos hacer una leve
modificación en nuestra función anterior (véase \vref{sec:org873c007}):

\begin{minted}[frame=lines,linenos=true,breaklines]{lua}
function rejilla (h,c)
   for linea in node.traverse(h) do
   if linea.id==0 then
    if linea.glue_order == 0 and linea.glue_sign == 1 and linea.glue_set > .4 then
    linea.head = node.hpack(linea.head)
    end
    linea.head=node.insert_before(linea.head,linea.head,node.copy(tex.box.rejilla))
      end
   end
   return h
end
\end{minted}

En la fig. \vref{fig:orge3a6bd8} podemos ver la nueva versión obtenida. Y, ya venidos arriba, otro ejemplo con
el primer párrafo de la tercera \emph{Filípica}, en la fig. \vref{fig:org4080172}.

\href{./images/cuadernillo-anabasis-mixta.png}{\begin{figure}[htbp]

\includegraphics[width=.9\linewidth]{./images/cuadernillo-anabasis-mixta.png}
\caption{\label{fig:orge3a6bd8}Ejemplo con justificación \guillemotleft{}imperfecta\guillemotright{} para el primer párrafo de la \emph{Anábasis}}
\end{figure}}

\href{./images/cuadernillo-filipica-mixta.png}{\begin{figure}[htbp]

\includegraphics[width=.9\linewidth]{./images/cuadernillo-filipica-mixta.png}
\caption{\label{fig:org4080172}Ejemplo con justificación \guillemotleft{}imperfecta\guillemotright{} para el primer párrafo de la tercera \emph{Filípica}}
\end{figure}}
\end{document}