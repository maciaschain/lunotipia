#+TITLE: La Lunotipia
#+SUBTITLE: Tipografía digital, TeX y cafeína
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+AUTHOR: Juan Manuel Macías
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: title:nil
#+OPTIONS: subtitle:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+OPTIONS: ':t
#+OPTIONS: todo:nil
#+LANGUAGE: es
* Últimas entradas publicadas

#+INCLUDE: entradas.org :lines "14-24" :only-contents t

[[./entradas.org][Todas las entradas publicadas]]

#+begin_export html
<div>
<hr />
<p>
<a href="acerca-de.html">Acerca de...</a>
</p></div>
<p>
<a href="https://maciaschain.gitlab.io/lunotipia/rss.xml">RSS</a>
</p></div>
#+end_export
