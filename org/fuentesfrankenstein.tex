% Created 2019-12-27 vie 03:44
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Fuentes Frankenstein (o la manipulación de caracteres en LuaTeX)}}]{{\cabecera\MakeUppercase{Fuentes Frankenstein (o la manipulación de caracteres en LuaTeX)}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{Fuentes Frankenstein (o la manipulación de caracteres en LuaTeX)}
\begin{document}

\maketitle
\index{LuaTeX!Manipulación de texto con Lua}
\index{LuaLaTeX!paquete luacode}

Una de las características esenciales de la versión más avanzada de \TeX{}, LuaTeX, y precisamente la que justifica su
nombre, es la de incluir entre sus engranajes un intérprete de Lua, que nos permite \guillemotleft{}puentear\guillemotright{} las primitivas del motor
tipográfico mediante \emph{scripts} en este lenguage de programación liviano. Gracias a ello, podemos intercalar código Lua
entre el habitual código (La)\TeX{}, de forma que se ejecute en el pre y post-proceso. Algo que, naturalmente, nos dará
mucho juego, y que abrirá además un abanico de posibilidades vastísimo a la hora de manipular diversos aspectos de la
composición tipográfica, y hacerlo en estadios puntuales de la misma.

El ejemplo que aportaremos aquí, muy simple, puede parecer de a primeras que se queda sólo entre los márgenes lúdicos.
Más bien es un ejemplo abstracto que puede dar pie a muchas utilidades prácticas. Sin ir más lejos, me fue muy útil en un
libro que compuse recientemente, donde necesitaba que cierto tipo de caracteres a lo largo de las entradas de un índice
analítico asumieran una serie de propiedades concretas.

Partamos, para empezar, de un poco de texto falso, al que le hemos incluido en cualquier lugar la serie de cifras que
van del 1 al 0:

\begin{verbatim}
Lorem Ipsum Dolor Sit Amet, 1234567890 Consectetuer Adipiscing Elit.
Donec Hendrerit Tempor Tellus. Donec Pretium Posuere Tellus. Proin
Quam Nisl, Tincidunt Et, Mattis Eget, Convallis Nec, Purus. Cum Sociis
Natoque Penatibus Et Magnis Dis Parturient Montes, Nascetur Ridiculus
Mus. Nulla Posuere. Donec Vitae Dolor. Nullam Tristique Diam Non
Turpis. Cras Placerat Accumsan Nulla. Nullam Rutrum. Nam Vestibulum
Accumsan Nisl.
\end{verbatim}

Para cuando compilemos nuestro documento, tenemos el capricho de que todas las cifras del texto (y sólo las cifras)
estén en negrita, mientras que todas las mayúsculas queden representadas la fuente de estilo Fraktur Duc de Berry, y
además en un color rojo que habremos definido antes en el espacio CMYK. La fuente principal será Latin Modern Roman:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
% Definimos la familia principal, con el paquete fontspec
\usepackage{fontspec}
\setmainfont{Latin Modern Roman}

% Y la familia con Duc de Berry en rojo
\usepackage{xcolor}
\definecolor{capitales}{cmyk}{0,0.98,0.98,0}
\newfontfamily\duc[Color=capitales]{Duc De Berry LT Std}
\end{minted}

En lo que hace al código Lua, podemos crear aparte un archivo \texttt{*.lua}, pero para estos menesteres, y siendo tan poco
código, es utilísimo el paquete \texttt{luacode}. Definimos entonces una función muy simple que sustituya expresiones
regulares (clases de caracteres, en la jerga de Lua): \texttt{\%d} para los dígitos y \texttt{\%u} para las letras mayúsculas\footnote{Para este tipo de sustituciones que se operan en el \emph{input}, téngase en cuenta, no obstante, lo que se comenta en
\href{https://maciaschain.gitlab.io/lunotipia/evitar\_funcion\_lua.html}{esta otra entrada} de \emph{La lunotipia}.}:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\usepackage{luacode}

%% Código Lua
\begin{luacode}
   function cambiar ( texto )
   texto = string.gsub ( texto, "%d",   "\\textbf{%0}" )
   texto = string.gsub ( texto, "%u",   "{\\duc %0}" )
   return texto
   end
\end{luacode}
\end{minted}

Y, por último, definimos en el lado \LaTeX{} un par de comandos que activen y desactiven la función Lua:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\newcommand\cambia{\directlua{luatexbase.add_to_callback
   ( "process_input_buffer" , cambiar , "cambiar" )}}
\newcommand\descambia{\directlua{luatexbase.remove_from_callback
    ( "process_input_buffer" , "cambiar" )}}
\end{minted}

Nuestro texto quedará, finalmente, así (resultado de la compilación: fig. \vref{fig:orgd1636be}):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\cambia

Lorem Ipsum Dolor Sit Amet, 1234567890 Consectetuer Adipiscing Elit.
Donec Hendrerit Tempor Tellus. Donec Pretium Posuere Tellus. Proin
Quam Nisl, Tincidunt Et, Mattis Eget, Convallis Nec, Purus. Cum Sociis
Natoque Penatibus Et Magnis Dis Parturient Montes, Nascetur Ridiculus
Mus. Nulla Posuere. Donec Vitae Dolor. Nullam Tristique Diam Non
Turpis. Cras Placerat Accumsan Nulla. Nullam Rutrum. Nam Vestibulum
Accumsan Nisl.

\descambia

\bigskip

Lorem Ipsum Dolor Sit Amet, 1234567890 Consectetuer Adipiscing Elit.
Donec Hendrerit Tempor Tellus. Donec Pretium Posuere Tellus. Proin
Quam Nisl, Tincidunt Et, Mattis Eget, Convallis Nec, Purus. Cum Sociis
Natoque Penatibus Et Magnis Dis Parturient Montes, Nascetur Ridiculus
Mus. Nulla Posuere. Donec Vitae Dolor. Nullam Tristique Diam Non
Turpis. Cras Placerat Accumsan Nulla. Nullam Rutrum. Nam Vestibulum
Accumsan Nisl.
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/color-lua1.png}
\caption{\label{fig:orgd1636be}Resultado de la compilación, con las cifras en negrita y las mayúsculas en Fraktur}
\end{figure}

También podemos ---por complicarlo algo más--- añadir un valor de escalado (mediante \texttt{fontspec}) a la familia que
definimos para Duc de Berry. Aquí un valor exagerado a modo de demostración (resultado de la compilación: fig. \vref{fig:orgf68f35e}):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\newfontfamily\duc[Color=capitales,Scale=2.5]{Duc De Berry LT Std}
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/color-lua2.png}
\caption{\label{fig:orgf68f35e}Resultado de la compilación, con las cifras en negrita y las mayúsculas en Fraktur, pero con escala mayor}
\end{figure}
\end{document}