#+TITLE: ¿Qué le pido a un libro?
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:t" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:t" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (exporta-romanos-versalitas-html)"))
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
# +STARTUP: inlineimages
#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Exportación a LaTeX
#+LaTeX_CLASS: normal-lua-org
#+LaTeX_Header: \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
#+LaTeX_HEADER: \let\maketitleold\maketitle
#+LaTeX_HEADER: \def\maketitle{{\fontspec{Arial}\maketitleold}}
#+LaTeX_HEADER: \let\tableold\tableofcontents
#+LaTeX_HEADER: \def\tableofcontents{\tableold\bigskip}
#+LaTeX_HEADER: \def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
#+LaTeX_HEADER: \newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
#+LaTeX_HEADER: \newpagestyle{base}{\sethead[\thepage][][{\cabecera TITULO}]{{\cabecera TITULO}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
#+LaTeX_HEADER: \pagestyle{base}
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage[spanish]{varioref}
#+LaTeX_HEADER: \renewcommand{\listingscaption}{Cuadro de código}

{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none
  ;; Para LaTeX

  (defun filtro-latex-misc (texto backend info)
    "reemplaza el coamndo \ref por \vref y forma nums. romanos con versalitas"
  (when (org-export-derived-backend-p backend 'latex)
  (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "\\\\miscrom{\\1}"
			    (replace-regexp-in-string "\\\\ref{"  "\\\\vref{" texto))))

  ;; Para html
  (defun exporta-romanos-versalitas-html (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
    números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))
#+end_src


¿Qué le pido a un libro? En principio, como tal libro, que su tipografía sea excelente. Y creo, junto a don Stanley Morison, que la excelencia de una tipografía se mide en términos de invisibilidad. Que el tipógrafo no se interponga entre lo que leo y yo como un camarero importuno. Tan sólo que me haga suave y fácil el viaje y no me pierda por sinuosas calles entre párrafos. Ni que me deje llorar con líneas huérfanas. Que funcione, cuando lo necesite, la venerable matemática y que la página tenga un buen color, pero que esa matemática no acabe deviniendo lastre, pues dos más dos, en el mundo, no siempre suman cuatro. Y que sepa usar la tinta más difícil, que es el espacio en blanco. Luego, a lo que leo, simplemente le pido que me guste, y, si es posible, que me enamore en un inagotable idilio. Que el libro viva y duerma conmigo, viaje conmigo, se desgaste conmigo. Que a pesar de nuestros momentos de silencio mutuo, sepamos encontrarnos a la vuelta del tiempo como dos viejos amigos que tienen mucho que decirse, cambiados, fragmentados por los días o las noches, y sin embargo, oh misterio, siempre los mismos. Que nunca se pudra, nuevo, en las estanterías, y que la intemperie dore sus páginas como un erudito otoño, pero que venga agotado de muchas deslumbradas primaveras. Y que algún día, tal vez, nos pida ser regalado: y es que no hay libro más vivo que el que cambia de manos.


{{{pie}}}

#+begin_export html
<div>
<p>Publicado: 31/08/19 - 19:07</p>

<p>Última actualización: 31/08/19 - 19:07</p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
