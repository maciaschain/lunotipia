# -*- org-confirm-babel-evaluate: nil; -*-
#+TITLE: Composición tipográfica del Diccionario Hispánico de la Tradición Clásica: cómo se hizo
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:t" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:t" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (filtro-html-misc)"))
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
# +STARTUP: inlineimages
#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Exportación a LaTeX
#+LaTeX_CLASS: normal-lua-org

#+INDEX: trabajos propios!diccionarios!Diccionario Hispánico de la Tradición Clásica

{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none
  ;; Para LaTeX
(defun filtro-latex-misc (texto backend info)
    "reemplaza el coamndo \ref por \vref y forma nums. romanos con versalitas"
    (when (org-export-derived-backend-p backend 'latex)
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "\\\\miscrom{\\1}"
	(replace-regexp-in-string "\\\\begin{enumerate}"  "\\\\begin{enumerate}[mienu]"
	  (replace-regexp-in-string "\\\\ref{"  "\\\\vref{" texto)))))

  ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src


En este vídeo os muestro, de manera general, cómo llevé a cabo la composición tipográfica
y maquetación del Diccionario Hispánico de la Tradición Clásica, obra de referencia
dirigida por Francisco García Jurado y publicada por la editorial Guillermo Escolar. Os
invito a un recorrido por todos los elementos implicados en el proceso de producción:
Emacs/Org Mode, LaTeX, Org Publish, BibLaTeX, etc.

(/click/ en la captura del vídeo para verlo en la instancia de Invidious [[https://inv.cthd.icu/]])

#+ATTR_HTML: :target _blank
[[https://inv.cthd.icu/watch?v=JLDiU6n2GCU][file:./images/video-dhtc-captura.png]]


{{{pie}}}

#+begin_export html
<div>
<p>Publicado: 15/11/21</p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
