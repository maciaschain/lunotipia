\begin{Verbatim}[commandchars=\\\{\}]
\PYG{k}{\PYGZbs{}onclaudatur}
\PYG{k}{\PYGZbs{}poemtitle}\PYG{n+nb}{\PYGZob{}}In memory W.B. Yeats (W.H. Auden, fragmento)\PYG{n+nb}{\PYGZcb{}}
\PYG{k}{\PYGZbs{}begin}\PYG{n+nb}{\PYGZob{}}verse\PYG{n+nb}{\PYGZcb{}}
But for him it was his last afternoon as himself,\PYG{k}{\PYGZbs{}\PYGZbs{}}
An afternoon of nurses and rumours;\PYG{k}{\PYGZbs{}\PYGZbs{}}
The provinces of his body revolted,\PYG{k}{\PYGZbs{}\PYGZbs{}}
The squares of his mind were empty,\PYG{k}{\PYGZbs{}\PYGZbs{}}
Silence invaded the suburbs,\PYG{k}{\PYGZbs{}\PYGZbs{}}
The current of his feeling failed; he became his admirers.

Now he is scattered among a hundred cities\PYG{k}{\PYGZbs{}\PYGZbs{}}
And wholly given over to unfamiliar affections,\PYG{k}{\PYGZbs{}\PYGZbs{}}
To find his happiness in another kind of wood\PYG{k}{\PYGZbs{}\PYGZbs{}}
And be punished under a foreign code of conscience.\PYG{k}{\PYGZbs{}\PYGZbs{}}
The words of a dead man\PYG{k}{\PYGZbs{}\PYGZbs{}}
Are modified in the guts of the living.

But in the importance and noise of to\PYGZhy{}morrow\PYG{k}{\PYGZbs{}\PYGZbs{}}
When the brokers are roaring like beasts on the floor of the bourse,\PYG{k}{\PYGZbs{}\PYGZbs{}}
And the poor have the sufferings to which they are fairly accustomed\PYG{k}{\PYGZbs{}\PYGZbs{}}
And each in the cell of himself is almost convinced of his freedom\PYG{k}{\PYGZbs{}\PYGZbs{}}
A few thousand will think of this day\PYG{k}{\PYGZbs{}\PYGZbs{}}
As one thinks of a day when one did something slightly unusual.

What instruments we have agree\PYG{k}{\PYGZbs{}\PYGZbs{}}
The day of his death was a dark cold day.\PYG{k}{\PYGZbs{}\PYGZbs{}}
\PYG{k}{\PYGZbs{}end}\PYG{n+nb}{\PYGZob{}}verse\PYG{n+nb}{\PYGZcb{}}
\end{Verbatim}
