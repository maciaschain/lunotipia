% Created 2019-09-03 mar 20:48
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{El punto alto griego, Unicode y LuaTeX}}]{{\cabecera\MakeUppercase{El punto alto griego, Unicode y LuaTeX}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{El punto alto griego, Unicode y LuaTeX}
\begin{document}

\maketitle
El punto alto es un signo prosódico específico de la escritura griega, cuyo uso vendría a ser equivalente al del punto y
coma en la puntuación latina. El estándar Unicode lo tiene codificado en la posición \texttt{U+0387}, y lo nombra (como en el
caso del resto de caracteres de todo el rango Unicode que afecta al griego) con el término empleado en griego actual:
\emph{ano teleía}. El signo, pues, estaría bien definido de cara al diseño de fuentes y la correcta representación
tipográfica de la lengua griega. Pero el problema viene, como suele ser demasiado frecuente en Unicode, por la
duplicidad de caracteres. Tal es así, que el carácter que nos ocupa no pocas veces se confunde con el del punto medio
(\emph{middle dot}: \texttt{U+00B7}). La contaminación ya se remonta a antes de Unicode, aunque parece que en estos últimos tiempos
digitales el punto medio va ganando la partida. Sin ir más lejos, la distribución de teclado que uso en Linux para
escribir en griego tiene mapeado por defecto este punto a costa del correcto \emph{ano teleía}. Vemos, por tanto, que el
problema del punto alto resulta ser al cabo un problema de altura. Se viene a unir al juego, además, otra anomalía
habitual, y es que algunas fuentes, aun disponiendo de los dos caracteres, igualan la altura del \emph{ano teleía} con la del
punto medio, con lo cual ambos signos son indistinguibles. Probablemente los diseñadores de tipos se dejan llevar por
esta tendencia a la baja de nuestro punto griego. O, en el fondo, no hay un consenso total y la cuestión ha de quedar
abierta \emph{sine die}. En mi opinión, el punto alto debe ser eso: un punto alto. ¿A qué altura? Lo más razonable sería no
muy lejos de la altura \emph{x} de la fuente, como se ve en estos dos ejemplos tomados de las \emph{Platonis Opera} de Oxford
(fig. \vref{fig:org55f995c}) y el Dionisio de Halicarnaso de Teubner (fig. \vref{fig:org2353e76}).

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{./images/platonis-opera-punto-alto.png}
\caption{\label{fig:org55f995c}
El punto \emph{correctamente} alto en las \emph{Platonis Opera} de Oxford}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{./images/dionisio-de-halicarnaso-punto-alto-teubner.png}
\caption{\label{fig:org2353e76}
Y en Teubner}
\end{figure}

La excelente y siempre recomendable fuente Old Standard de Alexey Kryukov distingue claramente el punto medio del punto
alto (aunque este último, curiosamente, es de un peso algo mayor, como se aprecia en la fig. \vref{fig:org6340221}). Es más,
incluye la etiqueta Open Type \texttt{locl} (sustitución de formas locales para un sistema de escritura determinado), gracias a
la cual nos aseguraremos de que todo punto medio en nuestro texto griego a procesar sea sustituido por el \emph{ano teleía}:
siempre y cuando, claro, tengamos activada dicha marca y el \texttt{script} para griego.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{./images/punto_alto_old_standard.png}
\caption{\label{fig:org6340221}
Contraste de punto alto y punto medio en la fuente Old Standard}
\end{figure}

\begin{sloppypar}

En otras fuentes que dispongan de ambos caracteres bien diferenciados, pero que carezcan de esa característica Open
Type, podremos añadirla con un software editor de fuentes (Fontforge es aquí siempre el recomendado). La otra
posibilidad, si usamos Lua(La)\TeX{}, es aplicarla al vuelo, sin necesidad de editar nuestra fuente, gracias a la función
Lua \texttt{fonts.handlers.otf.addfeature} dentro de una orden \texttt{directlua}. Añadiríamos esto en el preámbulo:

\end{sloppypar}

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\directlua{
fonts.handlers.otf.addfeature{
name = "substest",
type = "substitution",
data = {
periodcentered = "anoteleia",
       },
    }
}
\end{minted}

La etiqueta, que hemos llamado (por ejemplo) \texttt{substest}, podemos activarla o desactivarla a discreción mediante un
\texttt{RawFeature=\{+substest\}} o \texttt{RawFeature=\{-substest\}}, respectivamente.

Podemos también realizar esta sustitución de forma nativa a \TeX{}, mediante la modificación de los catcodes. Tocar los
catcodes siempre es delicado y puede traer consecuencias inesperadas en nuestra compilación. La forma correcta de
hacerlo sería algo así como esto:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begingroup
\catcode`\^^^^00b7=\active
\protected\gdef^^^^00b7{\string ^^^^0387}%
\endgroup
\AtBeginDocument{%
        \catcode`\^^^^00b7=\active
}
\end{minted}

En otros contextos, tal vez queramos componer en una fuente griega que, si bien tiene definido el carácter del \emph{ano
teleia}, éste se ubica (como es habitual, ya dijimos) o bien más bajo de lo deseable o exactamente a la misma altura que
el punto medio. Vamos, que el diseñador de fuentes ha hecho un simple «copia y pega» de glifos y no se ha complicado la
vida. ¿Cómo lo arreglamos? Podemos, en principio, editar la fuente con Fontforge, y así modificar la altura de nuestro
signo. Pero en LuaTeX nos vuelve a ayudar un poco de código Lua para el preprocesado, de tal modo que nos ahorramos el
andar editando y renombrando fuentes. De nuevo, un método simple, limpio y no destructivo (para con la fuente original).

Supongamos, entonces, que queremos componer con Minion Pro, cuyo punto alto luce así de mal colocado cuando nos viene
«de fábrica» (fig. \hyperref[fig:org7a6318b]{fig:minion}).

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/puntos-altos1.png}
\caption{\label{fig:org7a6318b}
El remiso punto alto de la Minion Pro}
\end{figure}

definimos una simple función en Lua que nos reemplace tanto el punto medio como el punto alto (así nos aseguramos de que
la corrección será global) por el propio punto alto, que habremos (eso sí) encerrado en una \texttt{\textbackslash{}raisebox} para elevarlo.
Por ejemplo, declarando este comando:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\def\puntoalto{\raisebox{0.3ex}{\char"0387}}
\end{minted}

Conviene usar, por cierto, una magnitud relativa como \texttt{ex} y no absoluta (puntos, milímetros, etc). Así el punto se
ubicará siempre igual independientemente del cuerpo de letra empleado.

Nuestra función en Lua la podemos encerrar en un entorno \texttt{luacode}, proporcionado por el paquete del mismo nombre:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begin{luacode}
   function cambiar_puntoalto ( texto )
   texto = string.gsub ( texto, "·",   "\\puntoalto" ) -- sustituye punto medio
   texto = string.gsub ( texto, "·",   "\\puntoalto" ) -- sustituye punto alto original
    return texto
   end
\end{luacode}
\end{minted}

Y en lo que hace a la parte \LaTeX{}, un par de comandos que active o desactive la función:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\newcommand\anoteleia{\directlua{luatexbase.add_to_callback
   ( "process_input_buffer" , cambiar_puntoalto , "cambiar_puntoalto" )}}
\newcommand\middledot{\directlua{luatexbase.remove_from_callback
    ( "process_input_buffer" , "cambiar_puntoalto" )}}
\end{minted}

Y, por último, nuestro mínimo ejemplo funcional (resultado de la compilación en la fig. \vref{fig:org07eb544}):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begin{document}

   πέπονθα· (con punto alto original)

     \anoteleia % Sustituye los 2 puntos a nuestro punto alto, pero alto de verdad ;-)

  πέπονθα· / πέπονθα· (con punto medio / punto alto original)

     \middledot % Deja las cosas como estaban

  πέπονθα· (con punto medio)

\end{document}
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/puntos-altos2.png}
\caption{\label{fig:org07eb544}
Compilación del ejemplo}
\end{figure}

\textbf{Y estrambote}: En  \href{https://vimeo.com/341114471 }{este vídeo} muestro un ejemplo real (esta vez con la fuente Linux Libertine), dentro de un libro que
compuse recientemente.
\end{document}