#+TITLE: «Tonos» y «oxia», o el duplicado acento agudo en griego
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:t" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:t" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (exporta-romanos-versalitas-html)"))
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
# +STARTUP: inlineimages
#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Exportación a LaTeX
#+LaTeX_CLASS: normal-lua-org
#+LaTeX_Header: \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
#+LaTeX_HEADER: \let\maketitleold\maketitle
#+LaTeX_HEADER: \def\maketitle{{\fontspec{Arial}\maketitleold}}
#+LaTeX_HEADER: \let\tableold\tableofcontents
#+LaTeX_HEADER: \def\tableofcontents{\tableold\bigskip}
#+LaTeX_HEADER: \def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
#+LaTeX_HEADER: \newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
#+LaTeX_HEADER: \newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{«Tonos» y «oxia», o el duplicado acento agudo en griego}}]{{\cabecera\MakeUppercase{«Tonos» y «oxia», o el duplicado acento agudo en griego}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
#+LaTeX_HEADER: \pagestyle{base}
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage[spanish]{varioref}
#+LaTeX_HEADER: \renewcommand{\listingscaption}{Cuadro de código}

#+INDEX: tipografía griega!distinción de acentos agudos
#+INDEX: LuaTeX!=fonts.handlers.otf.addfeature=!sustitución de acentos agudos en griego
#+INDEX: Unicode!distinción de acentos en griego básico y extendido

{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none
  ;; Para LaTeX

  (defun filtro-latex-misc (texto backend info)
    "reemplaza el coamndo \ref por \vref y forma nums. romanos con versalitas"
  (when (org-export-derived-backend-p backend 'latex)
  (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "\\\\miscrom{\\1}"
			    (replace-regexp-in-string "\\\\ref{"  "\\\\vref{" texto))))

  ;; Para html
  (defun exporta-romanos-versalitas-html (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
    números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))
#+end_src

#+begin_export html
<form>
<p style="text-align: center;"><button formaction="https://maciaschain.gitlab.io/lunotipia/pdf/tonos_oxia.pdf"><font size="2">Descargar esta entrada en formato PDF</font></button></p>
</form>
#+end_export

Nadie duda a estas alturas de la gran bendición que supuso un estándar como Unicode para el tratamiento digital de los
textos. Y la justicia y reparación que vino con ello, pues las computadoras se veían por fin capaces de entender y
representar cualquier sistema de escritura humano más allá del latino, tanto actual como antiguo, ya fuese usado por
muchos, ya minoritario y casi desconocido. O, al menos, aspirar a semejante ideal, una meta que cada vez se antoja más
cercana con cada edición oficial del estándar. Desde los toscos grafos oghámicos a las intrincadas escrituras de Asia o
de África, pasando por silabarios, alfabetos, notaciones musicales y símbolos de toda clase, por supuesto que no podría
faltar en esta ordenada fiesta y encasillado bullicio la venerable escritura griega. De la cual volvemos a hablar aquí a
cuento de una pequeña y curiosa inconsistencia que viene acarreando el estándar (Unicode no es perfecto) desde los
propios orígenes de la inclusión del griego. La cuestión afecta al acento agudo y, si no andamos con el debido cuidado,
puede complicarnos innecesariamente la vida a la hora de trabajar digitalmente con un texto en caracteres helenos.

* Dos acentos agudos ¿innecesarios?
   :PROPERTIES:
   :CUSTOM_ID: Dos acentos agudos ¿innecesarios?
   :END:

Como bien sabe (o debería saber) todo aquel que trabaja en un ordenador con textos griegos, Unicode tiene asignadas dos
«parcelas» bien delimitadas para la correcta representación de esta lengua. A una la llama «griego» y a la otra «griego
extendido». La primera incluye el alfabeto completo, los signos de puntuación y todas las vocales acentuadas como
caracteres precompuestos. O sea, la vocal de turno y, sobre ella, el único acento que desde la reforma monotónica del 82
admite la escritura griega actual, y que no es sino un acento agudo como el que tenemos en español y al cual
impropiamente llamamos «tilde». Por otro lado, la parcela de griego extendido comprende todos los caracteres
precompuestos (vocales, mayormente) con el juego y combinatoria de diacríticos necesarios para la escritura del griego
politónico, que es la manera de representar el griego «antiguo» y el griego anterior a la mencionada reforma, pues a
pesar de que la lengua sólo cuenta desde hace mucho tiempo con un único acento intensivo, se seguía escribiendo (por
cuestiones más culturales que pragmáticas) con los signos de los espíritus y la ancestral tríada de acentos agudo, grave
y circunflejo.

#+CAPTION: Área de griego básico en la fuente EB Garamond (visualizada en FontForge)
#+NAME: ebgaramond1
[[./images/ebgaramond1.png]]

#+CAPTION: Área de griego extendido en la misma fuente
#+NAME: ebgaramond2
[[./images/ebgaramond2.png]]

Naturalmente, las fuentes pueden incluir ambas parcelas o sólo la primera, que es la básica. Si se da este último caso,
únicamente podremos escribir o representar griego monotónico. Hasta aquí, todo perfecto y, más o menos, dentro de la
habitual coherencia de Unicode. Pero trazar una línea divisoria tan drástica entre los sistemas mono y politónicos nos
llevó también a una inesperada duplicidad de acentos agudos. De tal suerte que Unicode presenta un acento para el área
monotónica, al cual llama con el término griego /tonos/, y otro para el griego extendido politónico, que denomina,
también en griego, /oxia/. Esto ya de por sí es una inconsistencia, pues la distinción que hace aquí el estándar no es
gráfica sino histórica. Y nos da a entender algo que no se corresponde con la realidad, como si la reforma monotónica
estableciese un nuevo signo diacrítico cuando lo que hizo fue simplemente eliminar de la escritura normativa aquellos
signos innecesarios por representar acentos y hechos fonológicos ya dejados muy atrás por la lengua griega en su
constante discurrir. En dos palabras, se descartaron los acentos grave y circunflejo (además de los dos espíritus que
marcaban la aspiración inicial o la ausencia de ella), y se dejó sólo el acento agudo. En lo que atañe al plano de la
letra escrita, y al margen de la fonología y de la historia, este acento, por tanto, es y debe ser siempre el mismo.

A pesar de la inconsistencia, no tendríamos que temer ningún inconveniente, si al fin y al cabo ambos acentos son
idénticos. Sin embargo, el problema de verdad (al menos a efectos prácticos) viene ahora. Y es que no pocas fuentes,
manteniéndose fieles a esta distinción espuria que hace Unicode, incluyen dos diseños distintos para los dos
acentos. Mientras que el acento agudo (el /oxia/ del área politónica) aparece como un acento agudo normal y corriente,
el /tonos/, por contra, queda casi (cuando no totalmente) perpendicular a la línea base de la letra. Este diseño, por
cierto, no es arbitrario sino que proviene de una moda, por no decir plaga, relativamente reciente en la tipografía
hecha en Grecia, como una especie de autoafirmación en el nuevo estilo monotónico frente al preterido politonismo. Al
margen de la cuestión meramente estética, y de que tal signo es difícilmente combinable con los espíritus y el
circunflejo, ni se le puede anteponer claramente un acento grave debido a esa verticalidad, el problema que salta a la
vista es que contamos con dos acentos agudos que pueden muy fácil acabar mezclándose en un texto griego, ya sea
monotónico o politónico. Y si vamos a componer en una fuente que distinga gráficamente el /tonos/ del /oxia/, el aspecto
de nuestro texto también se volverá inconsistente.

¿Es habitual esta confusión de acentos? Sin ninguna duda: digamos que está a la orden del día. Por diversas causas, y
entre ellas no es la menos importante el hecho de que los teclados para escribir en griego politónico tienen casi
siempre mapeado el signo del /tonos/ a costa del /oxia/. Y podemos hacer también una prueba muy sencilla. Copiar, como
he hecho yo, cualquier palabra tomada al azar de los textos del [[http://www.perseus.tufts.edu/hopper/][Perseus-Hopper]] (por ejemplo, de Heródoto), que contenga
alguna letra con acento agudo, y la pegamos en un editor de texto que pueda interpretar el código hexadecimal de los
caracteres Unicode. Para ello yo uso Yudit. Y la palabra que he escogido, ἀπόδεξις. Vemos que en la pantalla de Yudit
(fig. [[yudit]]), si situamos el cursor a la altura del signo «ό», se nos informa que se trata del carácter Unicode que
lleva el código =U+03CC=. Éste no es otro que el denominado en la jerga del estándar /GREEK SMALL LETTER OMICRON WITH
TONOS/. Así pues, el /Proyecto Perseo/ está usando el /tonos/ y no el esperable /oxia/ en sus textos de griego antiguo.

#+CAPTION: Una prueba en el editor de texto plano Unicode Yudit
#+NAME: yudit
[[./images/yudit.png]]

Si un texto así lo procesamos con una fuente que distinga gráficamente ambos acentos, se notará a simple vista la
diferencia. Por fortuna, son más las tipografías que usan un único signo para el /tonos/ y el /oxia/.  Pero las de la
otra cuerda haberlas haylas, y no están por cierto entre las menos conocidas. De unas cuantas de éstas, muy populares,
podemos citar las Noto de Google o la Minion Pro de Adobe. En la fig. [[minion]] se muestra cómo luciría la palabra
ἀπόδεξις en la Minion, tal y como está copiada de los textos del /Proyecto Perseo/, con la ómicron y el /tonos/. Justo
debajo, la misma palabra en la misma fuente, pero esta vez con el carácter de la ómicron y el oxia (=U+1F79=).

#+CAPTION: Contraste entre /oxia/ y /tonos/ en la fuente Minion Pro
#+NAME: minion
[[./images/minion1.png]]

Y la diferencia de inclinación se hace, si cabe, más evidente cuando comparamos (fig. [[minion2]]) la /ómicron/ con el
/tonos/ frente a la /ómicron/ con el espíritu suave y el /oxia/.

#+CAPTION: Contraste entre /oxia/ y /tonos/ en la fuente Minion Pro (2)
#+NAME: minion2
[[./images/minion2.png]]

Otro ejemplo notable lo tenemos en la tipografía FiraGo, una /sans/ humanística auspiciada por la fundación Mozilla
(fig. [[firago1]]).

#+CAPTION: Contraste entre /tonos/ y /oxia/ en la fuente FiraGo
#+NAME: firago1
[[./images/tonos-oxia-firago.png]]

* Una solución en LuaTeX
   :PROPERTIES:
   :CUSTOM_ID: Una solución en LuaTeX
   :END:

@@latex:\begin{sloppypar}@@

Como se ve, si queremos trabajar en griego con alguna de estas fuentes no nos queda otra que normalizar el texto. Lo más
sensato sería procurar que todos los acentos agudos pasen a ser /oxia/. Una forma de hacerlo es editar la fuente
mediante un /software/ como Fontforge. Pero si no queremos alterar nuestra fuente, se me ocurre una manera muchísimo más
simple si estamos componiendo en Lua(La)TeX, que consiste en echar mano ---una vez más--- de la siempre utilísima
función Lua =fonts.handlers.otf.addfeature=, que nos permite (gracias a la inclusión de código Lua en nuestro
documento (La)TeX) modificar o aplicar ciertas características OpenType al vuelo, como las de substitución de
caracteres. Así pues, podemos definir los reemplazos necesarios para las vocales con /tonos/, bajo la nueva
característica OpenType que hemos bautizado «tonosoxia»:

@@latex:\end{sloppypar}@@

#+begin_src latex :exports code
\directlua{
fonts.handlers.otf.addfeature{
name = "tonosoxia",
type = "substitution",
data = {
alphatonos = "ά",
epsilontonos = "έ",
etatonos = "ή",
iotatonos = "ί",
omicrontonos = "ό",
omegatonos = "ώ",
upsilontonos = "ύ",
		},
	}
}
#+end_src

Y a continuación hacemos la prueba con la fuente FiraGo para comparar el texto con y sin conversión. A tal efecto,
definimos (con =fontspec=) dos familias para griego con FiraGo. Una, de base, que se comporte «normal»:

#+begin_src latex :exports code
\usepackage{fontspec}
\newfontfamily\sinoxia{FiraGo}
#+end_src

Y otra a la que le aplicaremos la nueva característica de substitución. Por tanto, convertirá automáticamente en la
compilación cualquier letra con /tonos/ en la misma letra con /oxia/:

#+begin_src latex :exports code
\newfontfamily\oxia[RawFeature={+tonosoxia}]{FiraGo}
#+end_src

Y, por último, en nuestro documento de prueba escribimos las dos series de vocales con /tonos/:

#+begin_src latex :exports code
\begin{document}

\sinoxia

ά έ ή ί ό ώ ύ

\oxia

ά έ ή ί ό ώ ύ

\end{document}
#+end_src

Con lo que obtendremos el esperado resultado (fig. [[compilacion]]) tras la compilación.

#+CAPTION: Resultado de la compilación con la doble serie de acentos
#+NAME: compilacion
[[./images/tonos-oxia-luatex.png]]

{{{pie}}}

#+begin_export html
<div>
<p>Publicado: 05/09/19 - 14:58</p>

<p>Última actualización: 05/09/19 - 14:58</p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
