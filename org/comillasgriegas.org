#+TITLE: Tipografía griega y comillas
#+AUTHOR: Juan Manuel Macías
#+MACRO: numeracion (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: num:t" "#+OPTIONS: num:nil"))
#+MACRO: contenido (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+OPTIONS: toc:nil" "#+OPTIONS: toc:nil"))
#+MACRO: pie (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "* COMMENT ∞" "* ∞"))
#+MACRO: filtro (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) "#+BIND: org-export-filter-final-output-functions (filtro-latex-misc)" "#+BIND: org-export-filter-final-output-functions (filtro-html-misc)"))
#+SETUPFILE: ~/Git/lunotipia/html-lunotipia.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
{{{numeracion}}}
{{{contenido}}}
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
# +STARTUP: inlineimages
#+OPTIONS: tags:nil
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Exportación a LaTeX
#+LaTeX_CLASS: normal-lua-org
#+LaTeX_Header: \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
#+LaTeX_HEADER: \let\maketitleold\maketitle
#+LaTeX_HEADER: \def\maketitle{{\fontspec{Arial}\maketitleold}}
#+LaTeX_HEADER: \let\tableold\tableofcontents
#+LaTeX_HEADER: \def\tableofcontents{\tableold\bigskip}
#+LaTeX_HEADER: \def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
#+LaTeX_HEADER: \newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
#+LaTeX_HEADER: \newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Tipografía griega y comillas}}]{{\cabecera\MakeUppercase{Tipografía griega y comillas}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
#+LaTeX_HEADER: \pagestyle{base}
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage[spanish]{varioref}
#+LaTeX_HEADER: \renewcommand{\listingscaption}{Cuadro de código}

#+INDEX: tipografía griega!las comillas correctas

{{{filtro}}}
#+begin_src emacs-lisp :exports results :results none
  ;; Para LaTeX

  (defun filtro-latex-misc (texto backend info)
    "reemplaza el coamndo \ref por \vref y forma nums. romanos con versalitas"
    (when (org-export-derived-backend-p backend 'latex)
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "\\\\miscrom{\\1}"
				(replace-regexp-in-string "\\\\ref{"  "\\\\vref{" texto))))

  ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

#+begin_export html
<form>
<p style="text-align: center;"><button formaction="https://maciaschain.gitlab.io/lunotipia/pdf/comillasgriegas.pdf"><font size="2">Descargar esta entrada en formato PDF</font></button></p>
</form>
#+end_export

@@html:<style> .figure-number {display: none;}</style>@@

#+CAPTION:
#+NAME: xx
#+ATTR_LaTeX: :placement [!h] :width 0.85\textwidth
[[./images/comillas-oxford.jpg]]

#+CAPTION:
#+NAME: yy
#+ATTR_LaTeX: :placement [!h] :width 0.85\textwidth
[[./images/comillas_bude.jpg]]

#+CAPTION:
#+NAME: zz
#+ATTR_LaTeX: :placement [!h] :width 0.85\textwidth
[[./images/comillas_teubner.jpg]]

Tres muestras de ediciones críticas: de Oxford, de la colección Budé (Les Belles Lettres) y de Teubner. Gran Bretaña,
Francia y Alemania. Con sus estilos tan reconocibles, estas tres editoriales representan el canon para la confección de
una edición crítica, no sólo en el ámbito académico sino también en el tipográfico. Y sobre este último, precisamente,
me apetece dejar aquí un pequeño comentario.

De la composición que asoma en los tres ejemplos, qué decir que no sean alabanzas hacia esos buenos tiempos cuando el
mundo tenía tipógrafos sabios y el humanismo de la producción editorial no había claudicado bajo la ameba amorfa del
«diseño gráfico». Pero también en las tres es bien visible una anomalía. Entiéndase: no es que los cajistas cometieran
un despiste, pues su trabajo es impoluto. La alarma vendría más bien del lado de lo que podríamos llamar tipografía
perversa. O un ejemplo de cómo en un mundo tan aparentemente neutro como el de la imprenta pueden colarse ciertos vicios
culturales.

Si se fijan, la edición oxoniense emplea las comillas altas, que son las correctas para el inglés. La francesa, las
comillas latinas con el espaciado (para comillas e incluso signos de puntuación) que es preceptivo del francés impreso.
Y la alemana, las habituales comillas teutonas bajas/altas. En los tres casos son las comillas correctas para el país
donde se publica cada edición. Salvo que el texto editado está en griego. Da la sensación de que para el editor inglés,
francés o germano el griego no tiene tradición tipográfica que respetar y ha de asimilarse sin más miramientos a la
lengua anfitriona. O fagocitadora. Podemos pensar que la obra no es del autor griego de la antigüedad en el fondo, sino
del estudioso filólogo que la anota y que, además, orquesta todos los comentarios eruditos que el tiempo ha ido depositando
sobre ella. Y no deja de ser cierto, lo cual también es inquietante. Pero habría que ver cómo se tomarían británicos,
franceses o alemanes el ver a Shakespeare, a Molière o a Rilke editados por esos anchos y variados mundos de Dios con
comillas foráneas e intrusas a su tradición tipográfica. En cualquier caso, lo que no deja de estar patente aquí, a mi
juicio, es una versión portátil del habitual paternalismo colonizador que el (digamos) occidente culto ha venido
adoptando hacia Grecia. El lema sería algo así como /lo griego sin Grecia/ o, mejor aún, /sin griegos/.

Es innegable lo mucho y bueno que los impresores y tipógrafos de ese culto occidente han hecho por el sistema de
escritura griego, ya desde el Renacimiento y los Grecs du Roi del gran Claude Garamond, quien se inspiró, por otra
parte, en la cursiva de volutas bizantinas que traían los peritos escribas migrados de Grecia. A la vuelta de los
siglos, la influencia fue recíproca, y es así que en Grecia se acabaron adoptando los tipos fundidos por otro francés,
Firmin Didot, como una tipografía casi autóctona y nacional. Y los libros compuestos en estilo /aplá/ (= «simple»), como
se conoce en la tradición impresora griega la composición en Didot, llegaron a ser un rasgo distintivo que, aún hoy y
por fortuna, sigue vigente allí, a pesar de la cada vez más omnipresente Times, para lo bueno y lo malo, todo hay que
decirlo.

La tipografía griega, en suma, está viva al igual que los griegos; y con ella, sus propias normas ortotipográficas para
la escritura griega, que deben respetarse no sólo en los libros impresos en Grecia sino en cualquier otra parte donde se
imprima un texto griego. Y eso también es extensible a lugares como Oxford, París o Leipzig. De modo que si queremos
practicar una correcta observancia de tales normas, en lo que a las comillas respecta tendremos que hacer uso de las
comillas latinas «», como en español o francés, pero sin el espaciado de éste.

@@latex:\bigskip@@

*(Estrambote)* Por cierto, las comillas latinas en la tipografía Didot no tienen esas esquinas agudas y cortantes que
se ven en otras fuentes. Sus trazos son de una rara sensualidad, como una caricia tímida a lo que encierran y acaso no
quieren encerrar: corteses y elegantes para algo tan «impertinente» como son las comillas.

#+CAPTION:
#+NAME: dd
#+ATTR_LaTeX: :placement [!h] :width 0.5\textwidth
[[./images/comillas_didot.png]]


{{{pie}}}

#+begin_export html
<div>
<p>Publicado: 02/11/19</p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
