% Created 2022-01-04 mar 21:25
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
	  \usepackage{normal-lua}
	 	 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera LA SUSTITUCIÓN CONTEXTUAL OPENTYPE EN LUATeX}]{{\cabecera LA SUSTITUCIÓN CONTEXTUAL OPENTYPE EN LUATeX}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{La sustitución contextual OpenType en LuaTeX\\\medskip
\large (y un ejemplo con la escritura griega)}
\begin{document}

\maketitle
\index{Open Type!sustitución contextual}
\index{LuaTeX!=fonts.handlers.otf.addfeature=!sustitución contextual}
\index{tipografía griega!caracteres estilísticos alternativos}

Una de las características más atractivas que ofrece la tecnología OpenType es aquella que consiste en sustituir unas
letras por otras dependiendo de un determinado contexto. Puede usarse para resolver ciertas necesidades de tipografía
pragmática, pero también habrá escenarios para satisfacer otras de índole más bien estilística o histórica, casi siempre
sepultadas en el olvido tras la llegada de los ordenadores al mundo de la producción editorial. A este segundo grupo
pertenecería la sustitución en mitad de palabra de la letra griega beta minúscula β por su variante estilística curvada
(o beta sin descendente) ϐ, procedimiento muy habitual en la tradición impresora francesa del pasado siglo y que,
aplicado en los textos y en las condiciones adecuadas, puede crear un efecto muy grato al lector (o al menos al lector
que soy yo).

Tenemos (si somos consecuentes con la tesis Unicode que distingue caracteres de glifos) un único carácter para la letra
beta, idea platónica que puede concretarse textualmente en dos posibles glifos: el de la beta «normal» con descendente y
el de la beta curvada, que el estándar Unicode sitúa bajo el código \texttt{U+03D0} y que denomina «Greek beta symbol» (fig.
\vref{fig:org71bdf32})

Lo que viene a aportar OpenType es el añadir a la fuente tipográfica una propiedad ---diga\-mos--- tridimensional, que
permite que el carácter opte por un glifo u otro según una serie de instrucciones que la fuente pueda incluir. Basta con
activar la etiqueta adecuada (que casi siempre suele ser \texttt{calt}, de «contextual alternates»), por ejemplo en nuestro
\LaTeX{} con el paquete Fontspec. A los efectos de transmisión textual ---y esto es lo esencial, insistimos--- ambos
glifos siguen siendo el mismo carácter beta, como podemos comprobar si copiamos cualquier pasaje de un PDF donde se haya
operado la sustitución contextual y lo pegamos en un editor de texto.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{./images/beta_symbol.png}
\caption{\label{fig:org71bdf32}Descripción del carácter Unicode \texttt{GREEK BETA SYMBOL}}
\end{figure}

Ahora bien, OpenType se limita a ofrecer los protocolos necesarios. Otro cantar es hasta qué punto los implementan los
diseñadores de fuentes. Por supuesto, si nuestra fuente no dispone de esa característica, como sucede en la gran mayoría
de fuentes que incluyen soporte para la escritura griega, siempre podremos añadirla nosotros mismos mediante el editor
de fuentes \href{http://fontforge.github.io/en-US/}{Fontforge}. Pero hoy toca hablar aquí de otra manera mucho más simple, si cabe, de hacerlo, que es dentro de
\LaTeX{} y echando mano de la utilísima función Lua \texttt{fonts.handlers.otf.addfeature}, que nos permite definir en el
pre-procesado del material textual ciertas características OpenType «al vuelo», gracias a la habilidad que \LaTeX{}
tiene de \guillemotleft{}injertar\guillemotright{}, como su propio nombre anuncia, \emph{scripts} y código Lua en el proceso de compilación. \LaTeX{}
incluye en sus engranajes un intérprete de Lua, lo que supone que podemos puentear las primitivas de \TeX{} mediante
este versátil y ligero lenguaje. En lo que atañe a nuestra deseada sustitución, la ventaja de hacerlo así es evidente,
pues añadiremos la propiedad OpenType a cualquier fuente que se nos antoje, sin necesidad de editarla y de crear
versiones modificadas de ésta. Siempre y cuándo, claro, la fuente disponga de los glifos necesarios con que jugar, pues
\texttt{fonts.handlers.otf.addfeature} no crea letras de la nada, sino que opera sobre la posición y el contexto de los glifos
de que ya dispone la fuente.

Veamos una prueba, por ejemplo, con la fuente Linux Libertine, que sí dispone de esos glifos. Añadimos lo siguiente a
nuestro archivo fuente de \LaTeX{}, convenientemente encerrado en la primitiva \texttt{\textbackslash{}directlua}, necesaria para poder
insertar código Lua.

Empezamos por definir una variable y asignarle una tabla de todos los caracteres griegos que deben ir antes del glifo a
sustituir:

\begin{minted}[frame=lines,linenos=true,breaklines]{lua}
letras_griegas = { "α", "ἀ", "ἁ", "ἂ", "ἃ", "ἄ", "ἅ", "ἆ", "ἇ",
"Ἀ", "Ἁ", "Ἂ", "Ἃ", "Ἄ", "Ἅ", "Ἆ", "Ἇ", "ε", "ἐ", "ἑ", "ἒ", "ἓ", "ἔ", "ἕ",
"Ἐ", "Ἑ", "Ἒ", "Ἓ", "Ἔ", "Ἕ", "η", "ἠ", "ἡ", "ἢ", "ἣ", "ἤ", "ἥ", "ἦ", "ἧ",
"Ἠ", "Ἡ", "Ἢ", "Ἣ", "Ἤ", "Ἥ", "Ἦ", "Ἧ", "ι", "ἰ", "ἱ", "ἲ", "ἳ", "ἴ", "ἵ", "ἶ", "ἷ",
"Ἰ", "Ἱ", "Ἲ", "Ἳ", "Ἴ", "Ἵ", "Ἶ", "Ἷ", "ο", "ὀ", "ὁ", "ὂ", "ὃ", "ὄ", "ὅ",
"Ὀ", "Ὁ", "Ὂ", "Ὃ", "Ὄ", "Ὅ", "υ", "ὐ", "ὑ", "ὒ", "ὓ", "ὔ", "ὕ", "ὖ", "ὗ",
"Ὑ", "Ὓ", "Ὕ", "Ὗ", "ω", "ὠ", "ὡ", "ὢ", "ὣ", "ὤ", "ὥ", "ὦ", "ὧ",
"Ὠ", "Ὡ", "Ὢ", "Ὣ", "Ὤ", "Ὥ", "Ὦ", "Ὧ",
"ὰ","ά","ὲ","έ","ὴ","ή","ὶ","ί","ὸ","ό","
ὺ","ύ","ὼ","ώ","ᾀ","ᾁ","ᾂ","ᾃ","ᾄ","ᾅ","ᾆ","ᾇ",
"ἈΙ","ᾉ", "ἊΙ","ᾋ", "ἌΙ","ᾍ", "ἎΙ","ᾏ",
"ᾐ","ᾑ","ᾒ","ᾓ", "ᾔ","ᾕ","ᾖ","ᾗ","ἨΙ","ᾙ",
"ἪΙ","ᾛ", "ἬΙ","ᾝ", "ἮΙ","ᾟ", "ᾠ","ᾡ","ᾢ","ᾣ","ᾤ","ᾥ","ᾦ","ᾧ",
"ὨΙ","ᾩ", "ὪΙ", "ᾫ", "ὬΙ","ᾭ", "ὮΙ","ᾯ",
"ᾰ","ᾱ","ᾲ","ᾳ","ᾴ","ᾶ","ᾷ","Ᾰ","Ᾱ","ῆ","ῇ",
"ῖ","ῗ","Ῐ","Ῑ","ῠ","ῡ","ῢ","ΰ","ῤ","ῥ","ῦ","ῧ","Ῠ","Ῡ","Ῥ",
"ῲ","ῳ","ῴ","ῶ","ῷ", "Ά","Έ","Ή","Ί","Ό","Ύ","Ώ","ΐ",
"Α","Β","Γ","Δ","Ε","Ζ","Η","Θ","Ι","Κ",
"Λ","Μ","Ν","Ξ","Ο","Π","Ρ","Σ","Τ","Υ","Φ","Χ","Ψ","Ω","Ϊ","Ϋ",
"ά", "έ","ή","ί","ΰ","α","β","γ","δ","ε","ζ","η","θ",
"ι","κ","λ","μ","ν","ξ","ο","π","ρ","ς","σ","τ","υ",
"φ","χ","ψ","ω","ϓ","ϔ","ϕ","ϖ","ϗ", "Ϙ", "ϙ","Ϛ","ϛ","Ϝ","ϝ", "ϐ" }
\end{minted}

Y a continuación, la nueva propiedad OpenType:

\begin{minted}[frame=lines,linenos=true,breaklines]{lua}
fonts.handlers.otf.addfeature{
   name = "contextualtest",
   type = "chainsustitution",
   lookups = {
      {
	 type = "sustitution",
	 data = {
	    ["β"] = "ϐ",
	 },

      },
   },
   data = {
      rules = {
	 {
	    before  = { letras_griegas  },
	    current = { { "β" } },
	    lookups = { 1 },
	 },
      },
   },
}
\end{minted}

Explicándolo un poco a trazo grueso. Hemos creado una propiedad OpenType que llamamos \texttt{contextualtest}, y declaramos que
es de tipo \texttt{chainsustitution}, es decir, queremos definir una cadena de sustitución contextual. Le incluimos un
\emph{lookup} que indica en qué consiste la sustitución (\guillemotleft{}β\guillemotright{} a \guillemotleft{}ϐ\guillemotright{}), y más abajo añadimos las reglas. Las cuales consisten en
que todos los caracteres encerrados bajo la variable \texttt{before} propiciarán la sustitución de beta simple a curvada
\emph{sólo} si ellos se sitúan inmediatamente antes de la letra.

Bien, ya casi lo tenemos. Esto nos soluciona el 99 \% de los contextos para sustituir la beta en mitad de
palabra. Pero la queremos, precisamente, así, en mitad de palabra y no al final, lo que no evitaría el código
anterior. Naturalmente, son muy raros los casos en que aparezca una beta en esa posición, si pensamos en la escritura
griega real. Pero algún que otro contexto puede surgir, así que sumamos al anterior este otro código, que evitará que
nuestra beta se sustituya por la variante curvada al final de una palabra:

\begin{minted}[frame=lines,linenos=true,breaklines]{lua}
fonts.handlers.otf.addfeature{
   name = "contextualtest2",
   type = "chainsustitution",
   lookups = {
      {
	 type = "sustitution",
	 data = {
	    ["ϐ"] = "β",
	 },
      },
   },
   data = {
      rules = {
	 {
	    after  = { { " ", 0xFFFC, ",", ".", "´", "·"} },
	    current = { { "ϐ" } },
	    lookups = { 1 },
	 },
      },
   },
}
\end{minted}

Y pasamos a probar nuestro código, cargando las dos nuevas propiedades OpenType con \texttt{fontspec}. Definimos entonces, para
la Linux Libertine, dos familias, con y sin sustitución (resultado de la compilación en la fig. \vref{fig:org8594f95}):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\documentclass{article}
\usepackage{fontspec}
% Aquí iría todo el código Lua...
\setmainfont{Linux Libertine}
\usepackage[greek.ancient]{babel}
\newfontfamily\nobetasub{Linux Libertine}
\newfontfamily\sibetasub[RawFeature={+contextualtest,+contextualtest2}]{Linux Libertine}
\begin{document}
\nobetasub
βαρβάρων - ἐβούλετο - ιβ´
\sibetasub
βαρβάρων - ἐβούλετο - ιβ´
\end{document}
\end{minted}


\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/contextual2.png}
\caption{\label{fig:org8594f95}Resultado de la compilación}
\end{figure}
\end{document}