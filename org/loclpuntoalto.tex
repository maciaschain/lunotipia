% Created 2019-10-14 lun 22:38
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{De nuevo el punto alto y la etiqueta 'locl'}}]{{\cabecera\MakeUppercase{De nuevo el punto alto y la etiqueta 'locl'}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{De nuevo el punto alto y la etiqueta 'locl'}
\begin{document}

\maketitle
\tableofcontents

\index{Open Type!etiqueta 'locl'}
\index{Tipografía griega!punto alto!etiqueta 'locl' (propiedad Open Type)}

\section{Ese terco punto a la baja}
\label{sec:orgdf1db88}

Mis últimas lecturas de textos griegos publicados recientemente (y no tan recientemente) en España vienen a confirmar lo
que, por otra parte, tampoco necesitaba de mucha confirmación: que el problema del punto alto, que aquí tratamos en \href{https://maciaschain.gitlab.io/lunotipia/punto\_alto.html}{una
anterior entrada}, ha devenido ya pandemia difícil de atajar. Son rémoras bastante gruesas, al menos, que comentamos
prolijamente en su momento: el gran error de Unicode de recomendar, e incluso imponer, el carácter del punto
medio a costa del punto alto; la ¿dejadez? de los diseñadores de tipos a la hora de concederle una adecuada altura al
punto alto (\emph{ano teleia}) real, probablemente influidos por lo primero; y, cómo no, la habitual ignorancia militante
hacia la tipografía griega o la tipografía digital en general por parte de los responsables de la producción editorial.
Que en el fondo no es ignorancia (toda ignorancia es curable a base de curiosidad y aplicación), sino más bien una terca
impermeabilidad sumada a un ya proverbial conformismo.

Un caso típico de punto alto mal ubicado lo encontré hace poco en esta edición de \emph{Ismene} de Ritsos (ver \vref{fig:org24910ef}),
publicada por Acantilado en 2012, con (por otra parte, notable) traducción de Selma Ancira. Sus páginas son por cierto
bastante pródigas en ejemplos de este error tipográfico, donde ya no sabemos si lo que se está empleando es el carácter
del punto medio (diseñado en esa fuente de estilo Times un poco más alto de lo esperable) o el del punto alto (menos
probable, me temo), situado más bajo de lo que sería correcto, como sucede en la mayoría de las fuentes con soporte para
escritura griega. Ambos inconvenientes, inaceptables en cualquier caso para la tipografía de esta lengua, se hubiesen
solventado si se hubiese utilizado un sistema de tipografía digital real para la confección de estas páginas: es decir,
si se hubiera empleado \TeX{} y lo hubiera hecho alguien con los conocimientos adecuados\footnote{Aunque el punto alto es aquí lo que nos ocupa, no resulta el único error tipográfico que afea el texto griego.
Podemos citar, entre otros, el \emph{kerning} defectuoso de algunos pares de letras (véase, en la imagen, las letras ίπ en
τίποτα, tercera línea por debajo), la ausencia de \guillemotleft{}colgado\guillemotright{} de diacríticos al margen (preceptivo para la \href{https://revistacuadernoatico.com/apuntestipograficos/2019/06/09/luatex-y-cavafis/\#el-colgado-de-los-diacr\%C3\%ADticos}{poesía en
griego politónico}) o la falta de un correcto corte por guiones, probablemente porque los programas de autoedición
típicos (QuarK Xpress, InDesign) carecen de los patrones de silabado para griego politónico y griego antiguo.}. Porque ---no lo
olvidemos--- \TeX{} es sólo la herramienta.

El tipo de letra empleado también me sugiere el siguiente comentario. Si se hubiese optado por LuaTeX como motor
tipográfico, y si hubiésemos querido mantener ese estilo Times, yo me habría tirado de cabeza sin pensar hacia la fuente
\textbf{Tempora LGC Unicode} de Alexej Kryukov. No sólo es de mayor calidad y más refinada que la del texto del ejemplo\footnote{El prof. Kryukov, helenista y gran conocedor de la tipografía griega es autor, además, de otra excelente fuente
que aquí comentamos a menudo: Old Standard.},
sino que hace una rigurosa distinción entre los caracteres del punto medio y los del punto alto (véase \vref{fig:org40a63d9})
y ---es más--- aporta una propiedad OpenType de sustitución que reemplazará el punto medio por el correcto \emph{ano teleia}
en el contexto adecuado, es decir, en la escritura griega. Esto es de gran utilidad, pues evitaremos así cualquier tipo
de inconsistencia y nos aseguramos de que siempre tendremos el signo del punto alto en nuestro texto.
Expliquemos esa propiedad más extensamente en el siguiente apartado.

\begin{figure}[!h]
\centering
\includegraphics[width=0.4\textwidth]{./images/ano-teleia-ritsos-acantilado.jpg}
\caption{\label{fig:org24910ef}
Puntos altos no tan altos en el \emph{Ismene} de Ritsos (Acantilado)}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{./images/comparación-puntoalto-medio-tempora-lualatex.png}
\caption{\label{fig:org40a63d9}
Los caracteres del punto medio y el punto alto en la fuente Tempora LGC Unicode}
\end{figure}

\section{Punto alto y etiqueta 'locl' en Tempora LGC Unicode: ejemplo de uso en LuaTeX con 'fontspec'}
\label{sec:org42e68aa}
La etiquete OpenType \texttt{locl} pertenece al grupo GSUB de sustitución de caracteres y su correspondiente propiedad se
ejecuta únicamente dentro de un \texttt{script} (es decir, de un sistema de escritura) determinado. Si editamos Tempora LGC Unicode con
el editor de fuentes Fontforge (véase fig. \vref{fig:orga0fb990}), veremos que nuestra fuente tiene varios valores \texttt{locl} para
el \texttt{script} griego, entre las cuales se encuentra la sustitución del punto medio por el punto alto \emph{ano teleia}
canónico. Comprobamos que es así si abrimos en Fontforge la Ventana de métricas y activamos la etiqueta, como puede
apreciarse en la fig. \vref{fig:org0dddf70}.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/locl-tempora-fontforge.png}
\caption{\label{fig:orga0fb990}
Valores de \texttt{locl} en Tempora LGC Unicode}
\end{figure}

Así pues, para que la sustitución se lleve a efecto, debemos activar la opción de \texttt{fontspec} \texttt{Script=Greek} en la
familia que hayamos definido para griego dentro de nuestro documento \LaTeX{}. Podemos hacerlo directamente desde las
facilidades que ofrece el paquete \texttt{babel}, de tal forma que definiremos una familia para el idioma principal, donde el
punto medio será punto medio siempre, y otra para griego, donde todo punto medio se sustituirá por el alto. En ésta,
activaremos el \texttt{script} para griego junto a la etiqueta \texttt{locl}. Y en ambas (floritura aparte) activaremos también la
propiedad de sustitucón por glifo alternativo \texttt{aalt}, para que la letra \emph{rho} se represente con la variante de
descendente curvo, que es más propia de la cursiva, pero la añadimos aquí para emular el ejemplo de la página de
Acantilado. \hyperref[org674d8f0]{Así} quedaría nuestro código. El resultado de la compilación se muestra en la fig. \vref{fig:org4ac651a}.

\clearpage

\begin{listing}[htbp]
\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\documentclass{article}
\usepackage{fontspec}
\usepackage[greek.polytonic,spanish]{babel}
% Definimos la familia general de rm para Tempora LGC Unicode
\babelfont{rm}[RawFeature={+aalt}]{Tempora LGC Unicode}
% Y aquí definimos la subfamilia para griego
\babelfont[greek]{rm}[Script=Greek,RawFeature={+locl,+aalt}]{Tempora LGC Unicode}

\begin{document}

\Huge

ἀέρα·

\bigskip

\selectlanguage{greek}

ἀέρα·

\end{document}
\end{minted}
\caption{\label{org674d8f0}
Nuestro código para compilar en LuaLaTeX}
\end{listing}

\begin{figure}[!h]
\centering
\includegraphics[width=0.7\textwidth]{./images/locl-fontforge-tempora-demo.png}
\caption{\label{fig:org0dddf70}
Testeando la etiqueta \texttt{locl} en la ventana de métricas de \texttt{fontforge}}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/compilacion-locl.png}
\caption{\label{fig:org4ac651a}
Compilando nuestro código desde Emacs}
\end{figure}
\end{document}