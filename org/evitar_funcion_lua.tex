% Created 2019-12-02 lun 02:16
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Evitar que las funciones en Lua «choquen» con las macros de LaTeX en LuaTeX}}]{{\cabecera\MakeUppercase{Evitar que las funciones en Lua «choquen» con las macros de LaTeX en LuaTeX}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{Evitar que las funciones en Lua «choquen» con las macros de \LaTeX{} en LuaTeX}
\begin{document}

\maketitle
LuaTeX ofrece muchas y variadas maneras de manipular el contenido textual en distintos estadios del proceso de
compilación, en lo que va del código fuente al objeto tipográfico resultante. Hay vías más triviales y otras de mayor
complejidad, pero todas parten de esa característica esencial de que goza la avanzada versión de \TeX{} de puentear
las primitivas del cajista binario mediante \emph{scrips} en Lua, ya que incluye un intérprete para este lenguaje liviano y
multiusos.

Una forma limpia y rápida de manipulación, pero con un poderoso alcance en cuanto a resultados, es mediante el añadido
(un injerto, más bien) de funciones en Lua que incluyan sustituciones de cadenas de texto. Este tipo de funciones deben
anclarse o registrarse en el \emph{callback} de LuaTeX \texttt{process\_input\_buffer} que, como su nombre anuncia, actúa sobre
el \texttt{input} en el momento que éste es analizado para su compilación. Ya dimos algunos ejemplos \href{https://maciaschain.gitlab.io/lunotipia/fuentesfrankenstein.html}{aquí}, pero ahora nos
detendremos en algunas particularidades más y cierto problema colateral.

El \emph{callback} \texttt{process\_input\_buffer}, en efecto, remite a un momento muy tempranero del proceso. Tan tempranero que las
macros de \LaTeX{} que tengamos dispersas por el código fuente todavía no se han expandido, y por tanto pueden verse
fácilmente afectadas por nuestras funciones de sustitución de caracteres. Por ejemplo: imaginemos que queremos sustituir
todas las apariciones de la letra \guillemotleft{}b\guillemotright{} minúscula por la secuencia \texttt{\textbackslash{}textbf\{b\}} (o sea, que toda ocurrencia de la \guillemotleft{}b\guillemotright{}
quede en negrita en el \emph{output}). En cuanto el análisis llegue a alguna macro de \LaTeX{} que contenga esta letra
(como un simple \texttt{\textbackslash{}bigskip}), la compilación se detendrá por un error insalvable. Y no es para menos, porque antes ya se
habría operado allí la sustitución, y lo que tendría que compilar sería entonces un ininteligible \texttt{\textbackslash{}\textbackslash{}textbf\{b\}igskip}.
Así que, de entrada, y antes de echar mano de este recurso tan útil, conviene valorar antes los pros y los contras que
acarreará el hacerlo. En ocasiones, nada hay que temer, como en este ejemplo que puse al final de \href{https://maciaschain.gitlab.io/lunotipia/punto\_alto.html}{mi entrada} sobre el
problema del punto alto griego, donde se ajustaba la altura del glifo mediante un \texttt{string.gsub} de Lua. Es improbable
que una macro de \LaTeX{} incluya un punto alto griego. En otros contextos, cuando lo que esperamos es una
sustitución lineal de un carácter por otro, será mejor considerar crear al vuelo una propiedad OpenType de reemplazo,
mediante la función nativa de LuaTeX \texttt{font.handlers.otf.add.feature}\footnote{Esto puede ser muy productivo, por ejemplo, para transliteraciones directas entre alfabetos, como el latino y el gótico.}.

Habrá casos, sin embargo, donde nos toparemos sin remedio con el problema de frente. ¿Cómo solucionarlo? Bien, en \href{https://tex.stackexchange.com/questions/262630/lualatex-how-to-use-a-char-directive-inside-a-string-gsub-function}{un
hilo} de \texttt{tex.stackexchange.com} un usuario aportó una función que intentaba evitar el desaguisado mediante un
condicional. Aunque me parece muy ingeniosa la idea del condicional, la función (en parte porque era poco menos que un
esbozo) tenía un par de problemas de peso:

\begin{enumerate}[mienu]
\item Impide que se «toquen» las macros simples de \LaTeX{}, pero no las que tienen argumentos obligatorios u opcionales, de
modo que puede seguir habiendo obstáculos en la compilación.
\item Tampoco permite cribar aquellos argumentos donde sí quisiésemos que se operase una sustitución (por ejemplo, en un
\texttt{\textbackslash{}textit\{...\}}.
\end{enumerate}

Por tanto, he probado a intentar \guillemotleft{}mejorar\guillemotright{} esa función, añadiendo más condicionales, afinando un poco más y ampliando
las variables. De todo eso, extraigo aquí un mínimo ejemplo operativo.

Supongamos que en nuestro documento de \LaTeX{} queremos que se sustituyan al compilar todos los casos de \guillemotleft{}p\guillemotright{} y \guillemotleft{}m\guillemotright{}
por las cadenas \guillemotleft{}Pili\guillemotright{} y \guillemotleft{}Mili\guillemotright{}, respectivamente. Por supuesto, no queremos que se sustituyan dentro de las macros ni de
sus argumentos, pero sí en el interior de algunos argumentos, en este caso sólo dos: \texttt{\textbackslash{}textit\{...\}} y \texttt{\textbackslash{}section\{...\}}.

Para empezar, tenemos que separar la estructura de una macro de \LaTeX{} en seis variables, y a cada una de ellas le
asignaremos luego una captura de expresiones regulares, o patrones\footnote{No son las expresiones regulares, dicho sea de paso, el punto fuerte de Lua, pues no sigue aquí el estándar
POSIX. Con un sistema de expresiones regulares más completo, probablemente no serían necesarias tantas vueltas y el
asunto se resolvería de una manera más quirúrgica.} en la jerga de Lua, opcionales u obligatorios en su
aparición:

\begin{description}
\item[{a}] la barra invertida
\item[{b}] una secuencia de caracteres
\item[{c}] la llave izquierda
\item[{d}] secuencia de caracteres que puede contener espacios
\item[{e}] la llave derecha
\item[{f}] secuencia de caracteres que puede incluir un corchete izquierdo y otro derecho
\end{description}

Definimos ahora una función simple para la sustitución de los caracteres \guillemotleft{}p\guillemotright{} y \guillemotleft{}m\guillemotright{}\footnote{Téngase en cuenta que todo código Lua que insertemos en nuestro preámbulo debe encerrarse en un entorno \texttt{luacode},
del paquete del mismo nombre.}:

\pagebreak

\begin{minted}[frame=lines,linenos=true,breaklines]{lua}
function sustitucion (texto)
   texto = string.gsub (texto, "p", "Pili")
   texto = string.gsub (texto, "m", "Mili")
   return texto
end
\end{minted}

Como dije, con esta función sola no vamos a ningún lado, porque en cuanto una macro de \LaTeX{} tenga una \guillemotleft{}p\guillemotright{} o una \guillemotleft{}m\guillemotright{}
estaremos acabados.

Así que, a continuación, la función que hace el resto, y que voy comentando entre medias del código:

\begin{minted}[frame=lines,linenos=true,breaklines]{lua}
function sustitucion_buena ( texto )
   -- Aquí definimos antes una variable local con las 6 capturas a
   -- buscar; la parte que sustituye es una función anónima
   local x = texto:gsub('(\\?)([%a%@%s]+)([{]?)([%s%a%@]*)([}]?)([%[%]%s%a%@%w]?)', function (a,b,c,d,e,f)
                           -- Este primer condicional nos permite tocar el interior de textit y section
                           if (a~="" and b=="textit" or b=="section")  then
                              -- Defino esta variable, para sustituir dentro de textit y section:
                              y = string.gsub (d, "[%s%a%@]",   sustitucion)
                              -- y lo concatenamos todo:
               return a .. b .. c .. y .. e .. f
                           end
                           -- El segundo condicional es para no tocar
                           -- el resto de macros de LaTeX y que no nos
                           -- dé error en la compilación
                           if (a~="" or c~="" or d~="" or e~="" or f~="")  then
                           return a .. b .. c .. d .. e .. f
                           end
               -- Si no se dan las dos condiciones previas, entonces
               -- ya lo que tenemos es la cadena simple a sustituir
            b = string.gsub (b, "[%a%@%s]",   sustitucion)
            return b
         end)
       return x
end
\end{minted}

No nos queda más que definir el comando de \LaTeX{} que cargue la función:

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\newcommand\sust{\directlua{luatexbase.add_to_callback
     ( "process_input_buffer" , sustitucion_buena , "sustitucion_buena" )}}
\end{minted}

Y probarla. Para ver bien los resultados, nada mejor que un entorno \texttt{verbatim} (y el resultado de la compilación en fig. \vref{fig:orgb24a454}):

\begin{minted}[frame=lines,linenos=true,breaklines]{latex}
\begin{document}
  \sust
     \begin{verbatim}
        p y m
       \textit{p y m}
       \section{p y m}
       Pero esto no lo sustituye:
       \textbf{p y m}
       \cualquiermacro{p y m}[p y m]
     \end{verbatim}
\end{document}
\end{minted}


\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/pilimili.png}
\caption{\label{fig:orgb24a454}
Compilando nuestra sustitución de cadenas}
\end{figure}
\end{document}