% Created 2019-10-12 sáb 10:07
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
          \usepackage{normal-lua}
          \newenvironment{poema}{\begin{verse}}{\end{verse}\par}
                 \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Tipografía digital: la (eterna) asignatura pendiente}}]{{\cabecera\MakeUppercase{Tipografía digital: la (eterna) asignatura pendiente}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{Tipografía digital: la (eterna) asignatura pendiente}
\begin{document}

\maketitle
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/ZapfKnuth.jpg}
\caption{\label{fig:orgfff97a1}
El prof. Donald Knuth (izquierda) junto a Hermann Zapf (derecha)}
\end{figure}

La tipografía digital no es sino la continuación de la tipografía mecánica o \guillemotleft{}tradicional\guillemotright{} mediante medios digitales.
Aspira a sintetizar, reproducir y automatizar aquellos procesos y rutinas de los viejos impresores mediante la
programación y la potencia de cálculo de una computadora. Se trata, por tanto, de una evolución natural que suma y no
resta, y que edifica sobre las bases de un conocimiento heredado. Algoritmos tan sofisticados como el Plass-Knuth de \TeX{}
para el corte de líneas y la composición de párrafo sistematizan la pericia de los cajistas. El propio \TeX{} podría
definirse (lo hemos hecho así a menudo) como un cajista binario. Y Hermann Zapf acudió a la \emph{Biblia} de Gutenberg para
sus teorías de escalado horizontal y márgenes ópticos, que aplicaría el \emph{hz}-Program de Peter Karow y recogerían luego,
renovadas, pdfTeX y LuaTeX.

Dicho lo cual, podríamos pensar en justicia que la tipografía que se hace en estos tiempos que corren, con dos
decenios casi cumplidos del siglo \miscrom{XXI}, es tipografía digital. Todo parecería indicarlo, habida cuenta de que la
práctica totalidad del grueso del material de impresión que reciben las imprentas consiste en ese producto etéreo que
llamamos PDF y que se genera en un ordenador. Lo malo es que esta época de avances tecnológicos, presuntos muchas
veces, también lo es de una inusitada frivolidad, de la cual no se escapa ni el término \guillemotleft{}digital\guillemotright{}, empleado con pareja
frivolidad por doquier. Y es que lo digital es el lema de nuestro tiempo, de hecho. Lo define todo y todo acaba
siendo, al cabo, digital. Una cosa tan presente y tan ubicua que termina brillando por su ausencia. Acaso ganaríamos
en precisión si hablásemos, más bien, de servidumbre digital. Incluso de borreguismo digital, donde el usuario no es
administrador de los sistemas que utiliza y se contenta con pulsar botones aquí y allá, siempre donde \emph{otros} (las
benefactoras corporaciones, las dueñas reales de lo digital) se lo indican. El usuario de medios digitales del llamado
Primer Mundo no sería muy distinto de cualquier miembro de alguna tribu perdida de la Amazonia, instruido en el manejo
de un \emph{smartphone} por unos imaginarios y perversos exploradores. Ambos, al final, tardarían lo mismo ---muy poco---
en abrirse un perfil en \emph{Instagram}.

Insistamos, entonces, en la pregunta: ¿la tipografía que se hace ahora ---al menos, la mayor parte de ella--- ha de ser
tildada como \guillemotleft{}digital\guillemotright{}? Ciertamente, no. Sería una tipografía producida \emph{en} una computadora, pero no \emph{por medio de} una
computadora. De la misma forma que una carta de amor redactada con un teclado (y no con pluma de ave y tintero) y
enviada por correo electrónico (y no por paloma mensajera) no convierte a ese amor, expresado con mayor o menor fortuna,
en \guillemotleft{}digital\guillemotright{}. Esta tipografía sería, más bien, \emph{paradigital}. Está realizada en una computadora, sí, pero no aprovecha
la computación. No la comprende y, por tanto, la desprecia. Los cajistas y los linotipistas de antaño conocían a fondo
su herramienta de trabajo. Ahora tendrás suerte si encuentras a algún \guillemotleft{}maquetador\guillemotright{} o \guillemotleft{}diseñador gráfico\guillemotright{} de hoy día que
sepa o tenga una vaga noción de por qué ha guardado un texto en UTF-8, y qué consecuencias prácticas supone tal acción.
Este tipo de operarios, iletrados digitales \emph{stricto sensu}, ha terminado alcanzando el raro estatus de artífices sin
técnica que ejecutan un arte sin herramientas. Algunos, los menos, tendrán algún tipo de cultura tipográfica en
abstracto, y cierta idea de lo que quieren conseguir. Pero les falta el medio y, lo más importante, la materia, junto a
la comprensión de dicha materia. De vacíos así e inanidades tales se nutre el contenido de todos esos másteres de
edición que pululan por ahí (que considero una verdadera estafa y, en caso de estar auspiciados por universidades
públicas, una estafa vergozante) o de páginas web, tan populares como sobrevaloradas, del estilo de \emph{Unos tipos duros},
que por cierto resultan sonrojantemente blandos a la hora de plegarse al \emph{software} propietario y privativo. Se verá
bastante decepcionado quien se asome por esos ámbitos buscando algo de tipografía digital real.

El paisaje en torno, por tanto, no parece precisamente acogedor, y lo que tendría que haber sido una transición
natural entre el arte tipográfico mecánico y el producido mediante un ordenador, se ha visto truncado por un penoso
hiato, no surgido de casualidad por cierto, sino promovido interesadamente por las corporaciones de \emph{software}. Apple
y Adobe tienen no poca culpa de ello cuando en los 80 y 90 del pasado siglo se estableció la tiranía de los programas
de autoedición, que unidos casi genéticamente al término nebuloso (o vacuo) de \guillemotleft{}diseño gráfico\guillemotright{} han acabado deviniendo
plaga y cáncer para la tipografía. Es el emblema de \guillemotleft{}todo para el usuario pero sin el usuario\guillemotright{}, que va de la mano con
la imposición de esa cantinela tan malsana y perniciosa de la \guillemotleft{}informática amigable\guillemotright{}, que es la informática sin la
informática, y donde los procesos complejos se embozan bajo una capa de impostada banalidad y de un simplismo que no
tiene nada que ver con el mero concepto de lo simple. Con herramientas tan débiles, el usuario no tiene un control
real sobre los procesos que ejecuta, si es que los llega a ejecutar, y carece de una noción precisa de ellos. Es más,
la aparente \guillemotleft{}amigabilidad\guillemotright{} del medio no se traduce ---paradójicamente--- en menos trabajo, sino en un mayor coste
humano y material. Casi todo ha de hacerse a mano, cuando no a ojo. Pero en programas como Adobe InDesign lo manual no
tiene sentido si hablamos de la composición y producción seria de libros (como sí lo tendría en un \emph{software} de
dibujo). Lo manual en este caso es llana imposibilidad y, en última instancia, masoquismo.
\end{document}