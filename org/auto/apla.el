(TeX-add-style-hook
 "apla"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("varioref" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb*")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "normal-lua"
    "minted"
    "varioref")
   (TeX-add-symbols
    "miscrom"
    "maketitleold"
    "maketitle"
    "tableold"
    "tableofcontents"
    "logopie")
   (LaTeX-add-labels
    "fig:orgf301ff8"
    "fig:org8231df5"
    "fig:orgcc4e22c"
    "fig:org7bb21e3"
    "sec:orge777f80"
    "fig:org8f7d5c6"
    "fig:orgfdbbda4"
    "sec:org227d46b"
    "fig:orgd202925"
    "sec:org117f9f2")
   (LaTeX-add-environments
    "poema"))
 :latex)

