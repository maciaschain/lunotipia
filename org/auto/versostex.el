(TeX-add-style-hook
 "versostex"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("varioref" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb*")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "normal-lua"
    "minted"
    "varioref"
    "calc")
   (TeX-add-symbols
    "miscrom"
    "maketitleold"
    "maketitle"
    "tableold"
    "tableofcontents"
    "logopie"
    "vresto"
    "versolargo"
    "finverso")
   (LaTeX-add-labels
    "sec:org7ed72cb"
    "sec:org244988b"
    "fig:org1386eda"
    "sec:org46d6995"
    "sec:orgbb59623"
    "fig:org2328dfa"
    "sec:orga6c74e4"
    "fig:orgc69f1a4"
    "sec:orgf05fbed"
    "sec:org22b8f06"
    "sec:org685cf05"
    "fig:org956ade5"
    "fig:org051d1b1"
    "sec:org4c274df"
    "sec:org04dcf64"
    "fig:orgddc658a"
    "fig:org6903603"
    "sec:orgebb3ea9"
    "sec:org2c3291e"
    "sec:orgcea8668")
   (LaTeX-add-environments
    "poema"
    "edpoema")
   (LaTeX-add-index-entries
    "LaTeX!paquete verse"
    "LaTeX!paquete gmverse"
    "LaTeX!paquete microtype"
    "LaTeX!paquete reledmac"
    "tipografía del verso!centrado óptico del poema en la página")
   (LaTeX-add-lengths
    "vdesbordado"
    "verso"
    "versob"))
 :latex)

