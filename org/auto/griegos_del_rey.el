(TeX-add-style-hook
 "griegos_del_rey"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("varioref" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "normal-lua"
    "minted"
    "varioref")
   (TeX-add-symbols
    "miscrom"
    "maketitleold"
    "maketitle"
    "tableold"
    "tableofcontents"
    "logopie")
   (LaTeX-add-labels
    "sec:org5647ac3"
    "fig:orgfc13d8f"
    "fig:org6ffa86a"
    "fig:orgdb1947d"
    "fig:org8c237e4"
    "sec:org7b2a342"
    "fig:org5e7b6a1")
   (LaTeX-add-environments
    "poema")
   (LaTeX-add-index-entries
    "Tipografía griega!Fuentes griegas!Didot"
    "Tipografía griega!Fuentes griegas!Grecs Du Roi"
    "Tipografía griega!Fuentes griegas!GFS Complutum"
    "Tipografía griega!Fuentes griegas!GFS Baskerville"
    "Claude Garamond"))
 :latex)

