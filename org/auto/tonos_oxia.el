(TeX-add-style-hook
 "tonos_oxia"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("varioref" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "normal-lua"
    "minted"
    "varioref"
    "fontspec")
   (TeX-add-symbols
    "miscrom"
    "maketitleold"
    "maketitle"
    "tableold"
    "tableofcontents"
    "logopie")
   (LaTeX-add-labels
    "sec:org96844ce"
    "fig:org08e506b"
    "fig:orga2bfabb"
    "fig:org6d2b017"
    "fig:org91868bd"
    "fig:org5d4b55e"
    "fig:orgb95c2f6"
    "sec:orge27a4e0"
    "fig:org08ad392")
   (LaTeX-add-environments
    "poema")
   (LaTeX-add-fontspec-newfontcmds
    "cabecera"
    "sinoxia"
    "oxia"))
 :latex)

