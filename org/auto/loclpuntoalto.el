(TeX-add-style-hook
 "loclpuntoalto"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("varioref" "spanish") ("babel" "greek.polytonic" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "normal-lua"
    "minted"
    "varioref"
    "art10"
    "fontspec"
    "babel")
   (TeX-add-symbols
    "miscrom"
    "maketitleold"
    "maketitle"
    "tableold"
    "tableofcontents"
    "logopie")
   (LaTeX-add-labels
    "sec:orgdf1db88"
    "fig:org24910ef"
    "fig:org40a63d9"
    "sec:org42e68aa"
    "fig:orga0fb990"
    "org674d8f0"
    "fig:org0dddf70"
    "fig:org4ac651a")
   (LaTeX-add-environments
    "poema")
   (LaTeX-add-index-entries
    "Open Type!etiqueta 'locl'"
    "Tipografía griega!punto alto!etiqueta 'locl' (propiedad Open Type)")
   (LaTeX-add-fontspec-newfontcmds
    "cabecera"))
 :latex)

