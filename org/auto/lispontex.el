(TeX-add-style-hook
 "lispontex"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("varioref" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "normal-lua"
    "minted"
    "varioref"
    "art10"
    "fontspec"
    "expl3"
    "lisp-on-tex"
    "lisp-mod-l3regex"
    "showhyphens"
    "everyhook")
   (TeX-add-symbols
    "miscrom"
    "maketitleold"
    "maketitle"
    "tableold"
    "tableofcontents"
    "logopie"
    "prueba"
    "pruebabis"
    "pruebatris"
    "babelgriego"
    "bgriegopar")
   (LaTeX-add-labels
    "sec:org16c0e3b"
    "sec:org17153e3"
    "fig:orge950ba3")
   (LaTeX-add-environments
    "poema"
    "fragsgriego")
   (LaTeX-add-index-entries
    "LaTeX!lisp-on-tex"
    "LaTeX!Expresiones regulares"
    "LaTeX3")
   (LaTeX-add-fontspec-newfontcmds
    "cabecera"))
 :latex)

