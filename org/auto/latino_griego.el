(TeX-add-style-hook
 "latino_griego"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("varioref" "spanish") ("babel" "greek.ancient" "english" "latin" "german" "italian" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb*")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "normal-lua"
    "minted"
    "varioref"
    "rotating"
    "fontspec"
    "babel")
   (TeX-add-symbols
    "miscrom"
    "maketitleold"
    "maketitle"
    "tableold"
    "tableofcontents"
    "logopie")
   (LaTeX-add-labels
    "fig:org57c9297"
    "fig:org14b1cf1"
    "sec:orge98fc72"
    "fig:org4426aee")
   (LaTeX-add-environments
    "poema")
   (LaTeX-add-index-entries
    "tipografía griega!combinación de tipos griegos con latinos"
    "LaTeX!=fontspec=!propiedad =scale=")
   (LaTeX-add-fontspec-newfontcmds
    "cabecera")
   (LaTeX-add-babel-babelfonts
    "rm"))
 :latex)

