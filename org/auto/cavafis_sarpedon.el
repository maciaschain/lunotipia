(TeX-add-style-hook
 "cavafis_sarpedon"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("varioref" "spanish") ("babel" "greek.polytonic")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "normal-lua"
    "minted"
    "varioref"
    "fontspec"
    "caluacode"
    "babel"
    "verse")
   (TeX-add-symbols
    '("protverso" 1)
    "miscrom"
    "maketitleold"
    "maketitle"
    "tableold"
    "tableofcontents"
    "logopie")
   (LaTeX-add-labels
    "sec:org001e13c"
    "fig:orgf1646df"
    "fig:orge9b00d6"
    "fig:org91ab780"
    "fig:org6020b0d"
    "fig:orgd6777ac"
    "sec:org3bc86d6"
    "sec:org015ed62"
    "fig:orga23df3f"
    "sec:orgf523265"
    "sec:org451527b"
    "fig:org2cc2c57"
    "sec:org0bac206"
    "sec:orgef921c4")
   (LaTeX-add-environments
    "poema")
   (LaTeX-add-fontspec-newfontcmds
    "cabecera"))
 :latex)

