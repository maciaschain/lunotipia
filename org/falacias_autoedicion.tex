% Created 2021-12-01 mié 14:46
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
	  \usepackage{normal-lua}
	 	 % \def\miscrom#1{\textsc{\MakeLowercase{#1}}}
\let\maketitleold\maketitle
\def\maketitle{{\fontspec{Arial}\maketitleold}}
\let\tableold\tableofcontents
\def\tableofcontents{\tableold\bigskip}
\def\logopie{\raisebox{-1em}{\href{https://maciaschain.gitlab.io/lunotipia}{\includegraphics[width=2.5cm]{logo-LaTeX.png}}}}
\newfontfamily\cabecera[Letters=UppercaseSmallCaps,Numbers=Lowercase]{Linux Libertine O}
\newpagestyle{base}{\sethead[\thepage][][{\cabecera\MakeUppercase{Las falacias de la edición de escritorio}}]{{\cabecera\MakeUppercase{Las falacias de la edición de escritorio}}}{}{\thepage}\setfoot[\logopie][][]{}{}{\logopie}}
\pagestyle{base}
\usepackage{minted}
\usepackage[spanish,nospace]{varioref}
\renewcommand{\listingscaption}{Cuadro de código}
\author{Juan Manuel Macías}
\date{\today}
\title{Las falacias de la edición de escritorio}
\begin{document}

\maketitle
Hacia principios de los años '90, en un articulito sobre las cualidades microtipográficas
del \emph{software} experimental \texttt{HZ-Program}\footnote{H. Zapf, \guillemotleft{}About Micro-Typography and the HZ-Program\guillemotright{}, \emph{Electronic Publishing} 6, n.
3 (1993), págs. 283--88.}, aprovechaba el gran tipógrafo alemán
Hermann Zapf para saludar a la ya por entonces inexorable autoedición digital. Y lo hacía
de una manera mixta. Con optimismo, sin duda, subrayando las previsibles mudanzas de los
usos y costumbres a que el nuevo modelo nos habría de conducir:

\begin{foreigndisplayquote}{english}
Desktop publishing has changed the production of books in recent years. For the first time
the author has the possibility of preparing and influencing the design of his text. A
publisher will not always be happy about this, especially if the author does not want to
follow the strict rules of the publishing house which it may have followed for years. But
everybody should welcome this new method of collaboration between author and publisher.
\end{foreigndisplayquote}

Pero un optimismo este que no le apartaba de exponer ciertas prevenciones, muy sensatas:

\begin{foreigndisplayquote}{english}
\noindent [\ldots{}] In the past, typography was in the hands of professionals,
compositors, and specially trained people. Today we are facing a completely different
situation: alphabets are in the hands of non-professionals, of all kind of users, of
office people, of young people working with the capabilities of their Macintosh or PC.
\end{foreigndisplayquote}

A la vuelta ya de unos cuantos decenios, y tras asistir a todo orden de estragos y
despropósitos bajo el paraguas de los nuevos paradigmas, bien nos tememos que no podemos
---ya no--- compartir ese optimismo noventero del autor de los tipos Palatino. Las
prevenciones, por contra, se nos antojan a estas alturas más justificadas que nunca.

Pero antes de insistir en nuestros duelos y quebrantos, conviene esclarecer
suficientemente los términos. ¿Qué entendemos por \guillemotleft{}autoedición digital\guillemotright{}, o \guillemotleft{}edición de
escritorio\guillemotright{}, traducción más o menos apresurada del inglés \emph{Desktop Publishing}? Bien, lo
pernicioso de estos conceptos no provienen tanto de su acepción básica, es decir, la
producción del libro mediante una computadora (hecho que no podemos dejar de aplaudir),
sino de una presunta consecuencia que, con mayor o menor pudor, se ha venido defendiendo
una vez asumido lo primero. Esto es, que el \emph{software} es capaz por sí propio de
reemplazar, de principio a fin, todo el quehacer de los operarios técnicos, de tal forma
que cualquiera, con pocos o nulos conocimientos de tipografía y composición, puede
emprender la producción del libro cómodamente desde el ordenador de su casa, con sólo
llamar a unos cuantos resortes. Esta interpretación corrupta de la edición de escritorio,
desvariada y, en cierto sentido, \guillemotleft{}mágica\guillemotright{}, surge de un inmenso malentendido ante la
llegada de los nuevos medios digitales. Digamos que llegaron, sí, con toda razón y
justicia a la tipografía. Pero el comité de bienvenida no tenía muy claro hacia dónde
dirigir los vítores.

No fue, desde luego, una catástrofe espontánea. La anomalía ya se venía fraguando desde
los procesadores de texto, con su \guillemotleft{}lo que ves es lo que obtienes\guillemotright{} y su imposición de una
forma antinatural de escribir, que obligaba a los autores a confundir la estructura con el
formato, y a tomar intempestivas decisiones de orden tipográfico en mitad de una frase. De
ahí a que los pobres usuarios del Word se vieran con las ínfulas necesarias para refundar
toda la tipografía desde cero no mediaba más que un paso. La llamada \guillemotleft{}industria\guillemotright{} del libro
asumió, casi en masa, un flujo de trabajo basado en estos vicios, y tomó por falsos y
desafortunados estándares a los nuevos programas de maquetación y diagramado, que con
razón habían revolucionado la producción de revistas ilustradas y periódicos al mismo
tiempo que rebajaron la calidad de los libros impresos a niveles nunca vistos desde
Gutenberg, pues no están pensados para producir libros. Pero no sólo estaban los
\guillemotleft{}maquetadores\guillemotright{} y los \guillemotleft{}diseñadores gráficos\guillemotright{} dispuestos a envilecer con denuedo la
tipografía. Muchos editores ---especialmente pequeños o medianos editores--- se unieron a
la bacanal y se instalaron en su propio ordenador el \texttt{QuarkXpress} o el \texttt{InDesign} para
ensayar sus propias florituras. Tal es así que no pocos de estos editores llegaron a la
conclusión de que ya no era necesario un diseñador gráfico para arruinar un libro. Ya ven:
todo es tan fácil. Sólo se necesita una computadora y una copia, legal o no, del programa
que usan todos.

Con una premisa parecida, curiosamente, se subrayó el nacimiento a finales de los '70 de
otro tipo de \emph{software}, en las antípodas de esos programas de diagramado que vendrían
poco después. Nos referimos al sistema de composición tipográfica digital \TeX{}, que
sin duda es la revolución más importante en la tipografía desde la propia invención de la
imprenta: los ordenadores podían hacer ya hermosos libros, con las artes y técnicas de los
viejos cajistas. Pero todas esas rutinas, todo ese trabajo manual y repetitivo \TeX{}
se encargaría de controlarlo hasta el último detalle y automatizarlo de una forma global y
coherente, añadiendo un refinamiento extremo, aún hoy imbatido, a través de precisos e
ingeniosos algoritmos. Y tal hecho es tan cierto como que el propio \TeX{} nació de un
deseo de autoedición, cuando su creador, el gran Donald Knuth, quedó oportunamente
horrorizado ante las galeradas recibidas de uno de los tomos de su obra magna \emph{The Art of
Computer Programming}. Y, por si hubiese alguna duda, ya se encargaba el propio Knuth de
anunciar en su \emph{\TeX{} book} que su sistema estaba concebido para que cualquiera pudiese
componer un libro desde la paz de su ordenador doméstico. \TeX{}, naturalmente, estaba
recién nacido y se mostraba complejo y agreste, demasiado críptico incluso para los
usuarios más suicidas. Necesitaba un lenguaje más sencillo, más estilizado para poder
tratarlo.

No tardó en llegar, propicio, \LaTeX{}, con su inconmensurable acervo de macro paquetes
semánticos, cada cual especializado en su cometido. Si \TeX{} era el tipógrafo, el que
se mancha el mono de trabajo, \LaTeX{} se nos presentó como el director editorial, con
corbata, que no habla de cuadratines, espacios elásticos o demás jerga arcana de la
maquinaria y engranajes que tan bien entiende \TeX{}, sino de epígrafes, prosa y verso,
ediciones críticas, ecuaciones, diccionarios, y cualquier término familiar al universo del
editor o del lector. La relación \TeX{}/\LaTeX{} era ideal, rectilínea, pues cubría
perfectamente los planos físicos y semánticos de la tipografía. De hecho, ya el propio
Knuth había concebido \TeX{} como la base, el motor tipográfico con una serie limitada
de órdenes y procesos que habría de ser extendido mediante macros de muy alto nivel, casi
hasta el infinito. Sin duda tenemos aquí, y no en los programas de maquetación, la
evolución natural de la tipografía mecánica a la digital. Qué lástima que la propia
comunidad de \TeX{} y \LaTeX{} se haya mostrado históricamente tan capaz a un tiempo
de las cosas más sublimes y meritorias como de dispararse en el pie, entre conjuros e
invocaciones a los complacientes duendes de la autoedición. Y así, de paso, todo se
enzarzaba de mala manera.

Por un lado está el autoexilio, por no decir descarado auto-secuestro, de \LaTeX{}
dentro de la Academia. O, más bien, de una parte de la Academia (matemáticas, ingenierías
y demás). Pero si esta jibarización del ecosistema \TeX{} ya de por sí ha sido
suficientemente lamentable, al apartarlo de su ámbito natural, que es el de la producción
editorial de cualquier tipo de libro, para reducirlo a la raquítica logia de unos cuantos
escolares escribiendo fórmulas y ecuaciones, no menos daño ha traído la liberalidad con
que tantas veces se ha recomendado el uso del formato \LaTeX{} a todo género de
autores, escritores, escribientes y escribanos. Cuánto, cuánto envenenado desoriente ha
traído esa terca cantinela de \guillemotleft{}no uses Word, usa \LaTeX{}\guillemotright{}, como si el segundo fuese
una feliz alternativa legítima al (malhadado) primero.

Y es que hay que decirlo de una vez: \TeX{} y \LaTeX{} son herramientas
esencialmente tipográficas. No sirven, o sirven muy mal, para escribir, a pesar del
marcado semántico de \LaTeX{}. Por expresarlo llanamente: recomendar este \emph{software} a un
autor sería igual de absurdo como pedirle a Hemingway que arrojase a la basura su máquina
de escribir para instalarse una monotipia en su despacho.

Tal vez en este punto comience a asomar un poco el malentendido a que nos referíamos
líneas arriba. Que el ecosistema \TeX{} sea algo admirable y maravilloso, un paso
adelante en el ámbito digital desde la impresión mecánica, no con pretensiones de abolir
las excelencias precedentes sino de vindicar la tradición y hacerla brillar uniendo lo
mejor de dos mundos; que ahorre muchísimo esfuerzo y tiempo mediante la automatización
total y consistente, trasladando a complejos algoritmos la rutina manual de los antiguos
impresores; que entienda la concepción global del libro como programación y código, desde
la colación de datos y contenido a su preparación para la imprenta, y todo ello sin salir
de la computadora; que sea algo que cualquiera (\emph{software} libre) pueda descargar e
instalar en su propio ordenador\ldots{}; todo esto y más, en suma, no impide que \TeX{} y su
formato \LaTeX{} dejen de ser aquello que siempre han sido y jamás, desde su
nacimiento, han dejado de ser (incluso a despecho del propio Donald Knuth): una simple y
llana herramienta.

Y, como herramientas que son, despojémonos, por tanto, de cualquier pretensión mágica
respecto a éstas o a cualquier otra que esté por venir. El código de programación, por muy
complejo que sea, no hace el libro. Los alambicados y refinados algoritmos no hacen el
libro. Nos quitarán, sí, tiempo y esfuerzo; pero no nos devuelven el libro terminado,
entendido en términos tipográficos, ni existe botón ni resorte bajo el cielo que sirva
para tal prodigio. El libro como objeto lo hace el ingenio y el magín del tipógrafo, que
es el artífice de la herramienta y quien ha de enfrentarse con mayor o menor fortuna a
cada reto, a cada página, párrafo, línea o palabra, pues la primera y última norma de la
tipografía es que nunca hubo ni habrá de esperarse dos libros iguales.

Cambiar de herramientas, incluso de paradigma, no debería llevarnos a abolir la división
de poderes que siempre ha funcionado en el mundo de la producción editorial: el autor
escribe, el editor publica y el tipógrafo compone. Pero hoy día parece que muchos autores
y editores, confiados en el empoderamiento ilusorio de los medios digitales, no quieren
depender de los tipógrafos, y prefieren ensayar el camino por su cuenta, a ciegas, torpes,
ignorantes. Una renuncia tan osada como triste, pues el buen tipógrafo no impone un hilo
umbilical y servil a sus otros dos compañeros necesarios de viaje, sino que extiende en
una y otra dirección las tramas del diálogo y el valor de su consejo. Y tanto más acabado
un libro cuanto más fructífero el coloquio entre esos tres actores. En tales batallas nos
conformaríamos con que el premio final no fuese tanto la gloria para el autor, el
reconocimiento para el editor o los créditos del tipógrafo, sino un trofeo aun más
satisfactorio si cabe: la legibilidad.
\end{document}